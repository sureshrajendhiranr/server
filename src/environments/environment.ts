
export const environment = {
  production: false,
  // baseUrl: 'http://106.51.96.115/api/v1',
  baseUrl: 'http://localhost:9000/api/v1',
  filePath: 'http://106.51.96.115/uploads/',
  ws: 'ws://localhost:9000/ws/',
  // baseUrl: 'http://34.74.212.154/api/v1',
  // baseUrl: 'https://api.myairliftusa.com/api/v1',
};



