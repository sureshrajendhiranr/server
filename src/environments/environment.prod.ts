export const environment = {
  production: true,
  baseUrl: 'https://api.myairliftusa.com/api/v1',
  // baseUrl: 'http://localhost:9000/api/v1',
  filePath: 'https://api.myairliftusa.com/uploads/',
  ws: 'ws://localhost:7000/ws/',
  // baseUrl: 'http://34.74.212.154/api/v1'
};
