import { FormBuilderComponent } from './control-flow/form-builder/form-builder.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateFlowComponent } from './control-flow/create-flow/create-flow.component';
import { ViewComponent } from './view/view/view.component';
import { TableSectionComponent } from './control-flow/table-section/table-section.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { GeneralProcessComponent } from './control-flow/general-process/general-process.component';


import { AssignOwnerComponent } from './control-flow/work-flow/assign-owner/assign-owner.component';
import { TableFieldOrderComponent } from './control-flow/work-flow/table-field-order/table-field-order.component';
import { SettingMainViewComponent } from './admin-settings/setting-main-view/setting-main-view.component';
import { UserManagementComponent } from './admin-settings/user-management/user-management.component';
import { GroupManagementComponent } from './admin-settings/group-management/group-management.component';

import { FilterViewComponent } from './filter/filter-view/filter-view.component';
import { ViewTimetableComponent } from './timetable/view-timetable/view-timetable.component';
import { AcademicInfoModule } from './academic-info/academic-info.module';
import { AcademicCalendarComponent } from './academic-info/academic-calendar/academic-calendar.component';
import { AttendanceComponent } from './academic-info/attendance/attendance.component';
import { StudentListViewComponent } from './view/student-list-view/student-list-view.component';
import { AssesmentMarkComponent } from './academic-info/assesment-mark/assesment-mark.component';
import { DashboardMainComponent } from './dashboard/dashboard-main/dashboard-main.component';
import { ViewSubprocessComponent } from './view/view-subprocess/view-subprocess.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SubjectSplitAllocationComponent } from './academic-info/subject-split-allocation/subject-split-allocation.component';
import { AssesmentMarkReportComponent } from './academic-info/assesment-mark-report/assesment-mark-report.component';


const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {
    path: 'form-builder',
    component: FormBuilderComponent,
  },
  {
    path: 'create-flow/:value',
    component: CreateFlowComponent
  },
  {
    path: 'single-view/:data_table_id/:data_table_name/:row_id',
    component: ViewSubprocessComponent,
  },
  {
    path: 'table-secction',
    component: TableSectionComponent
  },
  {
    path: 'general-process',
    component: GeneralProcessComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'assign',
    component: AssignOwnerComponent
  },
  {
    path: 'tablefields',
    component: TableFieldOrderComponent
  },
  {
    path: 'admin-setting',
    component: SettingMainViewComponent,
    children: [
      {
        path: 'user-management',
        component: UserManagementComponent
      },
      {
        path: 'group-management',
        component: GroupManagementComponent
      }
    ]
  }, {
    path: 'main',
    component: SidenavComponent,
    children: [
      {
        path: 'view/:data_table_id',
        component: ViewComponent,
      },
      {
        path: 'create-flow/:value',
        component: CreateFlowComponent
      },
      {
        path: 'timetable',
        component: ViewTimetableComponent
      },
      {
        path: 'Academic_Calendar',
        component: AcademicCalendarComponent
      },
      {
        path: 'attendances',
        component: AttendanceComponent
      },
      {
        path: 'students',
        component: StudentListViewComponent
      },
      {
        path: 'AssesmentMark',
        component: AssesmentMarkComponent
      },
      {
        path:'AssesmentMarkReport',
        component:AssesmentMarkReportComponent
      },
      {
        path: 'dashboard',
        component: DashboardMainComponent
      },
      {
        path: 'general-process',
        component: GeneralProcessComponent
      }, {
        path: 'Subject_Allocation',
        component: SubjectSplitAllocationComponent
      }
    ]
  },
  {
    path: 'filterView',
    component: FilterViewComponent
  },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
