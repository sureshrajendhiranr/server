import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { FormBuilderApiService } from '../../../app/control-flow/services/form-builder-api.service';
import { ToastService } from '../../../app/shared/common/toast.service';
import { CommonService } from '../../../app/shared/common/common.service';
import { AdvancedFieldListenerService } from '../../services/advanced-field-listener.service';
import { LastmodifiedListenerService } from '../../services/lastmodified-listener.service';
import { ComputationListenerService } from '../../services/computation-listener.service';
import { ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { WorkFlowService } from '../../control-flow/work-flow/service/work-flow.service';

@Component({
  selector: 'app-view-subprocess',
  templateUrl: './view-subprocess.component.html',
  styleUrls: ['./view-subprocess.component.css']
})
export class ViewSubprocessComponent implements OnInit {
  @Input() infoData: any;

  data = [];
  dataTableInfo = JSON.parse(localStorage.getItem('currentView'));
  // obj = { tableName: this.dataTableInfo.dataTableName, field: '', fieldValue: '' }
  apiObj = {
    tableName: 'Field_Groups', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'position', field: 'data_table_name_id', fieldValue: '', search: [], filter: {}
  };
  // Sub Process List
  subProcessApiObj = {
    tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'name',
    field: '', fieldValue: '', search: { categories: this.dataTableInfo.name }, filter: {}
  };
  subProcessList = [];
  selectedSubProcess = this.dataTableInfo.name;
  putBody = {};
  groupList = [];
  metaDataGroupBy = {};
  value = {};
  isLoader = true;
  isView = false;
  groupSelect = '';
  isSubProcessView: boolean = false;
  subProcessInput: any;
  currentUser = JSON.parse(localStorage.flowPodUser);
  groups: any;
  // Static Code
  shipment: any = {};
  isGroupList: boolean = false;
  isMetaData: boolean = false;
  isPostLoading: boolean = false;
  dd: any = { rowId: 0, value: {}, inputValue: {} };
  //  Get url paramates
  getUrlParam = {
    data_table_name: '',
    data_table_id: '',
    row_id: ''
  };
  getObj = {
    'table_name': 'My_Orders',
    'limit': 1,
    'page': 0,
    'sort_type': 'DESC',
    'sort_field': 'id',
    'filter': {
      'operator': 'OR',
      'parent_filter_list': [
        {
          'operator': 'OR',
          'child_filter_list': [
            {
              'left_field': 'id',
              'operator_condition': 'equal_to',
              'type': 'value',
              'value_list': [],
              'right_field': ''
            }
          ]
        }
      ]
    }
  };

  isLoadingRowInfo: boolean = true;
  actionList = [];
  constructor(public commonApiService: CommonApiService,
    public formBuilderApiService: FormBuilderApiService,
    private toastService: ToastService,
    public commonService: CommonService,
    public advancedFieldListenerService: AdvancedFieldListenerService,
    public lastmodifiedListenerService: LastmodifiedListenerService,
    public computationListenerService: ComputationListenerService,
    private route: ActivatedRoute,
    private location: Location,
    public workFlowService: WorkFlowService) { }

  ngOnInit() {

    this.getUrlParam.row_id = this.route.snapshot.paramMap.get('row_id');
    this.getUrlParam.data_table_id = this.route.snapshot.paramMap.get('data_table_id');
    this.getUrlParam.data_table_name = this.route.snapshot.paramMap.get('data_table_name');
    this.dd['rowId'] = this.getUrlParam.row_id;
    this.dd['value'] = {}
    this.getProcessInfo();
    this.getAllSubprocessList();
    // this.getMataDataByGroup();
    // this.value = this.infoData.value;
    this.getRowInfo();
    // this.getAllGroup();
    // this.subProcessList.push(this.dataTableInfo.name);
    // this.getAllSubprocessList();

    // // Static Data
    // this.shipment.events = [];
    // this.setShipmentEvents();
  }
  back() {
    this.location.back();
  }

  // Get proccess infomations
  getProcessInfo() {
    this.commonApiService.getDataTableInfo(this.getUrlParam.data_table_id).subscribe(res => {
      if (res.statusCode === 200) {
        this.dataTableInfo = res.info;
        // Group informations
        this.groupList = this.dataTableInfo['field_group_list'];
        this.isGroupList = true;
        this.subProcessList.push(this.dataTableInfo.name);
      }
    });
  }

  getRowInfo() {
    this.isLoadingRowInfo = true;
    this.getObj.table_name = this.getUrlParam.data_table_name;
    this.getObj.filter.parent_filter_list[0].child_filter_list[0].left_field = 'id';
    this.getObj.filter.parent_filter_list[0].child_filter_list[0].value_list = [this.getUrlParam.row_id + '']
    this.getObj['key_type'] = "test";
    this.commonApiService.get(this.getObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.dd['value'] = res.info[0];
        this.dd['inputValue'] = res.info[0];
        this.isLoadingRowInfo = false;
        // this.getStepAction(res.info[0]);
      }
    });
  }




  // isAdvanced(item) {
  //   let temp = [], advanced;

  //   this.advancedFieldListenerService.dataSource.subscribe(res => {
  //     temp = res;
  //     if (!!item.isAdvanced.length) {
  //       advanced = JSON.parse(item.isAdvanced)
  //       if (advanced.type === 'depended') {
  //         console.log(advanced.type)
  //       }
  //     }
  //   });
  //   this.advancedFieldListenerService.dataSource.next(temp);
  // }


  // Static code




  getMataDataByGroup() {
    this.isLoader = true;
    this.formBuilderApiService.getMetaDataGroupBy(this.getUrlParam.data_table_name).subscribe(res => {
      //  Meta data fields namee change ssnake to camelcase
      // Object.keys(res.info).forEach(i => {
      //   res.info[i].forEach(j => {
      //     j.items.forEach(k => {
      //       k.name = this.commonService.toCamelCase(k.name);
      //     });
      //   });
      // });
      this.metaDataGroupBy = res.info;
      this.isLoader = false;
      this.isMetaData = true;
    });
  }
  elementScroll(el) {
    document.getElementById(el).scrollIntoView({
      behavior: 'smooth', block: 'start', inline: 'start'
    });
  }

  getAllGroup() {
    this.apiObj.fieldValue = this.dataTableInfo.id;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
        this.isGroupList = true;
      }
    });
  }

  getAllSubprocessList() {
    this.commonApiService.getSubProcessTables(this.getUrlParam.data_table_id).subscribe(res => {
      if (res.statusCode === 200) {
        (res.info).forEach(i => {
          this.subProcessList.push(i);
        });
      }
    });
  }

  getSubProcess(name, type) {
    if (type === 'main') {
      this.isSubProcessView = false;

    } else {
      this.isSubProcessView = true;
      this.subProcessInput = name;
    }
  }



  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.dataTableInfo.dataTableName;
    this.putBody['id'] = this.infoData.rowId;
    if (e.isValid) {
      let temp = {};
      this.computationListenerService.dataSource.subscribe(res => {
        temp = res;
      });
      let list = Object.keys(temp);
      // console.log(list)
      if (list.includes(key)) {
        temp[key] = e[key];

        this.computationListenerService.dataSource.next(temp);
      }

      let fieldList;
      this.lastmodifiedListenerService.fieldList.subscribe(res => {
        fieldList = res;
      });
      if (!!fieldList.length) {
        let temp;
        this.lastmodifiedListenerService.dataSource.subscribe(res => {
          temp = res;
        });
        fieldList.forEach(i => {
          const userInfo = { 'userName': this.currentUser.userName, 'id': this.currentUser.id + '', 'email': this.currentUser.email };
          temp['isView'][i] = [userInfo];
          this.putBody[i] = this.currentUser.id + '';
        });
        this.lastmodifiedListenerService.dataSource.next(temp);
      }

      this.commonApiService.update(this.putBody).subscribe(res => {
        if (res.status === 202) {
          if (!!e.file) {
            this.commonApiService.fileCreate(e.file).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.toastService.success(res.message);
              }
            });
          } else {
            this.toastService.success(res.message);
          }
        }
      });
    } else {
      this.toastService.success('inValid Value');
    }
    this.putBody = {};
  }

}
