import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSubprocessComponent } from './view-subprocess.component';

describe('ViewSubprocessComponent', () => {
  let component: ViewSubprocessComponent;
  let fixture: ComponentFixture<ViewSubprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSubprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSubprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
