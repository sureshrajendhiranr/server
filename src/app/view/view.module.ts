import { FilterModule } from './../filter/filter.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewComponent } from './view/view.component';
import { MaterialModule } from '../material.module';
import { ControlFlowModule } from '../control-flow/control-flow.module';
import { CreateComponent } from './create/create.component';
import { SharedModule } from '../shared/shared.module';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ResizableModule } from 'angular-resizable-element';
import { ViewSubprocessComponent } from './view-subprocess/view-subprocess.component';
import { Template1Component } from './templates/template1/template1.component';
import { ViewFieldsComponent } from './view-fields/view-fields.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentListViewComponent } from './student-list-view/student-list-view.component';


@NgModule({
  declarations: [ViewComponent, CreateComponent,
    ViewSubprocessComponent, Template1Component, ViewFieldsComponent, StudentListViewComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ControlFlowModule,
    SharedModule,
    Ng2GoogleChartsModule,
    ResizableModule,
    FilterModule,
    FormsModule, ReactiveFormsModule
  ],
  exports: [ViewComponent, StudentListViewComponent, CreateComponent],
  entryComponents: [ViewComponent]
})
export class ViewModule { }
