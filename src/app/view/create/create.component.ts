import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { ToastService } from '../../shared/common/toast.service';
import { MatDialogRef } from '@angular/material/dialog';
import { DesignationService } from '../../services/designation.service';
import { FormBuilderApiService } from '../../../app/control-flow/services/form-builder-api.service';
import { ComputationListenerService } from '../../services/computation-listener.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  @Input('data') metaData: any;
  @Output() modelClose = new EventEmitter();
  postBody = {};
  currentView = JSON.parse(localStorage.currentView);
  fileObj = [];
  progress = 0;
  apiObj = {
    tableName: 'Field_Groups', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: 'data_table_name_id', fieldValue: '', search: [], filter: {}
  };
  isAllFieldValid: boolean = false;
  isAllFieldValidCount = 0

  groupList = [];
  selectedGroup = '';
  valid = [];
  fieldCount: any;
  // processingField = 0;

  // sub Section values
  sectionTableFields = [];

  // Check is designation
  isDesignation = false;
  isLoader: boolean = true;
  isPostLoading: boolean = false;
  metaDataGroupBy = {};
  inputValue = {};
  computationalFields = [];
  constructor(private commonApiService: CommonApiService, private toastService: ToastService,
    private designationService: DesignationService, public formBuilderApiService: FormBuilderApiService,
    private computationListenerService: ComputationListenerService
  ) { }

  ngOnInit() {
    if (window.location.href.includes('designation')) { this.isDesignation = true; }
    this.getAllGroup();
    this.postBody['tableName'] = this.currentView.dataTableName;
    this.fieldCount = this.metaData.length - 2;
    this.metaData.forEach(i => {
      if (i.required === 0) {
        this.valid.push(i.name);
      }
    });
    this.isAllFieldValid = (this.isAllFieldValidCount >= this.metaData.length - 2);
    this.getMataDataByGroup();
  }

  scrollElement(groupName) {
    document.getElementById(groupName).scrollIntoView({ behavior: 'smooth' });
  }

  eventListen(e) {
    // console.log(e)
    if (!!e.isSectionTable) {
      // console.log(e)
      this.sectionTableFields.forEach(i => {
        if (!!e[i]) {
          // console.log(e[i])
          e[i].forEach(j => {
            Object.assign(j, { rootTableId: this.currentView.id });
          });
          this.postBody[i] = e[i];
        }
      });
      if (!!e.file) {
        this.fileObj.push(e.file);
      }

    } else {
      this.computationListenerService.dataSource.next(this.inputValue);
      const key = Object.keys(e)[0];
      this.postBody[key] = e[key];
      if (!!e.autoFill) {
        e.autoFill.forEach(i => {
          this.postBody[i] = this.inputValue[i];
        });
      }
      // console.log(this.postBody)
      let processingField = (Object.keys(this.postBody).length - 1);

      if (!!e.file) {
        this.fileObj.push(e.file);
      }
      if (e.isValid) {
        if (!this.valid.includes(key)) {
          this.valid.push(key);
        }
        this.progress = e[key].length ? ((processingField / this.fieldCount) * 100) : this.progress;
      } else {
        if (this.valid.includes(key)) {
          this.valid.splice(this.valid.indexOf(key), 1);
          this.progress = this.progress - (processingField / this.fieldCount) * 50;
        }
      }
      this.isAllFieldValid = (this.valid.length >= this.fieldCount);
    }

  }

  getAllGroup() {
    this.apiObj['fieldValue'] = this.currentView.id;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
        this.selectedGroup = this.groupList[0].groupName;
      }
    });
  }

  getMataDataByGroup() {
    this.isLoader = true;
    let group = [];
    this.formBuilderApiService.getMetaDataGroupBy(this.currentView.dataTableName).subscribe(res => {
      this.metaDataGroupBy = res.info;
      group = Object.keys(res.info)
      // console.log(this.metaDataGroupBy)
      group.forEach(i => {
        res.info[i].forEach(j => {
          j.items.forEach(k => {
            if (k.type === 'table') {
              this.sectionTableFields.push(k.sectionTableId + '');
              this.postBody['sectionTableFields'] = this.sectionTableFields;
            }
            this.inputValue[k.name] = k.defaultValue;
            // console.log(this.inputValue[k.name])
            if (k.computational.length) {
              this.computationalFields.push(k.name);
            }
            // this.computationListenerService.fieldSource.next(this.computationalFields);
          });
        });
      });
      this.isLoader = false;
    });
    // console.log(this.inputValue)
  }
  save() {
    // Designation
    this.isPostLoading = true;
    if (this.isDesignation) {
      delete this.postBody['tableName'];
      this.designationService.create(this.postBody).subscribe(res => {
        if (res.statusCode === 201) {
          this.toastService.success('Successful created');
          this.isPostLoading = false;
          this.modelClose.emit(true);

        }
      });
    } else {
      if (!!this.computationalFields.length) {
        this.computationalFields.forEach(i => {
          this.postBody[i] = this.inputValue[i];
        });
      }
      this.commonApiService.create(this.postBody).subscribe(res => {
        if (res.statusCode === 201) {
          this.toastService.success('Successful created');
          this.isPostLoading = false;
          this.modelClose.emit(true);
          if (this.fileObj.length) {
            this.fileObj.forEach(i => {
              this.commonApiService.fileCreate(i).subscribe(fileRes => {
                if (fileRes.statusCode === 200) {

                }
              });
            });
          }
        }
      });
    }

  }
}
