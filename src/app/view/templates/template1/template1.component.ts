import { Component, OnInit, ElementRef, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { ResizeEvent } from "angular-resizable-element";
import { CommonApiService } from '../../../services/common-api.service';
import { FormBuilderApiService } from '../../../control-flow/services/form-builder-api.service';
import { ToastService } from '../../../shared/common/toast.service';
import { CommonService } from '../../../shared/common/common.service';
import { MatDialogRef } from '@angular/material';
import { iconList } from '../../../control-flow/constant';
@Component({
  selector: 'app-template1',
  templateUrl: './template1.component.html',
  styleUrls: ['./template1.component.css']
})
export class Template1Component implements OnInit {
  @ViewChild('myDiv') theDiv: ElementRef;
  @Output() closeEvent = new EventEmitter();
  style: any = {
    width: '100%',
    height: '100%',
    color: '#ccc',
    background: '#e6e3e3',
    border: '1px solid #fff',
    'border-radius': '1px',
    perspective: '1px'
  };
    // Icons
    iconList = iconList;
    materialIconList = [];
    filterIconList = [];
  isViewEdit = false;
  containerWidth: number;
  // Data set
  @Input() infoData: any;
  data = [];
  dataTableInfo = JSON.parse(localStorage.getItem('currentView'));
  // obj = { tableName: this.dataTableInfo.dataTableName, field: '', fieldValue: '' }
  apiObj = {
    tableName: 'Field_Groups', limit: 100, page: 0, sortType: 'DESC',
    sortValue: 'id', field: 'data_table_name_id', fieldValue: '', search: [], filter: {}
  };
  // Sub Process List
  subProcessApiObj = {
    tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'name',
    field: '', fieldValue: '', search: { categories: this.dataTableInfo.name }, filter: []
  };
  subProcessList = [];
  selectedSubProcess = this.dataTableInfo.name;
  putBody = {};
  groupList = [];
  metaDataGroupBy = {};
  value = {};
  isLoader = true;
  isView = false;
  groupSelect = '';
  // Tempale select
  isTempateShow=false;
  teplateItems = [];
  templateSelectIndex = 0;
  metaDataArray = [];
  constructor(public commonApiService: CommonApiService, public formBuilderApiService: FormBuilderApiService,
    private toastService: ToastService, public commonService: CommonService) { }

  ngOnInit() {
    this.value = this.infoData.value;
    this.getMataDataByGroup();
    this.getAllGroup();
    this.subProcessList.push(this.dataTableInfo.name);
    this.getAllSubprocessList();
    this.iconList.categories.forEach(i => { i.icons.forEach(j => { this.materialIconList.push(j.id); }); });
    this.filterIconList = this.materialIconList;
    this.teplateItems=JSON.parse(localStorage.teplateItems)
  }
  ngAfterViewChecked() {
    this.containerWidth = this.theDiv.nativeElement.clientWidth;
  }
  onResizeEnd(event: ResizeEvent): void {
    let valueCal = Math.round(((event.rectangle.width) / this.containerWidth) * 100);
    if (valueCal > 100) {
      this.style.width = 100 + '%';
    } else {
      this.style.width = valueCal + '%';
    }
    this.style.height = event.rectangle.height + 'px';
  }
  saveTemplate(){
    localStorage.setItem('template1',JSON.stringify(this.teplateItems))
  }

  customView(val, type) {
    if (type === 'w') {
      this.style.width = val + '%';
    } else if (type === 'h') {
      this.style.height = val + '%';
    } else if (type === 'c') {
      this.style.color = val;
    } else if (type === 'bg') {
      this.style.background = val;
    } else if (type === 'b') {
      this.style.border = val;
    } else if (type === 'b-r') {
      this.style['border-radius'] = val;
    }
  }


  getMataDataByGroup() {
    this.isLoader = true;
    this.formBuilderApiService.getMetaDataGroupBy(this.dataTableInfo.dataTableName).subscribe(res => {
      this.metaDataGroupBy = res.info;
      let keys = Object.keys(res.info);
      keys.forEach(i => {
        res.info[i].forEach(j => {
          j.items.forEach(k => {
            this.metaDataArray.push(k);
          });
        });
      });
      this.isLoader = false;
    });
  }
  getAllGroup() {
    this.apiObj.fieldValue = this.dataTableInfo.id;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
      }
    });
  }
  getAllSubprocessList() {
    this.commonApiService.getAll(this.subProcessApiObj).subscribe(res => {
      if (res.statusCode === 200) {
        (res.info).forEach(i => {
          this.subProcessList.push(i.name);
        });
      }
    });
  }

  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.dataTableInfo.dataTableName;
    this.putBody['id'] = this.infoData.rowId;
    if (e.isValid) {
      this.commonApiService.update(this.putBody).subscribe(res => {
        if (res.status === 202) {
          if (!!e.file) {
            this.commonApiService.fileCreate(e.file).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.toastService.success(res.message);
              }
            });
          } else {
            this.toastService.success(res.message);
          }
        }
      });
    } else {
      this.toastService.success('inValid Value');
    }
    this.putBody = {};
  }

  iconsFilter(val) {
    this.filterIconList = this.materialIconList.filter(i => i.includes(val))
  }

}
