import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class StudentService {

  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }


  getStudentsInfo(obj): Observable<any> {
    const params = new HttpParams()
      .set('getRequest', obj ? JSON.stringify(obj) : '');
    const requestUrl = this.baseUrl + '/getStudentInfo/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getHandlingClassess(userId, type): Observable<any> {
    const params = new HttpParams()
      .set('userId', userId)
      .set('type', type);
    const requestUrl = this.baseUrl + '/handlingClasses/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

}
