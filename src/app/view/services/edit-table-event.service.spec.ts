import { TestBed } from '@angular/core/testing';

import { EditTableEventService } from './edit-table-event.service';

describe('EditTableEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditTableEventService = TestBed.get(EditTableEventService);
    expect(service).toBeTruthy();
  });
});
