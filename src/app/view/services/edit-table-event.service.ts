import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditTableEventService {

  constructor() { }
  res = {};
  public eventEmit = new BehaviorSubject<any>(this.res);
  eventReceive = this.eventEmit.asObservable();

  }
