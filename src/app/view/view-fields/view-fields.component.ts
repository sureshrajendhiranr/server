import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { FormBuilderApiService } from '../../../app/control-flow/services/form-builder-api.service';
import { ToastService } from '../../../app/shared/common/toast.service';
import { CommonService } from '../../../app/shared/common/common.service';
import { AdvancedFieldListenerService } from '../../services/advanced-field-listener.service';
import { LastmodifiedListenerService } from '../../services/lastmodified-listener.service';

@Component({
  selector: 'app-view-fields',
  templateUrl: './view-fields.component.html',
  styleUrls: ['./view-fields.component.css']
})
export class ViewFieldsComponent implements OnInit {
  @Input() infoData: any;
  @Output() doucmentEmitter = new EventEmitter();
  currentUser = JSON.parse(localStorage.flowPodUser);
  data = [];
  dataTableInfo = JSON.parse(localStorage.getItem('currentView'));
  // obj = { tableName: this.dataTableInfo.dataTableName, field: '', fieldValue: '' }
  apiObj = {
    tableName: 'Field_Groups', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'position', field: 'data_table_name_id', fieldValue: '', search: [], filter: {}
  };
  isLoader: boolean = false;
  metaDataGroupBy = [];
  groupList = [];
  putBody = {};
  groupSelect = '';
  value = {};
  constructor(
    public commonApiService: CommonApiService,
    public formBuilderApiService: FormBuilderApiService,
    private toastService: ToastService,
    public commonService: CommonService,
    public advancedFieldListenerService: AdvancedFieldListenerService,
    public lastmodifiedListenerService: LastmodifiedListenerService
  ) { }

  ngOnInit() {
    this.value = this.infoData.value;
    this.getAllGroup();
    this.getMataDataByGroup();
  }

  getMataDataByGroup() {
    this.isLoader = true;
    this.formBuilderApiService.getMetaDataGroupBy(this.dataTableInfo.dataTableName).subscribe(res => {
      this.metaDataGroupBy = res.info;
      // console.log(res.info)
      this.isLoader = false;
    });
  }
  elementScroll(el) {
    document.getElementById(el).scrollIntoView({
      behavior: 'smooth', block: 'start', inline: 'start'
    });
  }
  getAllGroup() {
    this.apiObj.fieldValue = this.dataTableInfo.id;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
      }
    });
  }

  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.dataTableInfo.dataTableName;
    this.putBody['id'] = this.infoData.rowId;
    if (e.isValid) {
      let fieldList;
      this.lastmodifiedListenerService.fieldList.subscribe(res => {
        fieldList = res;
      });
      if (!!fieldList.length) {
        let temp;
        this.lastmodifiedListenerService.dataSource.subscribe(res => {
          temp = res;
        });
        fieldList.forEach(i => {
          const userInfo = { 'userName': this.currentUser.userName, 'id': this.currentUser.id + '', 'email': this.currentUser.email };
          temp['isView'][i] = [userInfo];
          temp['isTableView'][this.infoData.rowId][i] = [userInfo];
          this.putBody[i] = this.currentUser.id + '';
        });
        this.lastmodifiedListenerService.dataSource.next(temp);
      }
      if (!!e.autoFill) {
        e.autoFill.forEach(i => {
          this.putBody[i] = this.infoData.inputValue[i];
        });
      }
      this.commonApiService.update(this.putBody).subscribe(res => {
        if (res.status === 202) {
          if (!!e.file) {
            this.commonApiService.fileCreate(e.file).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.toastService.success(res.message);
              }
            });
          } else {
            this.toastService.success(res.message);
          }
        }
      });
    } else {
      this.toastService.success('inValid Value');
    }
    this.putBody = {};
  }

}

