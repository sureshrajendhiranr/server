import { Component, OnInit, Output, Input, ɵConsole } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';


import { Router, ActivatedRoute } from '@angular/router';
import { CommonApiService } from '../../services/common-api.service';
import { CommonService } from '../../shared/common/common.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastService } from '../../shared/common/toast.service';

// import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { ChartReadyEvent } from 'ng2-google-charts';
import { DesignationService } from '../../services/designation.service';
import { ResizeEvent } from 'angular-resizable-element';
import { FormBuilderApiService } from '../../control-flow/services/form-builder-api.service';
import { AdvancedFieldListenerService } from '../../services/advanced-field-listener.service';
import { LastmodifiedListenerService } from '../../services/lastmodified-listener.service';
import { ComputationListenerService } from '../../services/computation-listener.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ViewComponent implements OnInit {
  // @Output('createItem') createItem: any;
  expandedElement = { row: '', column: '' };
  data = [];
  metaData = [];
  metaDataLoader = false;
  dblClick = '';
  currentView: any;
  dataTableInfo: any;
  // columnsToDisplay = []
  createModelDialog: any;
  tableViewCustom: any;
  isLoader = false;
  infoData: any;
  dataSource: any;
  // Check is designation
  isDesignation = false;
  // public pieChart: GoogleChartInterface = {
  //   chartType: 'PieChart',
  //   dataTable: [
  //     ['Task', 'Hours per Day'],
  //     ['Work', 11],
  //     ['Eat', 2],
  //     ['Commute', 2],
  //     ['Watch TV', 2],
  //     ['Sleep', 7]
  //   ],
  //   options: { 'title': 'Tasks' },
  // };


  // Selection
  selectHover: number;
  isAllSelected = false;

  // Filter Open Dialog
  filterOpen = false;


  currentUser = JSON.parse(localStorage.flowPodUser)
  // API get Object
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };

  apiObjMeta = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'table_view_position',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  length = 100;
  pageSize = 10;
  stickyObj = 'number';
  pageSizeOptions: number[] = [5, 10, 25, 100];
  tempMetaData = [];
  metaDataGroupBy = [];

  // Search selected Item
  selectedSearchField = {};
  // Minimues
  minimize = [];

  putBody = {};
  // columnsToDisplay = ['select', 'containerNo', 'containerType',
  //   'siNumber', 'receiver', 'receiverLocation', 'estimatedShipDate', 'actualShipDate', 'status'];
  columnsToDisplay = [];
  searchMetaData = [];
  row
  expandColumn: any;
  gridViewFieldToDisplay = [];
  istableView: boolean = true;
  gridViewMetaData = [];
  gridMetaData = [];
  @Input('subProcessInput') subProcessInput: any;

  constructor(private route: ActivatedRoute,
    public commonApiService: CommonApiService,
    private commonService: CommonService,
    public dialog: MatDialog,
    private toastService: ToastService,
    private routingNavigate: Router,
    private designationService: DesignationService,
    public formBuilderApiService: FormBuilderApiService,
    public advancedFieldListenerService: AdvancedFieldListenerService,
    public lastmodifiedListenerService: LastmodifiedListenerService,
    public computationListenerService: ComputationListenerService) {

  }

  ngOnInit() {
    // this.getCurrentUserdesignation();
    if (window.location.href.includes('designation')) { this.isDesignation = true; }
    if (this.subProcessInput) {
      this.currentView = this.subProcessInput;
      this.apiObj.tableName = this.currentView.dataTableName;
      this.apiObj.search = {};
      this.getAll();
      this.getMetaData();
    } else {
      this.route.paramMap.subscribe(params => {

        this.currentView = JSON.parse(localStorage.currentView);
        this.apiObj.tableName = this.currentView.dataTableName;

        this.apiObj.search = {};
        // Customtable view change
        if (!!localStorage.tableView) {
          this.tableViewCustom = JSON.parse(localStorage.tableView);
          if (!!this.tableViewCustom[this.currentView.dataTableName]) {
            this.tempMetaData = JSON.parse(this.tableViewCustom[this.currentView.dataTableName]);
          }
        }
        if (!!localStorage.flowPodUser) {
          this.getAll();
          this.getProcessInfo();
          this.getMetaData();
        }
      });
    }

  }
  setExpand(column) {
    // this.expandedElement = { column: column }
    console.log(column)
    this.expandColumn = column;
  }
  changeView(type) {
    if (type === 'tableView') {
      this.istableView = true;
    } else {
      this.istableView = false;
    }
  }
  setTableViewType() {
    setTimeout(() => {
      // this.setTableViewType();
      let d = document.getElementsByTagName('tr');
      // console.log(d);
      const tableViewType = this.currentView.tableViewType;
      if (tableViewType === 1) {

      } else if (tableViewType === 2) {
        for (let i = 0; i < d.length; i++) {
          if (i % 2 === 0) {
            d[i].style.background = '#d2d2d247';
          } else {
            d[i].style.background = '#fff';
          }
          d[i].style.border = 'none';
        }
      } else {
        for (let i = 0; i < d.length; i++) {
          let td;
          if (i == 0) {
            td = document.getElementsByTagName('th');
          } else {
            td = document.getElementsByTagName('td');
          }
          for (let i = 0; i < td.length; i++) {
            td[i].style.borderRight = '1px solid #e0e0e0';
          }
        }
      }
    }, 200);

  }

  getProcessInfo() {
    this.commonApiService.getDataTableInfo(this.currentView.id).subscribe(res => {
      if (res.statusCode === 200) {
        this.dataTableInfo = res.info;
        // console.log(this.dataTableInfo)
      }
    });

  }
  style: any;

  onResizebtn(event: ResizeEvent): void {
    this.style = {
      width: `${event.rectangle.width}px`,
      height: `${event.rectangle.height}px`
    };
  }
  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.currentView.dataTableName;
    this.putBody['id'] = e['rowId'];
    const rowId = e['rowId'];
    if (e.isValid) {
      let fieldList;

      this.lastmodifiedListenerService.fieldList.subscribe(res => {
        fieldList = res;
      });
      if (!!fieldList.length) {
        let temp;
        this.lastmodifiedListenerService.dataSource.subscribe(res => {
          temp = res;
        });
        fieldList.forEach(i => {
          let userInfo = { 'userName': this.currentUser.userName, 'id': this.currentUser.id + '', 'email': this.currentUser.email }
          temp['isTableView'][rowId][i] = [userInfo];
          this.putBody[i] = this.currentUser.id + '';
        });
        this.lastmodifiedListenerService.dataSource.next(temp);
      }
      if (!!e.autoFill) {
        e.autoFill.forEach(i => {
          this.putBody[i] = e.rowData[i];
        });
      }
      this.commonApiService.update(this.putBody).subscribe(res => {
        if (res.status === 202) {
          this.data.forEach(i => {
            if (i.id === e['rowId']) {
              i[key] = e[key];
              this.dblClick = '';

            }
          });
          if (!!e.file) {
            this.commonApiService.fileCreate(e.file).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.toastService.success(res.message);
              }
            });
          } else {
            this.toastService.success(res.message);
          }
        }
      });
    } else {
      this.toastService.success('inValid Value');
    }
    this.putBody = {};
  }

  scrolTri(event) {
    let element = document.getElementById('city');
    // console.log(element.getBoundingClientRect().x);
  }

  searchFieldSelected(item) {
    this.apiObj.search = {};
    this.apiObj.search[item.name] = '';
    this.selectedSearchField = item;
    setTimeout(() => {
      document.getElementById(item.name).scrollIntoView({
        behavior: 'smooth', block: 'center', inline: 'center'
      });

    }, 100);
  }
  onResizeEnd(event: ResizeEvent, columnName): void {
    if (event.edges.right) {
      const cssValue = event.rectangle.width + 'px';
      const className = 'mat-column-' + columnName;
      const columnElts = document.getElementsByClassName(className);
      for (let i = 0; i < columnElts.length; i++) {
        const currentEl = columnElts[i] as HTMLDivElement;
        currentEl.style.width = cssValue;
        if (!!localStorage[this.currentView.name]) {
          let value = JSON.parse(localStorage[this.currentView.name]);
          if (!!value[className]) {
            value[className].width = cssValue;
          } else {
            Object.assign(value, { [className]: { width: cssValue } })
          }
          localStorage.setItem(this.currentView.name, JSON.stringify(value))
        } else {
          let value = { [className]: { width: cssValue } };
          localStorage.setItem(this.currentView.name, JSON.stringify(value));
        }
      }
    }
  }

  onResizeBottom(event, column) {
    if (event.edges.bottom) {
      const cssValue = event.rectangle.height + 'px';
      const className = 'mat-row';
      const columnElts = document.getElementsByClassName(className);
      for (let i = 0; i < columnElts.length; i++) {
        const currentEl = columnElts[i] as HTMLDivElement;
        currentEl.style.minHeight = cssValue;
        if (!!localStorage[this.currentView.name]) {
          let value = JSON.parse(localStorage[this.currentView.name]);
          if (!!value[className]) {
            value[className].minHeight = cssValue;
          } else {
            Object.assign(value, { [className]: { minHeight: cssValue } })
          }
          localStorage.setItem(this.currentView.name, JSON.stringify(value))
        } else {
          let value = { [className]: { minHeight: cssValue } };
          localStorage.setItem(this.currentView.name, JSON.stringify(value));
        }
      }
    }
  }

  createNewItem(item) {
    this.createModelDialog = this.dialog.open(item, {
      width: '98%',
      height: '98%',
      autoFocus: false
    });
  }
  // detailed View
  viewSelected(item, val, rowId, row) {
    this.routingNavigate.navigate(['/single-view', this.currentView.id
      , this.currentView.dataTableName, rowId]);

    this.infoData = { value: val, rowId: rowId, inputValue: val };
    let temp = {};
    this.metaData.forEach(i => {
      let advanced, computational;
      let lastMod = [];
      temp['isView'] = {};
      let f = [];
      this.computationListenerService.dataSource.subscribe(res => {
        f = res;
      });
      if (!!i.computational.length) {
        computational = JSON.parse(i.computational);
        if (computational.name === 'lastModifiedBy') {
          // lastMod.push(i.name);
          let fieldList;
          // this.lastmodifiedListenerService.field.next(fieldList);
          this.lastmodifiedListenerService.fieldList.subscribe(res => {
            fieldList = res;
          });
          this.lastmodifiedListenerService.dataSource.subscribe(res => {
            temp = res;
          });
          temp['isView'] = temp['isView'] ? temp['isView'] : {};
          const list = Object.keys(val);
          // console.log(list, lastMod)
          list.forEach(k => {
            if (fieldList.includes(k)) {
              temp['isView'][i.name] = val[k];
              // temp['isTableView'][rowId][i.name] = val[k];
              this.lastmodifiedListenerService.dataSource.next(temp);
            }
            // console.log(temp)
          });
        }
      }
    });

    // this.createModelDialog = this.dialog.open(item, {
    //   width: '98%',
    //   height: '98%',
    //   autoFocus: false,
    //   disableClose: true
    // });
  }

  isSelectAllCheck(e) {
    this.isAllSelected = e.checked;
    this.data.forEach(i => {
      i.checked = e.checked;
    });
  }

  isSelectCheck(e) {
    if (!e) { this.isAllSelected = false; }
    let totalCount = 0;
    let checkedCount = 0;
    this.data.forEach(i => {
      if (i.checked) { checkedCount++; }
      totalCount++;
    });
    if (totalCount === checkedCount) { this.isAllSelected = true; }
  }

  getAll() {
    this.isLoader = true;
    this.apiObj.field = '';
    this.apiObj.fieldValue = '';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        let count = 1;
        res.info.forEach(i => {
          i.select = count;
          i.checked = false;
          i.sticky = false;
          count++;
        });
        this.data = res.info;
        // console.log(this.data)
        this.dataSource = new MatTableDataSource(this.data);
        this.isLoader = false;
        // this.row = document.getElementsByTagName('tr');
        // console.log(this.row, this.row.length)
        // this.getMetaData();
        // this.setTableViewType();
      }
    });
  }
  setOrder() {
    let temp = [];
    this.metaData = [];
    this.response.forEach(i => {
      if (i.tableViewPosition !== -1) {
        this.metaData.push(i);
      } else
        if (i.tableViewPosition === -1) {
          temp.push(i);
        }
    });
    temp.forEach(j => {
      this.metaData.push(j);
    });
  }
  response = [];
  subTableList = [];
  getMetaData() {
    let tempMetaData = [];
    let tempGridMetaData = [];
    this.isLoader = true;
    this.metaDataLoader = true;
    this.columnsToDisplay = [];
    this.metaData = [];
    this.apiObjMeta.tableName = 'meta_data';
    this.apiObjMeta.field = 'data_table_id';
    this.apiObjMeta.fieldValue = this.currentView.id;
    // this.apiObjMeta.filterValue = { table_view_position: [''] }
    // this.apiObjMeta.
    this.metaData = [];
    // console.log(this.apiObjMeta)
    this.commonApiService.getAll(this.apiObjMeta).subscribe(res => {
      if (res.statusCode === 200) {
        this.response = res.info;
        let computational;
        // temp['isTableView'] = {};
        let lastMod = [];
        let temp1 = {};
        temp1['isTableView'] = {};
        this.columnsToDisplay.push('select');
        res.info.forEach(i => {
          i.name = this.commonService.toCamelCase(i.name);
          if (i.type) {
            if (i.tableViewPosition !== -1) {
              i.checked = true;
              i.sticky = false;
              this.columnsToDisplay.push(i.name);
              this.searchMetaData.push(i);
            } else {
              i.sticky = false;
              i.checked = false;
              // this.columnsToDisplay.push(i.name);
            }
            this.expandColumn = i;
            if (i.gridFieldViewPosition !== -1) {
              this.gridViewMetaData[i.gridFieldViewPosition] = i;
              this.gridViewFieldToDisplay[i.gridFieldViewPosition] = i.name;
            }
            this.metaData.push(i);
            this.gridMetaData.push(i);
            this.setOrder();
            // this.columnsToDisplay.push(i.name);
            if (!!i.computational.length) {
              computational = JSON.parse(i.computational);
              // console.log(computational.name)
              if (computational.name === 'lastModifiedBy') {
                lastMod.push(i.name);
                let fieldList = lastMod;
                this.lastmodifiedListenerService.field.next(fieldList);
                this.lastmodifiedListenerService.dataSource.subscribe(res => {
                  temp1 = res;
                });
                temp1['isTableView'] = temp1['isTableView'] ? temp1['isTableView'] : {}
                this.data.forEach(j => {
                  const list = Object.keys(j);
                  const rowId = j.id;
                  // console.log(j, list, lastMod)
                  temp1['isTableView'][rowId] = {};
                  list.forEach(k => {
                    if (lastMod.includes(k)) {
                      temp1['isTableView'][rowId][i.name] = j[k]
                    }
                  });
                  this.lastmodifiedListenerService.dataSource.next(temp1);
                });
              }
            }
            // if (!!i.isAdvanced && !!(i.isAdvanced.length > 1)) {
            //   advanced = JSON.parse(i.isAdvanced);
            //   if (advanced.type === 'depended') {
            //     this.advancedFieldListenerService.dataSource.subscribe(res => {
            //       temp = res;
            //     });
            //     this.data.forEach((j, index) => {
            //       i['rowData'] = j;
            //       temp['isTableView'][j.id] = !!temp['isTableView'][j.id] ? temp['isTableView'][j.id] : {};
            //       let name = this.commonService.toCamelCase(advanced.dependedField);
            //       if (typeof (j[name]) !== 'string') {
            //         let fname = this.commonService.toCamelCase(advanced.filterValue)
            //         if (j[name][0] && j[name][0][fname]) {
            //           temp['isTableView'][j.id][name] = j[name][0][fname];
            //         } else {
            //           temp['isTableView'][j.id][name] = '';
            //         }
            //       } else {
            //         temp['isTableView'][j.id][name] = j[name];
            //       }
            //     });
            //   }
            // }
            // console.log(advanced)
            // console.log(temp);
            // this.advancedFieldListenerService.dataSource.next(temp);
          } else {
            this.subTableList.push(i.name)
          }
        });
        this.columnsToDisplay.push('action');
        // this.metaData.forEach(k => {
        //   this.data.forEach(l => {
        //     k['rowData'] = l;
        //   });
        // });
        // console.log()
        // res.info.forEach(i => {
        //   if (!!i.computational.length) {
        //     this.lastmodifiedListenerService.data.subscribe(res => {
        //       temp1 = res;
        //     });
        //     computational = JSON.parse(i.computational);
        //     if (computational.name === 'lastModifiedBy') {
        //       lastMod.push(i.name)
        //     }
        //     temp1['isTableView'] = temp1['isTableView'] ? temp1['isTableView'] : {}
        //     this.data.forEach(j => {
        //       const list = Object.keys(j);
        //       const rowId = j.id;
        //       temp1['isTableView'][rowId] = {};
        //       list.forEach(k => {
        //         if (lastMod.includes(k)) {
        //           temp1['isTableView'][rowId][i.name] = j[k];
        //         }
        //       });
        //     });
        //     this.lastmodifiedListenerService.dataSource.next(temp1);
        //     // console.log(temp1)
        //   }
        // });
        // res.info.forEach(i => {
        //   if (i.type !== 'table') {
        //     i.checked = true;
        //     i.sticky = false;
        //     let f = 0;
        //     i.name = this.commonService.toCamelCase(i.name);
        //     let isNewItem = true;
        //     if (!!this.tempMetaData[0]) {
        //       for (let j = 0; j < this.tempMetaData.length; j++) {
        //         if (i.name === this.tempMetaData[j].name) {
        //           f = 0;
        //           this.metaData[j] = i;
        //           break;
        //         } else {
        //           f = 1;
        //         }
        //       }
        //       if (!!f) {
        //         this.metaData.push(i);
        //       }
        //     } else {
        //       this.metaData.push(i);
        //     }
        //   }
        // });
        // this.metaData = res.info;
        this.isLoader = false;
        this.metaDataLoader = false;
      }
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event.previousIndex, event.currentIndex)
    moveItemInArray(this.columnsToDisplay, event.previousIndex + 1, event.currentIndex);
    moveItemInArray(this.metaData, event.previousIndex, event.currentIndex);
    // moveItemInArray(this.columnsToDisplay, event.previousIndex, event.currentIndex);
    // console.log(this.tempColumnsToDisplay, event);
    this.customViewSave();
  }

  addItemList(e, item, i) {
    if (item.checked) {
      this.columnsToDisplay.splice(i, 0, item.name);
      item.tableViewPosition = this.columnsToDisplay.length;
    } else {
      this.columnsToDisplay.splice(this.columnsToDisplay.indexOf(item.name), 1);
      item.tableViewPosition = -1;
    }
    this.setOrder();
    this.customViewSave();
  }

  customViewSave() {
    if (!!localStorage.tableView) {
      let t = JSON.parse(localStorage.tableView);
      t[this.currentView.dataTableName] = JSON.stringify(this.metaData);
      localStorage.setItem('tableView', JSON.stringify(t));
    } else {
      let t = {};
      t[this.currentView.dataTableName] = JSON.stringify(this.metaData);
      localStorage.setItem('tableView', JSON.stringify(t));
    }
  }
  test(event) {
    this.metaData.forEach(i => {
      let obj = document.getElementById(i.name).style;
      obj.color = '';
    })
    if (!!this.apiObj.sortType && !!this.apiObj.sortValue) {
      let style = getComputedStyle(document.body);
      let obj = document.getElementById(this.apiObj.sortValue).style;
      obj.color = style.getPropertyValue('--color-primary');
    } else {
      let obj = document.getElementById(this.apiObj.sortValue).style;
      obj.color = '';
    }
  }
  deleteItem(id, index, item) {
    this.isLoader = true;
    // let temp = this.data;
    // this.data = [];
    // temp.splice(index, 1);
    // if (this.isDesignation) {
    //   this.designationService.delete(id + '::' + item.designation).subscribe(res => {
    //     if (res.status === 200) {
    //       this.data.splice(index, 1);
    //       this.toastService.success(res.message);
    //       this.isLoader = false;
    //     }
    //   });
    // } else {
    this.commonApiService.delete(id, this.currentView.dataTableName).subscribe(res => {
      if (res.status === 200) {
        this.data.splice(index, 1);
        this.dataSource = new MatTableDataSource(this.data);
        this.toastService.success(res.message);
        this.isLoader = false;
      }
    });
  }
  // }

  // Open Filter
  openFilter() {
    // this.filterOpen = !this.filterOpen;
    this.filterOpen = true;
  }
}
