import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { StudentService } from '../services/student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-list-view',
  templateUrl: './student-list-view.component.html',
  styleUrls: ['./student-list-view.component.css']
})
export class StudentListViewComponent implements OnInit {
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  currentUser = localStorage.flowPodUser ? JSON.parse(localStorage.flowPodUser) : '';
  currentView = JSON.parse(localStorage.currentView)
  studentList = [];
  classList = [];
  selectedClass: any
  constructor(public commonApiService: CommonApiService,
    public studentService: StudentService,
    private routingNavigate: Router) { }

  ngOnInit() {
    // this.getStudentInfo();
    this.getHandlingClass();
  }

  getHandlingClass() {

    this.studentService.getHandlingClassess(this.currentUser.id, 'unique').subscribe(res => {
      if (res.statusCode === 200) {
        this.classList = res.info;
        if (!!res.info.length) {
          this.selectedClass = res.info[0];
          this.getStudentInfo(res.info[0]);
        }
      }
    });
  }
  getStudentInfo(item) {
    this.studentList = [];
    let getBody = {
      degree: item['degree'][0].id + '',
      department: item['department'][0].id + '',
      section: item['section'],
      semester: item['semester'],
      academic_year: item['academicYear'][0].id + '',
      batch: item['batch'][0].id + ''
    };
    getBody['userId'] = this.currentUser.id;
    this.studentService.getStudentsInfo(getBody).subscribe(res => {
      if (res.statusCode) {
        this.studentList = res.info;
      }
    });
  }
  inputData = {}
  selectedView(student) {
    this.routingNavigate.navigate(['/single-view', this.currentView.id
      , this.currentView.dataTableName, student.id]);
    this.inputData = { value: student };


  }
}
