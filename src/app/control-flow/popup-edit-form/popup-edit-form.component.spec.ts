import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupEditFormComponent } from './popup-edit-form.component';

describe('PopupEditFormComponent', () => {
  let component: PopupEditFormComponent;
  let fixture: ComponentFixture<PopupEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupEditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
