import { Component, OnInit, ViewEncapsulation, Inject, Output, ViewChild, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { dataType, checkListBased, stringBased, toggleBased, fileBased, iconList } from '../constant';
import { validation } from '../constant';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormBuilderApiService } from '../services/form-builder-api.service'
import { CommonApiService } from '../../services/common-api.service';
import { ImageComponent } from '../form-fields/image/image.component';
import { DataTableService } from '../services/data-table.service';
import { DataShareService } from '../services/data-share.service';
import { ToastService } from '../../shared/common/toast.service';
import { ValidationsServicesService } from '../form-fields/validations-services/validations-services.service';

export interface DialogData {
  item: any
}

@Component({
  selector: 'app-popup-edit-form',
  templateUrl: './popup-edit-form.component.html',
  styleUrls: ['./popup-edit-form.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupEditFormComponent implements OnInit {
  @ViewChild('duplicate') duplicate: any;
  @Output() fieldCreated = new EventEmitter();
  validation = validation
  selector: any;
  dataType = dataType;
  selectItemIndex: number;
  fieldValue: any;
  isValidate: boolean = false;
  componentValueInput = {};
  checkListBased = checkListBased;
  stringBased = stringBased;
  toggleBased = toggleBased;
  fileBased = fileBased;
  temp: any;
  optionColor = [];
  after: number;
  before: number;
  isAdvance: boolean = false;
  attachmentDefualValue: any;
  // Icons
  iconList = iconList;
  materialIconList = [];
  filterIconList = [];
  dataTable: any;

  //Attachment Files
  attachments = { fileName: '', file: '' };
  dataTableName = [];
  tableFields = [];
  isUpdate: boolean = false;
  isTableFieldView: boolean = false;
  data: any;
  groupList = [];
  dialogisUnique: any;
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}, requestFields: ''
  };
  isLoading: boolean = false;
  isComputational: boolean = false;

  isShowHide: boolean = false;
  // Slider
  // sliderValue
  tableInfo = [];
  sectionTableInfo: any;
  sectionTableType = 0;
  isCreatedTable = false;
  tempOptionValue: any;
  fieldList = [];
  filterAdvanced = {
    type: 'depended',
    dependedField: '',
    dataTableName: '',
    filterValue: '',
    fieldName: '',
    filterType: 'equalTo'
  };
  filterList = ['equalTo', 'notEqualTo'];
  multiUserSelect = '0';
  constructor(public dialogRef: MatDialogRef<PopupEditFormComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: DialogData, private _sanitizer: DomSanitizer,
    public formBuilderApiService: FormBuilderApiService, public commonApiService: CommonApiService,
    public dataShareService: DataShareService, public toastService: ToastService,
    public dialog: MatDialog,
    public dataTableService: DataTableService,
    public validationsServicesService: ValidationsServicesService) {

    this.iconList.categories.forEach(i => { i.icons.forEach(j => { this.materialIconList.push(j.id); }); });
    this.filterIconList = this.materialIconList;
  }


  ngOnInit() {
    this.data = this.dialogData;
    this.selector = JSON.parse(JSON.stringify(this.data.item));
    this.isUpdate = this.data.isUpdate;
    this.dataTable = this.data;
    this.groupList = this.data.groupList;

    if (!this.isUpdate) { this.feildIndex(this.selector.displayValue); }
    this.selector['position'] = this.data.position;
    this.selector['tableViewPosition'] = this.data.tableViewPosition;
    this.selector['groupName'] = this.data.item.groupName;
    this.selector['groupId'] = this.data.item.groupId;
    this.componentValueInput['isCreate'] = 1;
    this.componentValueInput['isValidation'] = 0;
    this.componentValueInput['inputValue'] = {};
    this.componentValueInput['dataTableName'] = this.data.dataTable.dataTableName;
    this.componentValueInput['data'] = this.selector;
    this.componentValueInput['fieldViewType'] = 1;
    if (!!this.selector.computational) {
      this.componentValueInput['data']['computational'] = this.selector.computational.length ? JSON.parse(this.selector.computational) : [];
    }
    // Is update a value
    if (!!this.isUpdate) {
      this.componentValueInput['validationArray'] = this.selector.validations.length ? JSON.parse(this.selector.validations) : [];
      this.tempOptionValue = this.selector.optionValue;
      !(this.selector.isAdvanced.length > 1) ? this.selector.optionValue = this.selector.optionValue.split(',') : ''
    }
    this.eventListenTOallCComponent();
    if (!!this.selector.isAdvanced) {
      this.getDataTable();
      this.selector.isAdvanced = JSON.parse(this.selector.isAdvanced);
    }
    this.componentValueInput['data']['isAdvanced'] = JSON.stringify(this.selector.isAdvanced);
    // this.selector.isAdvanced = JSON.stringify(this.selector.isAdvanced);
  }
  getTableInfo(val) {
    this.tableInfo = [];
    this.apiObj.tableName = 'data_tables';
    this.apiObj.search = { name: val };
    this.apiObj.limit = 100;
    this.apiObj['requestFields'] = 'id,data_table_name,name';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.tableInfo = res.info;
      }
    });
  }
  test(item, index) {
    if (!!item.id) {
      if (!this.filterAdvanced['referedFields']) {
        this.filterAdvanced['referedFields'] = [];
      }
      this.filterAdvanced['referedFields'].splice(index, 1, item.id + '');
    }
  }
  displayTableName(val) {
    if (!!val) {
      return val.name;
    }
  }
  changeType(value) {
    if (value == '0') {
      this.selector['isAdvanced']['userSelectionType'] = 'single';
    } else if (value == '1') {
      this.selector['isAdvanced']['userSelectionType'] = 'multiple';
    }
  }
  displayValue(val) {
    return !!val ? val.name : '';
  }
  feildIndex(event) {
    this.dataType.forEach((val, key) => {
      if (val.displayValue === event) {
        val['rowNo'] = this.selector.rowNo;
        val['width'] = this.selector.width;
        val['groupName'] = this.selector.groupName;
        val['groupId'] = this.selector.groupId;
        this.selectItemIndex = key;
        this.selector = JSON.parse(JSON.stringify(val));
        this.componentValueInput['isCreate'] = 1;
        this.componentValueInput['data'] = this.selector;
        this.componentValueInput['validationArray'] = [];
        if (this.selector.type === 'user') {
          this.selector['isAdvanced'] = {};
          this.selector['isAdvanced']['userSelectionType'] = 'single';
        }
      }
    });
  }

  ValidationView(view) {

    if (view === 'settings') {
      this.isValidate = false;
      this.isAdvance = false;
      this.isComputational = false;
      this.isShowHide = false;
    } else if (view === 'validation') {
      this.isValidate = true;
      this.componentValueInput['isValidation'] = 1;
      this.componentValueInput['data'] = this.selector;
      this.isAdvance = false;
      this.isComputational = false;
      this.isShowHide = false;

    } else
      if (view === 'advanced') {
        this.isAdvance = true;
        this.isComputational = false;
        this.selector['isAdvanced'] = ' ';
        this.isShowHide = false;
        this.getDataTable();
      } else if (view === 'computational') {
        this.isComputational = true;
        this.isValidate = false;
        this.isAdvance = false;
        this.isShowHide = false;
      } else {
        this.isShowHide = true;
        this.isValidate = false;
        this.isAdvance = false;
        this.isComputational = false;
      }
  }
  getDataTable() {
    this.dataTableName = [];
    let getBody = {};
    getBody['tableName'] = 'data_tables';
    getBody['field'] = 'name';
    getBody['page'] = 0;
    getBody['limit'] = 100;
    getBody['sortType'] = 'ASC';
    getBody['sortValue'] = 'name';
    getBody['fieldValue'] = '';
    this.commonApiService.getAll(getBody).subscribe(res => {
      (res.info).forEach(i => {
        this.dataTableName.push(i)
      });
    });

  }
  getMetaData() {
    // this.tableFields = [];
    this.apiObj.tableName = 'meta_data';
    // this.apiObj.field = 'data_table_name';
    this.apiObj.filterValue = { 'data_table_name': [this.data.dataTable.dataTableName] };
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.info.length) {
        this.fieldList = res.info;
      }
      this.apiObj.filterValue = {};
    });
  }
  addAutoFillList() {
    if (!this.filterAdvanced['autoFillFields']) {
      this.filterAdvanced['autoFillFields'] = [];
    }
    this.filterAdvanced['autoFillFields'].push({ masterField: '', referedField: '' });
    this.setautoFillFilter();
  }
  removeItem(i) {
    if (i !== 0) {
      this.filterAdvanced['autoFillFields'].splice(i, 1);
    }
    this.setautoFillFilter();
  }
  setautoFillFilter() {
    this.filterAdvanced['type'] = 'auto-fill';
    // console.log(this.filterAdvanced);
    this.selector['isAdvanced'] = JSON.stringify(this.filterAdvanced);
  }
  selectTableName(tableName) {
    this.tableFields = [];
    this.apiObj.tableName = 'meta_data';
    this.apiObj.field = 'data_table_name';
    this.apiObj.filterValue = {};
    this.apiObj.search = {};
    this.apiObj.requestFields = '';
    this.apiObj.fieldValue = tableName.replace(' ', '_');
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.info.length) {
        this.tableFields = res.info;
      }
    });
    this.isTableFieldView = true;
    this.apiObj.field = '';
    this.apiObj.fieldValue = '';
  }
  selectOption(tableName, fieldName) {
    this.selector['isAdvanced'] = JSON.stringify({ type: 'non-depended', 'dataTableName': tableName, 'fieldName': fieldName });
    this.componentValueInput['data']['optionValue'] = [];
    this.apiObj.tableName = tableName;
    this.apiObj.field = fieldName;
    this.apiObj.limit = 100;
    this.apiObj.page = 0;
    this.commonApiService.getDistinct(this.apiObj).subscribe(res => {
      if (!!res) {
        (res.info).forEach(i => {
          (this.componentValueInput['data']['optionValue']).push(i);
        });
      }
    });
  }
  selectOption2() {
    this.selector['isAdvanced'] = JSON.stringify(this.filterAdvanced);
  }

  isRequire(event) {
    this.selector.validationArray = [];
    if (!event) {
      this.selector.displayValue = this.selector.displayValue.replace('*', '');
    } else {
      if (this.selector.displayValue.length) {
        this.selector.displayValue = this.selector.displayValue + '*';
      }
    }
  }
  isRequiredCheck() {
    if (this.selector.displayValue.length && this.selector.required) {
      this.selector.required = true;
    } else {
      this.selector.required = false;
    }
  }

  checkIsEmpty(checked) {
    if (checked) {
      this.commonApiService.getCount(this.data.dataTable.dataTableName).subscribe(res => {
        if (res['count'] > 0) {
          this.dialogisUnique = this.dialog.open(this.duplicate, {
            width: '600px',
          });
        }
      });
    }
  }
  deleteTableData() {
    this.commonApiService.deleteTableData(this.data.dataTable.dataTableName).subscribe(res => {
      this.dialogisUnique.close();
    });
  }
  ingore() {
    this.selector.isunique = 0;
    this.selector.required = 0;
    this.selector.displayValue.replace('*', '');
  }
  closeModel() {
    this.dialogRef.close();
  }
  setDefaultDate(type, val) {
    // this.validationsServicesService.setDefaultDate({ type: type, val: val });
    this.selector.defaultValue = {
      'type': type, 'value': val,
      'date': this.validationsServicesService.setDefaultDate(JSON.stringify({ type: type, val: val }))
    };
  }

  setDefaultDateTime(type) {
    this.selector.defaultValue = { 'type': type, 'datetime': new Date() };
  }

  save() {
    this.isLoading = true;
    const temp = JSON.parse(JSON.stringify(this.selector));
    if (temp.type === 'table') {
      // let randomName = Math.random().toString(36).substr(2, 10);
      let randomName = 'c' + new Date().getTime() + 'table';
      if (this.sectionTableType === 1) {
        temp['section_table_id'] = this.sectionTableInfo['id'];
        temp['displayValue'] = randomName;
        temp['name'] = randomName;
        this.saveUpdate(temp);
      } else {
        const createDataTable = { name: randomName, description: '', icon: '', categories: '', is_subtable: 1 };
        this.dataTableService.createDataTables(createDataTable).subscribe(res => {
          if (!!res.info) {
            temp['section_table_id'] = res.info.id;
            temp['displayValue'] = randomName;
            temp['name'] = randomName;
            temp['is_new_table'] = 1;
            this.isCreatedTable = true;
            this.saveUpdate(temp);
          }
        });
      }
    } else {
      this.saveUpdate(temp);
    }
  }
  saveUpdate(temp) {

    temp['showHideFields'] = !!temp['showHideFields'] ? JSON.stringify(temp['showHideFields']) : '';
    temp['optionValue'] = temp['optionValue'] ? temp['optionValue'] + '' : '';
    if (this.checkListBased.includes(temp['type'])) {
      temp['colorCodes'] = this.optionColor + '';
    }
    temp['optionValue'] = !!temp['isAdvanced'] && (temp['isAdvanced'].length > 1) ? '' : temp['optionValue'];
    if (temp.type !== 'user') {
      temp['isAdvanced'] = !!temp['isAdvanced'] ? temp['isAdvanced'] + '' : '0';
    }
    temp['validations'] = Object.keys(temp).includes('validationArray') ? JSON.stringify(temp['validationArray']) : '';
    temp['computational'] = temp['computational'] ? JSON.stringify(temp['computational']) : '';
    delete temp['validationArray'];
    temp['dataTableName'] = this.dataTable.dataTable.name;
    temp['dataTableId'] = this.dataTable.dataTable.id;
    temp['required'] = +temp['required'];
    temp['isunique'] = (+temp['isunique']) + '';
    delete temp['value'];
    if (temp['type'] === 'user') {
      temp['isAdvanced'] = JSON.stringify(temp['isAdvanced']);
    }
    if (temp['type'] === 'toggle') {
      temp['defaultValue'] = (+temp['defaultValue']) + '';
    }
    if (temp['type'] === 'image' || temp['type'] === 'attachment') {
      temp['defaultValue'] = !!this.attachments['fileName'].length ? this.attachments['fileName'] : '';
    }
    if (temp['type'] === 'datetime' || temp['type'] === 'date') {
      // temp['defaultValue'] = (+temp['defaultValue']) + '';
      if (!!temp['defaultValue'][temp['type']].length) {
        temp['defaultValue'] = JSON.stringify(temp['defaultValue']);
      } else {
        temp['defaultValue'] = '';
      }
    }
    if (this.isUpdate) {
      this.formBuilderApiService.updateMetaData(temp.id, temp).subscribe(res => {
        if (res.status === 202) {
          this.closeModel();
          this.toastService.success(res.message);
          this.dataShareService.dataSource.next({ created: true });
          this.fieldCreated.emit({ type: 'created' });
          if ((temp['type'] === 'image' || temp['type'] === 'attachment') && this.attachments['fileName'].length) {
            this.commonApiService.fileCreate(this.attachments).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.closeModel();
              }
            });
          }
          this.isLoading = false;
        } else if (res.statusCode == 500) {
          if (res.statusMessage.includes('Duplicate')) {
            this.dialog.open(this.duplicate, {
              width: '250px',
            });
          }
        }
      });

    } else {
      this.formBuilderApiService.createMetaData(temp).subscribe(res => {
        if (res.statusCode === 201) {
          this.closeModel();
          this.toastService.success(res.message);
          this.dataShareService.dataSource.next({ created: true });
          this.fieldCreated.emit({ type: 'created' });
          if ((temp['type'] === 'image' || temp['type'] === 'attachment') && this.attachments['fileName'].length) {
            this.commonApiService.fileCreate(this.attachments).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.closeModel();
              }
            });
          }
          this.isLoading = false;
        }
        if (res.statusCode === 203) {
          this.toastService.success(res.message);
        }
      });

    }
  }
  eventListenTOallCComponent() {
    this.dataShareService.data.subscribe(data => {
      if (!!data.image) {
        if (!!data.image.shape) {
          this.selector.cssClass = data.image.shape;
          this.dataShareService.image(data.image);
        } else if (!!data.image.file) {
          this.dataShareService.image(data.image.file);
        }
      }
    });
  }

  iconsFilter(val) {
    this.filterIconList = this.materialIconList.filter(i => i.includes(val))
  }
}
