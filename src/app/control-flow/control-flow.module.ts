import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilderComponent } from './form-builder/form-builder.component';
import { MaterialModule } from '../material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { PopupEditFormComponent } from './popup-edit-form/popup-edit-form.component';

import { TextComponent } from './form-fields/text/text.component';
import { NumberComponent } from './form-fields/number/number.component';
import { TelComponent } from './form-fields/tel/tel.component';
import { CheckboxComponent } from './form-fields/checkbox/checkbox.component';
import { EmailComponent } from './form-fields/email/email.component';
import { UrlComponent } from './form-fields/url/url.component';
import { PasswordComponent } from './form-fields/password/password.component';
import { TextareaComponent } from './form-fields/textarea/textarea.component';
import { DateComponent } from './form-fields/date/date.component';
import { TimeComponent } from './form-fields/time/time.component';
import { SelectComponent } from './form-fields/select/select.component';
import { ChecklistComponent } from './form-fields/checklist/checklist.component';
import { ToggleComponent } from './form-fields/toggle/toggle.component';
import { SliderComponent } from './form-fields/slider/slider.component';

import { AttachmentComponent } from './form-fields/attachment/attachment.component';
import { ImageComponent } from './form-fields/image/image.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationsComponent } from './form-fields/validations/validations.component';
import { FieldsMapComponent } from './fields-map/fields-map.component';
import { DatetimeComponent } from './form-fields/datetime/datetime.component';
import { CreateFlowComponent } from './create-flow/create-flow.component';
import { SharedModule } from '../shared/shared.module';
import { ResizableModule } from 'angular-resizable-element';
import { CreateflowModelComponent } from './create-flow/createflow-model/createflow-model.component';

import { ComputationFieldComponent } from './form-fields/computation-field/computation-field.component';
import { ComputationFieldViewComponent } from './form-fields/computation-field-view/computation-field-view.component';
import { AngularDraggableModule } from 'angular2-draggable';
import { AdvancedComponent } from './form-fields/advanced/advanced.component';
import { ShowhideComponent } from './form-fields/showhide/showhide.component';
import { TableSectionComponent } from './table-section/table-section.component';
import { GeneralProcessComponent } from './general-process/general-process.component';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './form-fields/user/user.component';
import { WorkFlowModule } from './work-flow/work-flow.module';

import { ViewWithinSectionTableComponent } from './view-within-section-table/view-within-section-table.component';



@NgModule({
  declarations: [FormBuilderComponent, PopupEditFormComponent,
    TextComponent, NumberComponent, TelComponent, CheckboxComponent,
    EmailComponent, UrlComponent, PasswordComponent, TextareaComponent,
    DateComponent, TimeComponent, SelectComponent, ChecklistComponent,
    ToggleComponent, SliderComponent, AttachmentComponent, ImageComponent,
    ValidationsComponent, FieldsMapComponent, DatetimeComponent, CreateFlowComponent,
    CreateflowModelComponent,
    ComputationFieldComponent,
    ComputationFieldViewComponent,
    AdvancedComponent,
    ShowhideComponent,
    TableSectionComponent,
    GeneralProcessComponent,
    UserComponent,
    ViewWithinSectionTableComponent,

  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ResizableModule,
    AngularDraggableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    BrowserModule,
    WorkFlowModule,
    // ViewModule
  ],
  exports: [FormBuilderComponent, CreateFlowComponent, FieldsMapComponent, CreateflowModelComponent],
  entryComponents: [PopupEditFormComponent, FormBuilderComponent, TextComponent,
    NumberComponent, TelComponent, CheckboxComponent, EmailComponent, UrlComponent,
    PasswordComponent, TextareaComponent, DateComponent, TimeComponent, SelectComponent,
    ChecklistComponent, ToggleComponent, SliderComponent, AttachmentComponent, ImageComponent,
    CreateFlowComponent, CreateflowModelComponent, TableSectionComponent]
})
export class ControlFlowModule { }
