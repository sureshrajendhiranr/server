import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-create-work-flow',
  templateUrl: './create-work-flow.component.html',
  styleUrls: ['./create-work-flow.component.css']
})
export class CreateWorkFlowComponent implements OnInit {
  @Input('inputData') dataTable: any;
  dialogRef: any;
  constructor(public dialog: MatDialog) { }
  steps = [];
  tempStep: any;
  stepInfo = {};
  ngOnInit() {
  }
  openDialog(templateName) {
    this.dialogRef = this.dialog.open(templateName, {
      width: '60%',
      autoFocus: false,
      disableClose: true,
    });
  }
  assginOwnerEvent(event) {
    if (!!event.close) {
      this.dialogRef.close();
    }

  }
  assginTo(templateName, item) {
    if (!!item) {
      this.stepInfo = item;
      this.stepInfo['method'] = 'edit';
    } else {
      this.stepInfo['method'] = 'add';
    }
    this.dialogRef = this.dialog.open(templateName, {
      width: '40%',
      autoFocus: false,
      disableClose: true,
    });
  }
  setStepInfo() {
    console.log(this.tempStep)
    if (this.tempStep.method === 'add') {
      this.steps.push(this.tempStep);
    }
    // this.tempStep = {};
    this.dialogRef.close();
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.steps, event.previousIndex, event.currentIndex);
  }
}
