import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class WorkFlowService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  //Search User name and designation
  SearchUserAndDesignation(obj): Observable<any> {
    const params = new HttpParams()
      .set('q', obj.q)
      .set('limit', obj.limit);
    const requestUrl = this.baseUrl + '/userAndDessignationSearch/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  workFlowAdminGet(obj): Observable<any> {
    const params = new HttpParams()
      .set('q', obj.q);
    const requestUrl = this.baseUrl + '/workFlowAdmin/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  workFlowAdminSave(body): Observable<any> {
    const requestUrl = this.baseUrl + '/workFlowAdmin/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

  getDataTable(id): Observable<any> {
    const requestUrl = this.baseUrl + '/editDataTableGet/' + id;
    return this.http.get(requestUrl).pipe(map(this.extractData));
  }


}
