import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableFieldOrderComponent } from './table-field-order.component';

describe('TableFieldOrderComponent', () => {
  let component: TableFieldOrderComponent;
  let fixture: ComponentFixture<TableFieldOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableFieldOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableFieldOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
