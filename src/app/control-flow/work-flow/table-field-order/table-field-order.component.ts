import { Component, OnInit, Input } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ToastService } from '../../../shared/common/toast.service';

@Component({
  selector: 'app-table-field-order',
  templateUrl: './table-field-order.component.html',
  styleUrls: ['./table-field-order.component.css']
})
export class TableFieldOrderComponent implements OnInit {
  // currentView = JSON.parse(localStorage.currentView)
  @Input('inputData') currentView: any
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'table_view_position',
    field: '', fieldValue: '', search: {}, filterValue: {},
  };
  metaData = [];
  displayColumns = [];
  response = [];
  constructor(public commonApiService: CommonApiService, public toastService: ToastService) { }
  ngOnInit() {
    this.getAllFields();
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.metaData, event.previousIndex, event.currentIndex);
    if (this.metaData[event.currentIndex].tableViewPosition === -1) {
      //if we moved the unchecked field
      moveItemInArray(this.metaData, event.currentIndex, event.previousIndex);
    } else {
      let putBody = [];
      let currentFieldPosition = this.metaData.indexOf(this.metaData[event.currentIndex]);
      for (let i = 0; i < this.metaData.length; i++) {
        if (this.metaData[i].tableViewPosition !== -1) {
          this.metaData[i].tableViewPosition = i;
          let temp = {}
          temp['tableName'] = 'meta_data';
          temp['id'] = this.metaData[i].id;
          temp['tableViewPosition'] = i;
          putBody.push(temp);
        }
      }
      this.commonApiService.update(putBody).subscribe(res => {
        if (res.status === 202) {
          // this.setOrder();
          this.toastService.success(res.message);
        }
      });
    }
  }

  getAllFields() {
    this.apiObj.tableName = 'meta_data';
    this.apiObj.field = 'data_table_id';
    this.apiObj.fieldValue = this.currentView.id;
    let temp = [];
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      // this.metaData = res.info;
      this.response = res.info;
      this.setOrder();

    });
  }
  changeView(item, checked, index) {
    let putBody = {};
    putBody['tableName'] = 'meta_data';
    putBody['id'] = item.id;
    if (checked) {
      putBody['tableViewPosition'] = index;
      item.tableViewPosition = index;
    } else {
      putBody['tableViewPosition'] = -1;
      item.tableViewPosition = -1;
    }
    this.commonApiService.update(putBody).subscribe(res => {
      if (res.status === 202) {
        this.toastService.success(res.message);
        // this.setOrder();
        this.getAllFields();
      }
    });
  }
  setOrder() {
    let temp = [];
    this.metaData = [];
    this.response.forEach(i => {
      if (i.tableViewPosition !== -1) {
        this.metaData.push(i);
      } else
        if (i.tableViewPosition === -1) {
          temp.push(i);
        }
    });
    temp.forEach(j => {
      this.metaData.push(j);
    });
  }
}
