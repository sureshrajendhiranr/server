import { Component, OnInit, Input } from '@angular/core';
import { WorkFlowService } from '../service/work-flow.service';
import { CommonApiService } from '../../../services/common-api.service';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-notification-config',
  templateUrl: './notification-config.component.html',
  styleUrls: ['./notification-config.component.css']
})
export class NotificationConfigComponent implements OnInit {
  @Input('inputData') inputData: any;
  userAndDesignationList = [];
  apiObj = {
    limit: 100,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    field: 'data_table_id',
    fieldValue: '',
    search: {},
    filter: {}
  };
  data = [];
  columnList = [];
  notificationPostObj = {
    userId: [],
    designationId: [],
    create: 0,
    update: 0,
    delete: 0,
    dataTableId: 0,
    createDescription: 0,
    columnId: 0,
    updateDescription: '',
    deleteDescription: '',
    onClick: true
  };
  errors = {
    selectPerson: '',
    column: '',
    operation: ''
  };
  userOnclick = false;
  constructor(
    private workFlowService: WorkFlowService,
    private commonApiService: CommonApiService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.apiObj.fieldValue = this.inputData['id'];
    this.get();
    this.getColumn();
  }

  addNewRole() {
    const len = this.data.length - 1;
    const body = {
      tableName: 'Notifications_Config',
      data_table_id: this.inputData['id'], created_by: JSON.parse(localStorage.flowPodUser).id
    };
    if (!(len < 0)) {
      if (!this.data[len]['userId'].length && !this.data[len]['designationId'].length) {
        this.errors.selectPerson = 'select a persons';
      }
      if (this.data[len]['columnId'] == '0') {
        this.errors.column = 'select a column'
      }
      if (this.data[len]['create'] == 0 && this.data[len]['update'] == 0 && this.data[len]['delete'] == 0) {
        this.errors.operation = 'select a operation';
      }
      if (!this.errors.selectPerson && !this.errors.column && !this.errors.operation) {
        this.commonApiService.create(body).subscribe(res => {
          this.data.push(res.info);
        })
      }
    } else {
      this.commonApiService.create(body).subscribe(res => {
        this.data.push(res.info);
      });
    }
  }

  get() {
    this.notificationService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        res.info.forEach(i => {
          i.onClick = false;
        });
        this.data = res.info;
      }
    });
  }
  delete(index) {
    console.log(this.data[index]['id'], this.data[index])
    const id = this.data[index]['id'];
    if (!!id) {
      this.commonApiService.delete(id, 'Notifications_Config').subscribe(res => {
        console.log(res);
        this.data.splice(index, 1);
      });
    }
  }
  getColumn() {
    this.apiObj['tableName'] = 'meta_data';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.columnList = res.info;
      }
    });
  }
  userSelect(e, indexNo) {
    let updateBody = {};
    if (e.option.value.type === 'user') {
      const boby = {
        id: e.option.value.id,
        email: e.option.value.email,
        userName: e.option.value.userName
      };
      updateBody['userId'] = e.option.value.id;
      updateBody['id'] = this.data[indexNo]['id'];
      this.data[indexNo]['userId'] = [];
      this.data[indexNo]['userId'][0] = boby;
      this.data[indexNo]['designationId'] = [];
      this.updateNotification(updateBody);

    } else if (e.option.value.type === 'designation') {
      const boby = {
        id: e.option.value.id,
        designation: e.option.value.designation
      };
      updateBody['designationId'] = e.option.value.id;
      updateBody['id'] = this.data[indexNo]['id'];
      this.data[indexNo]['designationId'] = [];
      this.data[indexNo]['designationId'][0] = boby;
      this.data[indexNo]['userId'] = [];
      this.updateNotification(updateBody);
    }
    this.errors.selectPerson = '';
    this.data[indexNo].onClick = false;
  }
  updateColumn(e, indexNo) {
    let updateBody = {};
    this.data[indexNo]['columnId'] = e.option.viewValue;
    updateBody['columnId'] = e.option.value.id;
    updateBody['id'] = this.data[indexNo]['id'];
    this.errors.column = '';
    if (!!this.data[indexNo]['id']) {
      this.updateNotification(updateBody);
    }

  }
  descriptionChange(type, index) {
    let updateBody = {};
    updateBody['id'] = this.data[index]['id'];
    if (type === 'create') {
      updateBody['createDescription'] = this.data[index]['createDescription'];
    } else if (type === 'update') {
      updateBody['updateDescription'] = this.data[index]['updateDescription'];
    } else if (type === 'delete') {
      updateBody['deleteDescription'] = this.data[index]['deleteDescription'];
    }
    if (!!this.data[index]['id']) {
      this.updateNotification(updateBody);
    }

  }
  oprationChange(event, type, index) {
    let updateBody = {};
    updateBody['id'] = this.data[index]['id'];
    if (type === 'create') {
      updateBody['create'] = +event.checked;
    } else if (type === 'update') {
      updateBody['update'] = +event.checked;
    } else if (type === 'delete') {
      updateBody['delete'] = +event.checked;
    }
    this.errors.operation = '';
    if (!!this.data[index]['id']) {
      this.updateNotification(updateBody);
    }
  }
  getUserAndDes(val) {
    const body = { q: val, limit: 10 };
    this.workFlowService.SearchUserAndDesignation(body).subscribe(res => {
      if (res.statusCode === 200) {
        this.userAndDesignationList = res.info;
      }
    });
  }

  updateNotification(body) {
    body['tableName'] = 'Notifications_Config';
    this.commonApiService.update(body).subscribe(res => {

    });
  }
}
