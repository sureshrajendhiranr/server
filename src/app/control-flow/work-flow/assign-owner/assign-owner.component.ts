import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { WorkFlowService } from '../service/work-flow.service';

@Component({
  selector: 'app-assign-owner',
  templateUrl: './assign-owner.component.html',
  styleUrls: ['./assign-owner.component.css']
})
export class AssignOwnerComponent implements OnInit {
  @Input('inputData') dataTable: any;
  // dataTable={id:0};
  @Output() event = new EventEmitter();
  optionList = [];
  apiObj = {
    tableName: 'Users',
    limit: 100,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    field: '',
    fieldValue: '',
    search: {},
    filter: {}
  };
  selectedUser = [];
  administerList = [];
  isadminister = false;
  isadministerError = false;
  processList = [];
  processObj = { type: '', item: {}, create: 0, delete: 0, edit_process: 0, reports: 0 };
  isprocessError = { check: false, error: '' };

  isFlowEdit = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  constructor(
    public commonApiService: CommonApiService,
    private workFlowService: WorkFlowService
  ) { }

  ngOnInit() {
    // this.dataTable['id'] = 380;
    this.getValue();
  }

  addNewAdmin(item, index) {
    this.administerList.forEach((i, key) => {
      if (!i.type && index === -1) {
        this.isadministerError = true;
        let obj = document.getElementById('admin' + key);
        obj.style.borderColor = 'red';
      }
    });
    if (index !== -1) {
      this.isadministerError = false;
    }
    if (!this.isadministerError) {
      if (index !== -1) {
        this.administerList[index] = item;
      } else {
        this.administerList.push({});
      }
    }
  }
  removeAdmin(index) {
    if (!!this.administerList[index]['workflow_admin_id']) {
      this.commonApiService.delete(this.administerList[index]['workflow_admin_id'], 'Workflow_admin').subscribe(res => {
        this.administerList.splice(index, 1);
      });
    } else {
      this.administerList.splice(index, 1);
    }
  }

  removeProcess(index) {
    if (!!this.processList[index]['workflow_action_id']) {
      this.commonApiService.delete(this.processList[index]['workflow_action_id'], 'Workflow_Action_Access').subscribe(res => {
        this.processList.splice(index, 1);
      });
    } else {
      this.processList.splice(index, 1);
    }
  }
  processPush(item, index) {
    let body = {
      type: item.type, item: item,
      create: this.processList[index].create,
      delete: this.processList[index].delete,
      reports: this.processList[index].reports,
      edit_process: this.processList[index].edit_process
    };
    if (!!this.processList[index]['workflow_action_id']) {
      body['workflow_action_id'] = this.processList[index]['workflow_action_id'];
    }
    this.processList[index] = body;
    this.isprocessError.check = false;
    this.isprocessError.error = '';
  }
  addNewProcess() {
    this.processList.forEach(i => {
      if (!i.type.length) {
        this.isprocessError.check = true;
        this.isprocessError.error = 'Select a group or User';
      } else if (i.create === 0 && i.delete === 0) {
        this.isprocessError.check = true;
        this.isprocessError.error = 'Atleast one permission is required';
      } else {
        this.isprocessError.check = false;
        this.isprocessError.error = '';
      }
    });
    if (!this.isprocessError.check) {
      this.processList.push(this.processObj);
      this.isprocessError.check = false;
      this.isprocessError.error = '';
    }
  }

  getUserAndDes(val) {
    const body = { q: val, limit: 10 };
    this.workFlowService.SearchUserAndDesignation(body).subscribe(res => {
      if (res.statusCode === 200) {
        this.optionList = res.info;
      }
    });
  }

  get(val) {
    this.apiObj.search = { userName: val };
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.optionList = res.info;
      }
    });
  }

  remove(item): void {
    const index = this.selectedUser.indexOf(item);
    if (index >= 0) {
      this.selectedUser.splice(index, 1);
    }
  }

  getValue() {
    let body = {};
    body['q'] = this.dataTable['id'];
    this.workFlowService.workFlowAdminGet(body).subscribe(res => {
      if (res.statusCode === 200) {
        this.processList = res.info.change;
        this.administerList = res.info.admin;
      }
    })
  }

  save() {
    let isError = false;
    let body = {};
    body['admin'] = this.administerList;
    body['change'] = this.processList;
    body['dataTableId'] = this.dataTable.id;
    this.processList.forEach(i => {
      if (!i.type.length) {
        this.isprocessError.check = true;
        this.isprocessError.error = 'Select a group or User';
      } else if (i.create === 0 && i.delete === 0 && i.edit_process && i.reports) {
        this.isprocessError.check = true;
        this.isprocessError.error = 'Atleast one permission is required';
      } else {
        this.isprocessError.check = false;
        this.isprocessError.error = '';
      }
    });
    this.administerList.forEach((i, key) => {
      if (!i.type) {
        this.isadministerError = true;
        let obj = document.getElementById('admin' + key);
        obj.style.borderColor = 'red';
      }
    });
    if (!this.isprocessError.check && !this.isadministerError) {
      this.isprocessError.check = false;
      this.isprocessError.error = '';
      this.workFlowService.workFlowAdminSave(body).subscribe(res => {
        this.event.emit({ close: true });
      });
    }
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.selectedUser.push(event.option.value);
  }
}
