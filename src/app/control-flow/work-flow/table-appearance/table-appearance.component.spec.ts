import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAppearanceComponent } from './table-appearance.component';

describe('TableAppearanceComponent', () => {
  let component: TableAppearanceComponent;
  let fixture: ComponentFixture<TableAppearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableAppearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAppearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
