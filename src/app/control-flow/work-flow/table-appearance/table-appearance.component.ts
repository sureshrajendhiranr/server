import { Component, OnInit, Output,EventEmitter,Input } from '@angular/core';

@Component({
  selector: 'app-table-appearance',
  templateUrl: './table-appearance.component.html',
  styleUrls: ['./table-appearance.component.css']
})
export class TableAppearanceComponent implements OnInit {
  @Output() event = new EventEmitter();
  // tslint:disable-next-line:no-input-rename
  @Input('inputData') data: any;
  constructor() { }

  ngOnInit() {
  }

}
