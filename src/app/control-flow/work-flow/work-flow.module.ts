import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CreateWorkFlowComponent } from './create-work-flow/create-work-flow.component';
import { MaterialModule } from '../../../app/material.module';
import { AssignOwnerComponent } from './assign-owner/assign-owner.component';
import { AddStageComponent } from './add-stage/add-stage.component';
import { FlowEditComponent } from './flow-edit/flow-edit.component';
import { AppearanceComponent } from './appearance/appearance.component';
import { TableAppearanceComponent } from './table-appearance/table-appearance.component';
import { NotificationConfigComponent } from './notification-config/notification-config.component';
import { TableFieldOrderComponent } from './table-field-order/table-field-order.component';

@NgModule({
  declarations: [CreateWorkFlowComponent, AssignOwnerComponent,
    AddStageComponent, FlowEditComponent, AppearanceComponent,
    TableAppearanceComponent, NotificationConfigComponent,
    TableFieldOrderComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [CreateWorkFlowComponent],
  entryComponents: [FlowEditComponent]
})
export class WorkFlowModule { }
