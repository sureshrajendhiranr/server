import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material';
// import { EventEmitter } from 'protractor';
@Component({
  selector: 'app-add-stage',
  templateUrl: './add-stage.component.html',
  styleUrls: ['./add-stage.component.css']
})
export class AddStageComponent implements OnInit {
  StepInfo = {
    name: 'Unnamed Step',
    description: '',
    assginedTo: []
  };
  apiObj = {
    tableName: 'Users', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'id', field: '', fieldValue: '', search: {}, filter: {}
  };
  optionList = [];
  @Output() emitInfo = new EventEmitter;
  @Input('inputData') data: any;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  constructor(public commonApiService: CommonApiService) { }

  ngOnInit() {
    if (this.data.method === 'edit') {
      this.StepInfo = this.data;
    } else {
      this.StepInfo['method'] = 'add';
    }
  }
  getUsers(val) {
    this.apiObj.search = { userName: val }
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.optionList = res.info;
      }
    });
  }
  remove(item): void {
    const index = this.StepInfo.assginedTo.indexOf(item);

    if (index >= 0) {
      this.StepInfo.assginedTo.splice(index, 1);
      this.emitEvent();
    }
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.StepInfo.assginedTo.push(event.option.value);
    this.emitEvent();
  }
  emitEvent() {
    this.emitInfo.emit(this.StepInfo);
  }
}
