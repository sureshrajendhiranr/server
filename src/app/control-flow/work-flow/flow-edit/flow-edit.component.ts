import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WorkFlowService } from '../service/work-flow.service';
import { CommonApiService } from '../../../services/common-api.service';
import { DataTableService } from '../../services/data-table.service';
import { iconList } from '../../constant';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-flow-edit',
  templateUrl: './flow-edit.component.html',
  styleUrls: ['./flow-edit.component.css']
})
export class FlowEditComponent implements OnInit {
  @Input('inputData') dataTable: any;
  // dataTable = { id: 0 };
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filter: []
  };
  catLastClickIndex = -1;

  @Output() event = new EventEmitter();
  data: any;
  dataTableList: any;
  groupList = [];

  dialogRef: any;
  // Icons
  iconList = iconList;
  materialIconList = [];
  filterIconList = [];
  constructor(private workFlowService: WorkFlowService,
    private commonApiService: CommonApiService,
    private dataTableService: DataTableService,
    public dialog: MatDialog) { }

  ngOnInit() {
    // this.dataTable['id'] = 380;
    this.getDataTable();
    this.iconList.categories.forEach(i => { i.icons.forEach(j => { this.materialIconList.push(j.id); }); });
    this.filterIconList = this.materialIconList;
  }

  previewLayout(temp) {
    this.dialogRef = this.dialog.open(temp, {
      width: this.data.width + '%',
      height: this.data.height + '%',
    });
  }
  widthAndHeight() {
    if (this.data.width >= 100) {
      this.data.width = 98;
    } else if (this.data.width <= 25) {
      this.data.width = 25;
    }
    if (this.data.height >= 100) {
      this.data.height = 98;
    } else if (this.data.height <= 25) {
      this.data.height = 25;
    }
    this.updateDataTable();
  }

  addCategoriesList() {
    let isCheck = false;
    this.data.categoriesList.forEach((i, key) => {
      if (i.id === -1) {
        isCheck = true;
        document.getElementById(key + 'input').style.borderColor = 'red';
      }
    });
    if (!isCheck) {
      this.data.categoriesList.push({ id: -1, name: '' });
    }
  }

  groupOptionSelect(event) {
    this.data.groupName = event.option.value.displayValue;
    this.updateDataTable();
  }

  iconOptionSelect(event) {
    this.data.groupName = event.option.value.displayValue;
    // this.updateDataTable();
  }
  getDataTable() {
    this.workFlowService.getDataTable(this.dataTable['id']).subscribe(res => {
      this.data = res.info;
    });
  }
  addCategoriesListObject(e) {
    let l = this.data.categoriesList.length;
    this.data.categoriesList[l - 1] = { id: e.option.value.id, name: e.option.value.name };
    this.data.categories = this.data.categories + ',' + e.option.value.id;
    this.updateDataTable();
  }
  removeCategoriesListObject(index) {
    let array = this.data.categories.split(',');
    if (this.data.categoriesList[index]['id'] == 0) {
      array.forEach((value, key) => {
        if ('Side Navigation' == value) {
          array.splice(key, 1);
        }
      });
      this.data.categories = array + '';
    } else {
      array.forEach((value, key) => {
        if (this.data.categoriesList[index]['id'] == value) {
          array.splice(key, 1);
        }
      });
      this.data.categories = array + '';
    }
    if (this.data.categoriesList[index]['id'] !== -1) {
      this.updateDataTable();
      this.deleteCategoriesDataTable(this.data.categoriesList[index]['id']);
      this.data.categoriesList.splice(index, 1);
    } else {
      this.data.categoriesList.splice(index, 1);
    }

  }

  getAllDataTable(value) {
    this.apiObj.tableName = 'data_tables';
    this.apiObj.search['name'] = value;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.dataTableList = res.info;
      }
    });
  }

  getAllGroup(value) {
    this.apiObj.tableName = 'Table_Groups';
    this.apiObj.search['name'] = value;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
      }
    });
  }
  updateDataTable() {
    let body = {};
    body['id'] = this.data.id;
    body['name'] = this.data.name;
    body['dataTableName'] = (this.data.name).split(' ').join('_');
    body['description'] = this.data.description;
    body['categories'] = this.data.categories;
    body['groupName'] = this.data.groupName;
    body['icon'] = this.data.icon;
    body['width'] = this.data.width;
    body['height'] = this.data.height;
    body['sideBarVisible'] = this.data.sideBarVisible;
    body['fieldViewType'] = this.data.fieldViewType;
    body['tableViewType'] = this.data.tableViewType;
    this.dataTableService.updateDataTables(this.data.id, body).subscribe(res => {
    });
  }
  deleteCategoriesDataTable(id) {
    this.dataTableService.deleteCategoriesDataTable(id, this.data.dataTableName).subscribe(res => {

    });
  }
  iconsFilter(val) {
    this.filterIconList = this.materialIconList.filter(i => i.includes(val))
  }

}
