import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-appearance',
  templateUrl: './appearance.component.html',
  styleUrls: ['./appearance.component.css']
})
export class AppearanceComponent implements OnInit {
  @Output() event = new EventEmitter();
  @Input('inputData') data: any;
  inputData: any;
  constructor() { }

  ngOnInit() {
  }

}
