import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import { CommonApiService } from '../../../services/common-api.service';


@Component({
  selector: 'app-tel',
  templateUrl: './tel.component.html',
  styleUrls: ['./tel.component.css']
})
export class TelComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  getDataObject = {
    limit: 10,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    search: {},
    filterValue: '',
    field: '',
    fieldValue: '',
    tableName: 'country'
  };
  data: any;
  countryCode: any;
  isValid: boolean = true;
  isUniqueValid: boolean = true;
  validationCount: number;
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  currentUser = JSON.parse(localStorage.flowPodUser)
  constructor(public validationsServicesService: ValidationsServicesService, public commonApiService: CommonApiService) { }

  ngOnInit() {
    // console.log(this.inputData)
    if (!!this.inputData.isCreate) {
      this.getCountry();
    }
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.countryCode = '+ 91';
    this.isValid = (!this.inputData.data.required);
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';

  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  emitChangeData() {
    if (this.isUnique()) {
      this.inputData.value = this.inputData.data.defaultValue;
      this.event.emit({
        [this.inputData.data.name]: this.inputData.data.defaultValue,
        'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
      });
    }
  }
  click(val, event) {
    if (event.ctrlKey) {
      window.open('tel:' + val, '_self');
    }
  }
  edit() {
    this.inputData.data.defaultValue = this.inputData.value; this.isEdit = this.inputData.rowId.id;
  }
  isUnique() {
    if (!!this.inputData.data.isunique) {
      this.getDataObject.tableName = this.inputData.data.dataTableName;
      this.getDataObject.search = { [this.inputData.data.name]: this.inputData.data.defaultValue }
      this.commonApiService.getAll(this.getDataObject).subscribe(res => {
        if (res.info.length) {
          this.errorMessage = 'already exit, it should be unique';
          this.isUniqueValid = false;
        } else {
          this.errorMessage = '';
          this.isUniqueValid = true;
        }
      });
    } else {
      return true;
    }
    return this.isUniqueValid;
  }
  validationCallBack(val) {
    // console.log(this.validationArray)
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = this.isValid;
    }
    this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
    this.isValid = this.isUnique();
  }

  getCountry() {
    this.commonApiService.getAll(this.getDataObject).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;

      }
    });
  }
  test(e) {
    this.countryCode = '+' + e.option.value.callingCode;
  }
}
