import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DataShareService } from '../../services/data-share.service';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import { ConditionalExpr } from '@angular/compiler';
import { MatDialog } from '@angular/material';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  @Input('data') inputData: any;
  @Output('eventEmit') eventEmit = new EventEmitter();
  @Output() event = new EventEmitter();
  isEdit: boolean = false;
  imageUrl: string;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  fullPreview = false;
  isValid: boolean = false;
  validationCount: number;
  imagePathLink = environment.filePath;
  fileObjSectionTable: any;
  currentUser = JSON.parse(localStorage.flowPodUser);
  constructor(public dataShareService: DataShareService, public validationsServicesService: ValidationsServicesService, public dialog: MatDialog) { }

  ngOnInit() {
    if (!!this.inputData.value) {
      this.imagePathLink = this.imagePathLink + this.inputData.value;
    } else {
      this.imagePathLink = 'https://image.flaticon.com/icons/svg/149/149092.svg';
    }
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    // Event Lister
    this.dataShareService.data.subscribe(data => {
      if (!!data.image) {
        if (data.image.fieldName === this.inputData.data.displayValue) {
          if (!!data.image.shape) {
            this.inputData.cssClass = data.image.shape;
          } else if (!!data.image.file) {
            this.imageUrl = data.image.file.file;
          }
        }
      }
    });

  }
  toggleSelect(event) {
    if (event.checked) {
      this.dataShareService.image({ image: { shape: 'round' } });
    } else {
      this.dataShareService.image({ image: { shape: 'square' } });
    }
  }

  openImage(imageFullPreview, event) {
    if (event.ctrlKey) {
      this.dialog.open(imageFullPreview, {
        width: '50%',
        height: '70%'
      });
    }
  }
  editImage() {
    this.inputData.data.defaultValue = this.inputData.value;
    this.isEdit = !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo;
    if (this.inputData.isTableCreate) {
      document.getElementById('sectionTableViewImageLabel').click();
    } else {
      document.getElementById('tableViewImageLabel').click();
    }
  }
  validationCallBack(event) {
    let size = 0, fileName = '';
    if (!!this.validationArray && event.target.files.length) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        if (this.validationArray[i].name == 'max_size') {
          size = event.target.files[0].size;
        } else { fileName = event.target.files[0].name }
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](!!size ? size : fileName, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else { this.imageUrl = ''; this.isValid = true; }
  }
  changeProfilePic(event, obj) {
    // console.log(event.target.files[0], obj)
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      this.inputData.value = file.name;

      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (even) => {
        if (even.target['result'].toString().startsWith('data:image')) {
          obj.data.defaultValue = even.target['result'];
          this.eventEmit.emit({ fileName: file.name, file: this.imageUrl });
          if (!this.inputData.isTableCreate) {
            this.dataShareService.image({ image: { file: { fileName: file.name, file: this.imageUrl }, fieldName: obj.data.displayValue } });
            this.imagePathLink = even.target['result'];
            this.imageUrl = even.target['result'];
          } else {
            this.fileObjSectionTable = even.target['result'];
          }
          // Output emit
          this.event.emit({
            [this.inputData.data.name]: file.name,
            file: { fileName: file.name, file: even.target['result'] },
            'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo,
            'isValid': this.isValid
          });
        }
      };
    }
  }
}

