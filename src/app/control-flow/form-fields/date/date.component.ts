import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import * as moment from 'moment';
import { flattenStyles } from '@angular/platform-browser/src/dom/dom_renderer';
@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  minDate: any;
  maxDate: any;
  isValid: boolean = true;
  validationCount: number;
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor(public validationsServicesService: ValidationsServicesService) { }

  ngOnInit() {
    // if (typeof (this.inputData.value) !== 'string') {
    //   this.inputData.value = this.inputData.value['date']
    // }
    if (!(!!this.inputData.data.defaultValue.date)) {
      this.inputData.data.defaultValue = {};
      this.inputData.data.defaultValue['date'] = '';
    }
    this.isValid = (!this.inputData.data.required);
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name]()
        this.minDate = this.resultData.minDate;
        this.maxDate = this.resultData.maxDate;
        this.validationCount = this.validationCount + 1;
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = true;
    }
    (!!this.inputData.isView) ? this.inputData.data.defaultValue['date'] = this.inputData.value : '';
    this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  emitChangeData(e) {
    this.inputData.value = moment(this.inputData.data.defaultValue.date).format('YYYY-MM-DD');
    this.event.emit({
      [this.inputData.data.name]: moment(this.inputData.data.defaultValue.date).format('YYYY-MM-DD'),
      'isValid': this.isValid,
      'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }

}
