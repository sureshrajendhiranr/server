import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { CommonService } from '../../../shared/common/common.service';

@Component({
  selector: 'app-advanced',
  templateUrl: './advanced.component.html',
  styleUrls: ['./advanced.component.css']
})
export class AdvancedComponent implements OnInit {
  @Input('inputData') inputData: any;
  @Output() eventEmit = new EventEmitter();
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filter: {}
  };
  fieldList = [];
  selectedFieldList = [];
  constructor(public commonApiService: CommonApiService,
    public commonService: CommonService) { }

  ngOnInit() {
    this.inputData['showHideFields'] = { 'fields': [] };
    (this.inputData.data.optionValue).forEach(i => {
      let optionName = this.commonService.toSnakeCase(i);
      this.inputData['showHideFields'][optionName] = [];
    });
    this.eventEmit.emit({});
    this.getFeilds();
  }
  selectedField(e) {
    e.forEach(i => {
      i['checked'] = !!i['checked'] ? i['checked'] : false;
      // if (i.checked) {
      //   this.inputData.showHideFields.fields.push(i.name);
      // } else if (!i.checked) {
      //   if ((this.inputData.showHideFields.fields).includes(i.name)) {
      //     this.inputData.showHideFields.fields.splice(this.inputData.showHideFields.fields.indexOf(i.name), 1);
      //   }
      // }
    });
    this.selectedFieldList = e;
  }
  addFieldList() {
    this.inputData.showHideFields.fields = [];
    this.selectedFieldList.forEach(i => {
      let fieldName = this.commonService.toCamelCase(i.name);
      this.inputData.showHideFields.fields.push(fieldName);
    });
    // console.log(this.inputData.showHideFields.fields);
  }
  add(item, e, val) {
    let temp = item['checked'];
    let fieldName = this.commonService.toCamelCase(item.name);
    let optionName = this.commonService.toSnakeCase(val);
    if (e) {
      item['checked'] = true;
      this.selectedFieldList[this.selectedFieldList.indexOf(item)] = item;
      // this.inputData['showHideFields'][val] = [];
      item['checked'] = temp;
      // console.log(this.commonService.toSnakeCase(optionName))
      // let f = this.commonService.toCamelCase(optionName) + '';
      if (!!this.inputData['showHideFields'][optionName]) {
      } else {
        this.inputData['showHideFields'][optionName] = [];
      }
      this.inputData['showHideFields'][optionName].push(fieldName);
      // this.eventEmit.emit(this.inputData.showHideFields);
    } else {
      item['checked'] = false;
      let index = this.inputData['showHideFields'][optionName].indexOf(fieldName);
      this.inputData['showHideFields'][optionName].splice(index, 1);
      item['checked'] = temp;
    }
    console.log(this.inputData.showHideFields)
    this.eventEmit.emit(this.inputData.showHideFields);
  }
  getFeilds() {
    this.apiObj.tableName = 'meta_data';
    this.apiObj.field = 'data_table_name';
    this.apiObj.fieldValue = this.inputData.dataTableName;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.info.length) {
        this.fieldList = res.info;
      }
    });
  }
}
