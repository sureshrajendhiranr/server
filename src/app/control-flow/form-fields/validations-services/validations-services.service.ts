import { Injectable } from '@angular/core';
import { specialcharater } from '../../constant'
import * as moment from 'moment';
import { CommonService } from '../../../shared/common/common.service';
import { validation, autoRule, setContainsvalid, file, fileTypeList } from '../../constant';
@Injectable({
  providedIn: 'root'
})
export class ValidationsServicesService {

  constructor(public commonService: CommonService) { }

  validationCallBack = {
    'max_len': (val, obj) => {
      if ((val + '').length > obj.value) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'min_len': (val, obj) => {
      if ((val + '').length < obj.value) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'greater_than': (val, obj) => {
      if (val < obj.value) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'not_equal_to': (val, obj) => {
      if (val == obj.value) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'equal_to': (val, obj) => {
      if (val != obj.value) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'lesser_than': (val, obj) => {
      if (val > obj.value) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'does_not_contains': (val, obj) => {
      let flag = 0;
      (obj.rule).forEach(i => { if (val.includes(i)) { val = val.replace(i, ''); flag = 1; } });
      if (flag) { return { 'status': true, 'value': val } } else { return { 'status': false, 'value': val } }
    },
    'contains': (val, obj) => {
      let count = 0;
      (obj.rule).forEach(i => { if (val.includes(i)) { count += 1; } });
      if (count == (obj.rule).length) { return { 'status': false, 'value': val } } else { return { 'status': true, 'value': val } }
    },
    'letter_only': (val, obj) => {
      let regex = /^[a-zA-Z]+$/i;
      if (regex.test(val)) {
        return { 'status': false, 'value': val }
      } else {
        return { 'status': true, 'value': val.replace(/[^a-zA-Z]/g, '') }
      }
    },
    'no_special': (val, obj) => {
      let regex = /^[a-zA-Z0-9]+$/i, flag = false;
      if (!regex.test(val)) {
        val = val.replace(/[^a-zA-Z0-9]/g, '')
        flag = true;
      }
      return { 'status': flag, 'value': val }
    },
    'in_past_date': () => {
      return { 'minDate': new Date(1900, 1, 1), 'maxDate': new Date() }
    },
    'in_future_date': () => {
      return { 'minDate': new Date(), 'maxDate': new Date(4000, 1, 1) }
    },
    'in_past_time': (val, obj) => {
      if (moment().format('hh:mm') < moment(val, 'HH:mm').format('hh:mm')) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }
    },
    'in_future_time': (val, obj) => {
      if (moment().format('hh:mm') > moment(val, 'HH:mm').format('hh:mm')) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }
    },
    'in_past_datetime': (val, obj) => {
      // console.log(moment(val).format("YYYY-MM-DDTkk:mm"),moment().unix(),moment(val).unix(),moment().unix() < moment(val).unix())
      if (moment().unix() < moment(val).unix()) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }
    },
    'in_future_datetime': (val, obj) => {
      if (moment().unix() > moment(val).unix()) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }
    },
    'image_type': (val, obj) => {

      let ex = val.split('.'); ex = ex[ex.length - 1];

      let flag = 0;
      (obj.rule).forEach(i => {
        if (i === ex.toUpperCase()) { flag = 1; } else { flag = 0; }
      });
      if (!flag) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }
    },
    'max_size': (val, obj) => {
      if (val / (1024 * 1024) > (obj.value)) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }
    },
    'file_type': (val, obj) => {
      let ex = val.split('.'); ex = ex[ex.length - 1];
      let flag = 0;
      (obj.rule).forEach(i => {
        if (i === ex.toUpperCase()) {
          flag = 1;
        }
      });
      if (!flag) {
        return { 'status': true, 'value': val }
      } else {
        return { 'status': false, 'value': val }
      }

    },

  };
  computationalCallBack = {
    'average': (val, obj) => {
      obj = obj.fieldList;
      let count = obj.length;
      let avg = 0;
      if (!!val) {
        obj.forEach(i => {
          if (!!val[i]) {
            avg = avg + parseInt(val[i]);
          }
        });
      }
      return (avg / count);
    },
    'concatenate': (val, obj) => {
      let str = '';
      const joinBy = obj.joinValue;
      obj = obj.fieldList;
      if (!!val) {
        obj.forEach(i => {
          // if (!!'_' + val[i] && !!'_' + val[i].length) {
          //   val[i] = '_' + val[i];
          // }
          if (!!val[i] && !!val[i].length) {
            str = str + joinBy + (!!val[i] ? val[i] : '');
          }
        });
      }
      return str.slice(1, str.length)
    },
    'createdBy': (val, obj) => {

      return JSON.parse(localStorage.flowPodUser).userName;
    },
    'lastModifiedBy': (val, obj) => {
      return JSON.parse(localStorage.flowPodUser).userName;
    },
    'rand': (val, obj) => {
      return Math.round(+new Date() / 1000);
    },
    'randBetween': (max, min) => {
      return Math.round(Math.random() * (max - min) + min);
    },
    'sum': (val, obj) => {
      obj = obj.fieldList;
      let sum = 0;
      if (!!val) {
        obj.forEach(i => {
          if (!!val[i]) {
            sum = sum + parseInt(val[i]);
          }
        });
      }
      return sum
    },
    'dateDiff': (val, obj) => {
      obj = obj.fieldList;
      let now = moment(new Date());
      let diff;
      if (!!val && !!val[obj[0]]) {
        let endDate = moment(new Date(val[obj[0]]));
        diff = endDate.diff(now, 'days');
        if (diff < 0) {
          diff = diff + ' days';
        } else {
          if (new Date().getDate() !== new Date(val[obj[0]]).getDate()) {
            diff = diff + 1 + ' days';
          } else {
            diff = diff + ' day';
          }
        }
      }

      return diff;
    },
    'min': (val, obj) => {
      obj = obj.fieldList;
      let min;
      if (!!val) {
        min = !!val[obj[0]] ? val[obj[0]] : 0;
        obj.forEach(i => {
          if (!!val[i]) {
            if (min > parseInt(val[i])) {
              min = parseInt(val[i]);
            }
          }
        });
      }
      return min;
    },
    'max': (val, obj) => {
      obj = obj.fieldList;
      let max;
      if (!!val) {
        max = !!val[obj[0]] ? val[obj[0]] : 0;
        obj.forEach(i => {
          if (!!val[i]) {
            if (max < parseInt(val[i])) {
              max = parseInt(val[i]);
            }
          }
        });
      }
      return max;
    }
  };


  setDefaultDate(obj) {
    if (!!obj.length) {
      obj = JSON.parse(obj);
      let type = obj.type;
      let val = obj.val;
      let date;
      if (type === 'before') {
        date = moment(new Date()).subtract(val, 'days').toDate();
      } else if (type === 'after') {
        date = moment(new Date()).add(val, 'days').toDate();
      } else { date = moment(new Date()).toDate(); }
      // console.log(date)
      return date;
    }
  }
}

