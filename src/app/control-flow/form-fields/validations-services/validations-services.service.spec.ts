import { TestBed } from '@angular/core/testing';

import { ValidationsServicesService } from './validations-services.service';

describe('ValidationsServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidationsServicesService = TestBed.get(ValidationsServicesService);
    expect(service).toBeTruthy();
  });
});
