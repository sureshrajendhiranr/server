import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShowHideListenerService } from '../../../services/show-hide-listener.service';
import { CommonService } from '../../../shared/common/common.service';

CommonService
@Component({
  selector: 'app-showhide',
  templateUrl: './showhide.component.html',
  styleUrls: ['./showhide.component.css']
})
export class ShowhideComponent implements OnInit {
  @Input('inputData') inputData: any;
  @Output() eventEmit = new EventEmitter();
  constructor(public showHideListenerService: ShowHideListenerService,
    public commonService: CommonService) { }
  showHideFields: any;
  fieldList = [];
  temp: any;
  TableFieldList = [];
  ngOnInit() {
    this.showHideFields = this.inputData.data.showHideFields;
    this.showHideFields = JSON.parse(this.showHideFields);
    if (this.inputData.isTableView) {
      this.setSHTableView();
    } else {
      this.setShowHideField();
    }
    // console.log(this.fieldList)

  }
  setSHTableView() {
    this.showHideListenerService.TableFieldList.subscribe(res => {
      this.TableFieldList = res;
    });
    (this.showHideFields.fields).forEach(i => {
      if (!this.TableFieldList.includes(i)) {
        // console.log(i)
        this.TableFieldList.push(i);
      }
    });
    this.showHideListenerService.FieldDataSource.next(this.TableFieldList);
    this.showHideListenerService.data.subscribe(res => {
      this.temp = res;
    });
    const rowId = this.inputData.rowId.id;
    this.temp['isTableView'] = this.temp['isTableView'] ? this.temp['isTableView'] : {}
    this.temp['isTableView'][rowId] = this.temp['isTableView'][rowId] ? this.temp['isTableView'][rowId] : {}
    let name = this.commonService.toCamelCase(this.inputData.data.name);
    if (this.inputData.data.type === 'toggle') {
      if (this.inputData.isView && !!(!!(+this.inputData.value) + '').length) {
        this.temp['isTableView'][rowId][name] = this.showHideFields[!!(+this.inputData.value) + '']
      } else {
        // let optionName = this.commonService.checkToCamelCase(this.inputData.data.defaultValue)
        this.temp['isTableView'][rowId][name] = this.showHideFields[!!(+this.inputData.data.defaultValue) + '']
      }
      this.showHideListenerService.dataSource.next(this.temp);
    } else {
      if (!!this.inputData.value.length) {
        let n = this.commonService.toSnakeCase(this.inputData.value)
        this.temp['isTableView'][rowId][name] = this.showHideFields[n];
      } else if (!!this.inputData.data.defaultValue) {
        let n = this.commonService.toSnakeCase(this.inputData.data.defaultValue)
        this.temp['isTableView'][rowId][name] = this.showHideFields[n];
      } else {
        this.temp['isTableView'][rowId][name] = [];
      }
      // this.temp[name] = this.showHideFields[this.inputData.data.defaultValue];
      // console.log(this.temp)
      this.showHideListenerService.dataSource.next(this.temp);
    }
  }
  setShowHideField() {
    this.showHideListenerService.fieldList.subscribe(res => {
      this.fieldList = res;
    });
    (this.showHideFields.fields).forEach(i => {
      if (!this.fieldList.includes(i)) {
        // console.log(i)
        this.fieldList.push(i);
      }
    });
    this.showHideListenerService.FieldDataSource.next(this.fieldList);

    this.showHideListenerService.data.subscribe(res => {
      this.temp = res;
    });
    let name = this.commonService.toCamelCase(this.inputData.data.name);
    if (this.inputData.data.type === 'toggle') {
      if (this.inputData.isView && !!(!!(+this.inputData.value) + '').length) {
        this.temp[name] = this.showHideFields[!!(+this.inputData.value) + '']
      } else {
        let optionName = this.commonService.checkToCamelCase(this.inputData.data.defaultValue)
        this.temp[name] = this.showHideFields[!!(+this.inputData.data.defaultValue) + '']
      }
      this.showHideListenerService.dataSource.next(this.temp);
    } else {
      if (!!this.inputData.value.length) {
        let n = this.commonService.toSnakeCase(this.inputData.value)
        this.temp[name] = this.showHideFields[n];
      } else if (!!this.inputData.data.defaultValue) {
        let n = this.commonService.toSnakeCase(this.inputData.data.defaultValue)
        this.temp[name] = this.showHideFields[n];
      } else {
        this.temp[name] = [];
      }
      // this.temp[name] = this.showHideFields[this.inputData.data.defaultValue];
      this.showHideListenerService.dataSource.next(this.temp);
    }
  }
  eventHandlerInShowHide(e) {
    if (this.inputData.isTableView) {
      this.sHHandlerTableView(e);
    } else {
      this.showHideHandler(e)
    }

  }
  sHHandlerTableView(e) {
    const rowId = this.inputData.rowId.id;
    let key = Object.keys(e)[0];
    this.showHideListenerService.data.subscribe(res => {
      this.temp = res;
    });
    // let name = this.commonService.toCamelCase(this.inputData.data.name);
    let name = this.commonService.toCamelCase(this.inputData.data.name);
    // console.log(this.inputData.data.name)
    if (this.inputData.data.type === 'toggle') {
      let temp = (!!(e[key])) + '';
      this.temp['isTableView'][rowId][name] = this.showHideFields[(!!(e[key])) + ''];
      this.showHideListenerService.dataSource.next(this.temp);
    } else {
      let n = this.commonService.toSnakeCase(e[key])
      this.temp['isTableView'][rowId][name] = this.showHideFields[n];
      this.showHideListenerService.dataSource.next(this.temp);
    }
    console.log(this.temp['isTableView'])
    this.eventEmit.emit(e);
  }
  showHideHandler(e) {
    let key = Object.keys(e)[0];
    this.showHideListenerService.data.subscribe(res => {
      this.temp = res;
    });
    // let name = this.commonService.toCamelCase(this.inputData.data.name);
    let name = this.commonService.toCamelCase(this.inputData.data.name);
    // console.log(this.inputData.data.name)
    if (this.inputData.data.type === 'toggle') {
      let temp = (!!(e[key])) + '';
      this.temp[name] = this.showHideFields[(!!(e[key])) + ''];
      this.showHideListenerService.dataSource.next(this.temp);
    } else {
      let n = this.commonService.toSnakeCase(e[key])
      this.temp[name] = this.showHideFields[n];
      this.showHideListenerService.dataSource.next(this.temp);
    }
    this.eventEmit.emit(e);
  }
}
