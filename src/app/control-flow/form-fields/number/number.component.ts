import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ValidationsServicesService } from "../validations-services/validations-services.service"
import { FormControl } from '@angular/forms';
import { PopupEditFormComponent } from '../../popup-edit-form/popup-edit-form.component';
import { MatDialog } from '@angular/material';
import { CommonApiService } from '../../../services/common-api.service';
CommonApiService
@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css']
})
export class NumberComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  validationArray = [];
  error: any;
  stopLoop: boolean = false;
  errorMessage: any;
  resultData: any;
  isValid: boolean = true;
  validationCount: number;
  getDataObject = {
    limit: 10,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    search: {},
    filterValue: '',
    field: '',
    fieldValue: '',
    tableName: ''
  };
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor(public validationsServicesService: ValidationsServicesService,
    public dialog: MatDialog, public commonApiService: CommonApiService) { }
  ngOnInit() {
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.isValid = (!this.inputData.data.required);
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  isUnique() {
    if (!!this.inputData.data.isunique) {
      this.getDataObject.tableName = this.inputData.data.dataTableName;
      this.getDataObject.search = { [this.inputData.data.name]: this.inputData.data.defaultValue }
      this.commonApiService.getAll(this.getDataObject).subscribe(res => {
        if (res.info.length) {
          this.errorMessage = 'already exit, it should be unique';
          this.isValid = false;
        }
      });
    }
  }

  validationCallBack(val) {
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = true;
    }

    this.isValid = this.inputData.data.required ? ((this.inputData.data.defaultValue + '').length && this.isValid) : this.isValid;

  }
  emitChangeData() {

    this.inputData.value = this.inputData.data.defaultValue;
    if (this.inputData.isCreate) {
      this.inputData.inputValue[this.inputData.data.name] = this.inputData.value;
    }
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
  err(err: any) {
    throw new Error("Method not implemented.");
  }
}
