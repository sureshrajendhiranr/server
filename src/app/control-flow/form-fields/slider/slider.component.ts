import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  min = 0;
  max = 10;
  validation: any;
  isEdit = false;
  isValid: boolean = true;
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor() { }

  ngOnInit() {
    // console.log(this.inputData)
    this.validation = this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    if (!!this.inputData.isTableView) {
      this.validation = this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    }
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
  }
  emitChangeData() {
    this.inputData.value = this.inputData.data.defaultValue;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }

}
