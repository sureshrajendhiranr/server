import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.css']
})
export class ToggleComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  isEdit: boolean = false;
  currentUser = JSON.parse(localStorage.flowPodUser);

  constructor() { }

  ngOnInit() {
    (this.inputData.isView) ? (this.inputData.data.defaultValue = this.inputData.value) : false;
    // this.inputData.data.defaultValue = !!(+this.inputData.data.defaultValue);
    this.inputData.isCreate ? this.inputData.data.defaultValue = !!(+this.inputData.data.defaultValue) : ''
  }
  emitChangeData() {
    this.inputData.value = +this.inputData.data.defaultValue;
    this.event.emit({
      [this.inputData.data.name]: +this.inputData.data.defaultValue,
      'isValid': true, 'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
}
