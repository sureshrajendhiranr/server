import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { LastmodifiedListenerService } from '../../../services/lastmodified-listener.service';
import { CommonService } from '../../../shared/common/common.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  apiObj = {
    tableName: 'Users', limit: 10, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  optionList = [];
  SelectedValueList = [];
  isEdit = 0;
  currentUser = JSON.parse(localStorage.flowPodUser)
  displayValue = [];
  constructor(public commonApiService: CommonApiService,
    public lastmodifiedListenerService: LastmodifiedListenerService,
    public commonService: CommonService) { }

  ngOnInit() {
    if (this.inputData.value) {
      (this.inputData.value).forEach(i => {
        this.displayValue.push(i.userName);
      });
    }
    if (!this.inputData.isComputational) {
      if (typeof (this.inputData.data.isAdvanced) === 'string') {
        this.inputData.data.isAdvanced = JSON.parse(this.inputData.data.isAdvanced);
      }
    } else {
      Object.assign(this.inputData, this.inputData.componentInput);
      delete (this.inputData['componentInput']);
      if (this.inputData.isCreate) {
        this.inputData.data.defaultValue = this.currentUser.userName;
        this.inputData.value = this.currentUser.id;
        this.inputData.inputValue[this.inputData.data.name] = this.currentUser.id + '';
        this.userId.push(this.currentUser.id);
        // this.emitChangeData();
      }
      // console.log(this.inputData)
      let tempCom = JSON.parse(this.inputData.data.computational)
      // let t;
      if (tempCom.name === 'lastModifiedBy') {
        let temp;
        this.lastmodifiedListenerService.dataSource.subscribe(res => {
          temp = res;
          // console.log(temp)
          if (this.inputData.isTableView && temp['isTableView'][this.inputData.rowId.id]) {
            // console.log(temp['isTableView'])
            this.inputData.value = temp['isTableView'][this.inputData.rowId.id][this.inputData.data.name];
          }
          // console.log(this.inputData.value)
          if (this.inputData.isView && temp['isView']) {
            // console.log(this.inputData)
            this.inputData.value = temp['isView'][this.inputData.data.name];
            this.inputData.value = temp['isTableView'][this.inputData.rowInfo.id][this.commonService.toCamelCase(this.inputData.data.name)]

          }
          if (this.inputData.isCreate) {
            this.userId = this.currentUser.id;
          }
        });
      }
    }
  }

  userId = [];
  toggleSelect(item) {
    if (!!this.SelectedValueList.includes(item.userName)) {
      this.SelectedValueList.splice(this.SelectedValueList.indexOf(item.userName), 1);
    } else {
      this.SelectedValueList.push(item.userName);
    }
    this.inputData.data.defaultValue = this.SelectedValueList + '';
    // this.userId = [];
    // item.forEach(i => {
    //   this.userId.push(i.id);
    //   console.log(i)
    // });
    if (!this.userId.includes(item.id)) {
      this.userId.push(item.id);
    } else {
      this.userId.splice(this.userId.indexOf(item.id), 1);
    }
    this.emitChangeData();
  }
  displayWith(item) {
    return item ? item.userName : ''
  }
  singleSelect(item) {
    this.userId.push(item.id);
  }
  emitChangeData() {
    // this.inputData['value'] = this.inputData.data.defaultValue + '';
    this.inputData.value = this.userId + ''
    this.inputData.inputValue[this.inputData.data.name] = this.inputData.value;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.value,
      'isValid': true, 'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
  getOption(val) {
    this.apiObj.search = { 'user_name': val };
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.optionList = res.info;
      }
    });
  }
}
