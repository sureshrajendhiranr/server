import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommonService } from '../../../shared/common/common.service';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import { MatDialog } from '@angular/material';
import { environment } from '../../../../environments/environment';



@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.css']
})
export class AttachmentComponent implements OnInit {
  @Input('data') inputData: any;
  @Output('eventEmit') eventEmit = new EventEmitter();
  @Output() event = new EventEmitter();
  imageUrl: string;
  isEdit: boolean = false;
  fileObject: any;
  fileName: string;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  isValid: boolean = false;
  validationCount: number;
  fileObjects = {};
  filePathLink = environment.filePath;
  constructor(public commonService: CommonService, public validationsServicesService: ValidationsServicesService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    if (!!this.inputData.indexNo) {
      this.inputData['rowId'] = {};
      this.inputData['rowId']['id'] = this.inputData.indexNo;
      this.inputData['isTableView'] = true;
    }
  }

  editFile() {
    this.inputData.data.defaultValue = this.inputData.value; this.isEdit = this.inputData.rowId.id;
    setTimeout(() => {
      document.getElementById('tableViewFileLabel').click();
    }, 100);
  }

  openFile(url, event) {
    if (event.ctrlKey && !!url.length) {
      url = !url.includes('https://') && !url.includes('http://') ? 'https://' + url : url;
      window.open(url, 'newwindow', 'width=800,height=800');
      return false;
    }
  }

  validationCallBack(val) {
    if (!!this.validationArray && val.target.files.length) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        if (this.validationArray[i].name === 'max_size' && val.target.files.length) { val = val.target.files[0].size } else { val = val.target.files[0].name }
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          this.fileObject = {};
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else { this.fileObjects = {}; this.isValid = true; }
  }
  changeProfilePic(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      this.fileObject = file;
      this.fileName = !!file.name ? file.name : '';
      this.inputData.value = this.fileName;
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (even) => {
        if (even.target['result'].toString()) {
          this.imageUrl = even.target['result'];
          this.eventEmit.emit({ fileName: file.name, file: even.target['result'] });
          this.event.emit({
            [this.inputData.data.name]: file.name,
            file: { fileName: file.name, file: even.target['result'] },
            'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNos,
            'isValid': this.isValid
          });
          // this.fileSize=this.commonService.fileSizeParse(this.fileObject.size);
        }
      };
    }
  }
  removeFile() {
    this.fileObject = {};
  }
  fullFilePreview(filePreview) {
    this.dialog.open(filePreview, {
      width: '50%',
      height: '70%'
    });
  }
}
