import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import * as moment from 'moment';

@Component({
  selector: 'app-datetime',
  templateUrl: './datetime.component.html',
  styleUrls: ['./datetime.component.css']
})
export class DatetimeComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  isValid: boolean = true;
  validationCount: number;
  isHover = false;
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor(public validationsServicesService: ValidationsServicesService) { }

  ngOnInit() {
    // if (typeof (this.inputData.value) !== 'string') {
    //   this.inputData.value = this.inputData.value['datetime']
    // }
    if (!(!!this.inputData.data.defaultValue.datetime)) {
      this.inputData.data.defaultValue = {};
      this.inputData.data.defaultValue['datetime'] = '';
    }

    (!!this.inputData.isView) ? this.inputData.data.defaultValue['datetime'] = this.inputData.value : '';
    // this.inputData.data.defaultValue.datetime = {};

    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.isValid = (!this.inputData.data.required)
    // if (!!this.inputData.isTableCreate) {
    //   this.test(this.inputData.value.includes(this.inputData.data.type) ? JSON.parse(this.inputData.value) : this.inputData.value)
    // }
  }

  opendatetime() {
    setTimeout(() => {
      let id = document.getElementById('datetime')
      id.click();
    }, 10);
  }
  validationCallBack(val) {
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = true;
    }
    this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
  }
  emitChangeData() {
    this.inputData.data.defaultValue.datetime = moment(this.inputData.data.defaultValue.datetime).format('YYYY-MM-DD HH:mm:ss');
    this.inputData.value = this.inputData.data.defaultValue.datetime;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue.datetime,
      type: this.inputData.data.type,
      'isValid': true, 'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
}
