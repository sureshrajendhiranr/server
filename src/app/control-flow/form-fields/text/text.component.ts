import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service'
import { CommonApiService } from '../../../services/common-api.service';
@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = 0;
  validationArray = [];
  errorMessage: any;
  resultData: any;
  isValid: boolean = true;
  validationCount = 0;
  getDataObject = {
    limit: 10,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    search: {},
    filterValue: '',
    field: '',
    fieldValue: '',
    tableName: 'country'
  };
  isUniqueValid: boolean = true;
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor(public validationsServicesService: ValidationsServicesService, public commonApiService: CommonApiService,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    // this.computation
    // console.log(this.inputData.inputValue)
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.isValid = (!this.inputData.data.required);
    (!!this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
    if (this.inputData.data.defaultValue.length && this.inputData.isCreate) {
      this.emitChangeData();
    }
  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }

  emitChangeData() {
    if (this.isUnique()) {
      this.inputData.value = this.inputData.data.defaultValue;
      if (!!this.inputData.isCreate) {
        this.inputData.inputValue[this.inputData.data.name] = this.inputData.value;
      }
      this.event.emit({
        [this.inputData.data.name]: this.inputData.data.defaultValue,
        'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
      });
    }
  }



  isUnique() {
    if (!!this.inputData.data.isunique) {
      this.getDataObject.tableName = this.inputData.data.dataTableName;
      this.getDataObject.search = { [this.inputData.data.name]: this.inputData.data.defaultValue }
      this.commonApiService.getAll(this.getDataObject).subscribe(res => {
        if (res.info.length) {
          this.errorMessage = 'already exit, it should be unique';
          this.isUniqueValid = false;
        } else {
          this.errorMessage = '';
          this.isUniqueValid = true;
        }
      });
    } else {
      return true;
    }
    return this.isUniqueValid;
  }
  validationCallBack(val) {
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else { this.isValid = true; }
    this.isValid = this.inputData.data.required ? !!(this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
  }

  test() {
    // return '"input-view-2"'
    // this.eventChangeValue.emit({})
  }
}
