import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { MatSelect } from '@angular/material';
import { AdvancedFieldListenerService } from '../../../services/advanced-field-listener.service';
import { CommonService } from '../../../shared/common/common.service';
// import { emit } from 'cluster';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  // form=new FormControl('',Validators.pattern)
  @ViewChild('myInput') myInput: MatSelect;
  @ViewChild('myInputSectionTable') myInputSectionTable: MatSelect;
  isEdit = false;

  constructor(
    public commonApiService: CommonApiService,
    public advancedFieldListenerService: AdvancedFieldListenerService,
    public commonService: CommonService) { }
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}, notEqualFilter: {}, requestFields: ''
  };
  isValid: boolean = true;
  fieldIsOpen = false;
  isAdvanced = false;
  advanced: any;
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  // currentUser = JSON.parse(localStorage.flowPodUser);
  ngOnInit() {
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
    // console.log(this.inputData.data.isAdvanced)
    if ((!!this.inputData.data.isAdvanced) && this.inputData.data.isAdvanced.length > 1) {

      this.isAdvanced = true;
      let temp;
      this.advanced = JSON.parse(this.inputData.data.isAdvanced);
      // console.log(this.advanced)
      if (this.advanced.type === 'depended') {
        if (!!this.inputData.isTableView) {

        } else {
          this.advancedFieldListenerService.dataSource.subscribe(res => {
            temp = res;
            if (!temp['isCreate']) {
              let t = { isCreate: { [this.advanced.dependedField]: '' } };
              Object.assign(temp, t);
            } else {
              temp['isCreate'][this.advanced.dependedField] = !temp['isCreate'][this.advanced.dependedField] ? '' :
                temp['isCreate'][this.advanced.dependedField];
            }
          });
          // console.log(temp)
          this.advancedFieldListenerService.dataSource.next(temp);
        }
      }
    }
  }
  isOpenSectionSelect() {
    if (!this.inputData.data.isAdvanced) {
      setTimeout(() => {
        this.myInputSectionTable.open();
      }, 50);
    }
  }
  selectedId = [];
  displayName = ''
  selectedItem: any;
  selectedValue(item) {
    this.displayName = item[this.commonService.toCamelCase(this.advanced.fieldName)];
    this.selectedId = item.id;
    this.inputData.data.defaultValue = [item];
    if (this.advanced.type === 'auto-fill') {
      this.selectedItem = item;
    }
  }

  ngAfterViewChecked() {
    if (this.isEdit && !this.fieldIsOpen && !this.isAdvanced) {
      setTimeout(() => {
        this.fieldIsOpen = true;
        if (!!this.inputData.isTableCreate) {
          this.myInputSectionTable.open();
        } else {
          this.myInput.open();
        }
      }, 20);
    }
  }
  openAdvanced() {
    setTimeout(() => {

    }, 20);
  }
  emitChangeData(e) {
    let name = this.commonService.checkToCamelCase(this.inputData.data.name)
    if (!this.isAdvanced) {
      this.inputData.data.defaultValue = e;
      this.inputData.value = e;
    } else {
      this.inputData.value = this.displayName;
      if (this.inputData.isCreate) {
        this.inputData.data.defaultValue = this.displayName;
        this.inputData.inputValue[name] = this.displayName;
        this.inputData.inputValue[this.inputData.data.name] = this.selectedId + ''
      }
    }
    if (this.inputData.isView) {
      this.inputData.rowInfo[name] = this.inputData.data.defaultValue;
    }
    if (this.inputData.isTableCreate) {
      this.inputData.rowData[name] = this.isAdvanced ? this.selectedId + '' : this.inputData.data.defaultValue;
    }
    // console.log(this.inputData.inputValue, this.selectedId)

    let emitData = {
      [this.inputData.data.name]: this.isAdvanced ? this.selectedId + '' : this.inputData.data.defaultValue,
      'isValid': true, 'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    };
    if (!!this.isAdvanced && this.advanced.type === 'auto-fill') {
      emitData['autoFill'] = [];
      let autofill = this.advanced.autoFillFields;
      autofill.forEach(i => {
        if (this.inputData.isCreate) {
          this.inputData.inputValue[i.referedField] =
            this.selectedItem[this.commonService.toCamelCase(i.masterField)];
          emitData['autoFill'].push(i.referedField);
        }
        if (this.inputData.isTableView) {
          this.inputData.rowData[i.referedField] =
            this.selectedItem[this.commonService.toCamelCase(i.masterField)];
          emitData['autoFill'].push(i.referedField);
          emitData['rowData'] = this.inputData.rowData;
        }

        if (this.inputData.isView) {
          this.inputData.inputValue[i.referedField] =
            this.selectedItem[this.commonService.toCamelCase(i.masterField)];
          emitData['autoFill'].push(i.referedField);

        }

        if (this.inputData.isTableCreate) {
          this.inputData.inputValue[this.commonService.toCamelCase(i.referedField)] =
            this.selectedItem[this.commonService.toCamelCase(i.masterField)];
          emitData['autoFill'].push(i.referedField);
        }
      });
    }
    // console.log(emitData)
    this.event.emit(emitData);

  }
  getOption(val) {
    if ((!!this.inputData.data.isAdvanced) && this.inputData.data.isAdvanced.length > 1) {
      let temp = JSON.parse(this.inputData.data.isAdvanced);
      if (temp.type === 'non-depended') {
        this.inputData.data.optionValue = [];
        this.apiObj.tableName = temp.dataTableName;
        this.apiObj.limit = 10;
        this.apiObj.page = 0;
        this.apiObj.search = { [temp.fieldName]: val };
        this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
          this.inputData.data.optionValue = res.info;
        });
      } else if (temp.type === 'auto-fill') {
        this.inputData.data.optionValue = [];
        this.apiObj.tableName = temp.dataTableName;
        this.apiObj.search = { [temp.fieldName]: val };
        this.apiObj.limit = 10;
        this.apiObj.page = 0;
        // console.log(temp)
        this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
          this.inputData.data.optionValue = res.info;
        });
      } else
        if (temp.type === 'depended') {
          this.inputData.data.optionValue = [];
          this.apiObj.tableName = temp.dataTableName;
          this.apiObj.requestFields = 'id,' + temp.fieldName;
          this.apiObj.search = { [temp.fieldName]: val };
          this.apiObj.limit = 10;
          this.apiObj.page = 0;
          // let filter;
          // this.advancedFieldListenerService.dataSource.subscribe(res => {
          //   filter = res;
          // });
          // IS TABLE VIEW
          const name = this.commonService.checkToCamelCase(temp.dependedField)
          if (!!this.inputData.isTableView) {
            if (temp.filterType === 'equalTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            } else if (temp.filterType === 'notEqualTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            }
          } else if (!!this.inputData.isView) {
            if (temp.filterType === 'equalTo') {
              if (typeof (this.inputData.rowInfo[name]) !== 'string') {
                this.apiObj.filterValue = { [temp.filterValue]: [this.inputData.rowInfo[name][0]['id'] + ''] };
              } else {
                this.apiObj.filterValue = { [temp.filterValue]: [this.inputData.rowInfo[name] + ''] };
              }
            } else if (temp.filterType === 'notEqualTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowInfo[name][0]['id'] };
              } else {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowInfo[name] };
              }
            }
          } else if (!!this.inputData.isTableCreate) {
            if (temp.filterType === 'equalTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            } else if (temp.filterType === 'notEqualTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            }

          } else if (!!this.inputData.isCreate) {
            // console.log(temp, this.inputData.inputValue[name])
            if (temp.filterType === 'equalTo') {
              this.apiObj.filterValue = { [temp.filterValue]: [this.inputData.inputValue[name] + ''] };
            } else if (temp.filterType === 'notEqualTo') {
              this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.inputValue[name] };
            }
          }

          this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
            this.inputData.data.optionValue = res.info;
          });


        }
    }
  }
  scroll(event) {
    // console.log("ss")
    // console.log("ss",item,document.getElementById(item).scrollTop)
    // const position = Math.round(document.getElementById(item).scrollTop);
    // // const page = Math.round(this.data[item].length / 10);
    // const filter = {};
    // if (document.getElementById(item).offsetHeight + document.getElementById(item).scrollTop >=
    //   document.getElementById(item).scrollHeight) {

    // }
  }
}
