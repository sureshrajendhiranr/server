import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import * as moment from 'moment';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})
export class TimeComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  isValid: boolean = true;
  validationCount: number;
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  currentUser = JSON.parse(localStorage.flowPodUser)
  constructor(public validationsServicesService: ValidationsServicesService) { }

  ngOnInit() {
    // console.log(this.inputData)
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.isValid = (!this.inputData.data.required);
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  opentime() {
    setTimeout(() => {
      document.getElementById('timeInput').click()
    }, 50);
  }
  emitChangeData() {
    let time;
    if (typeof (this.inputData.data.defaultValue) !== 'string') {
      if (!!this.inputData.isCreate) {
        time = moment(this.inputData.data.defaultValue).format('hh:mm:ss');
        this.inputData.value = time;
      } else {
        time = moment(this.inputData.data.defaultValue).format('hh:mm:ss');
        this.inputData.data.defaultValue = time;
        this.inputData.value = this.inputData.data.defaultValue;
      }
      console.log(time);
      this.event.emit({
        [this.inputData.data.name]: time,
        'isValid': this.isValid, 'isrequired': this.inputData.data.required,
        'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
      });

    }
  }
  validationCallBack() {
    let val = this.inputData.data.defaultValue
    if (!!this.validationArray.length) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = true;
    }
    // this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
  }
}
