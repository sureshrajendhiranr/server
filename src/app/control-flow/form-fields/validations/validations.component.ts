import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { validation, autoRule, setContainsvalid, file, fileTypeList } from '../../constant';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { FormControl } from '@angular/forms';
import { ValidationsServicesService } from '../validations-services/validations-services.service';

@Component({
  selector: 'app-validations',
  templateUrl: './validations.component.html',
  styleUrls: ['./validations.component.css']
})
export class ValidationsComponent implements OnInit {
  @Input('validateSelector') validateSelctorData: any;
  @Output() eventEmit = new EventEmitter();
  validationArray = [];
  validation = JSON.parse(JSON.stringify(validation));
  fileTypeList = fileTypeList
  file = file
  isCustomErrorId: any;
  isVisibleError:boolean=false;
  customErrorFor: any;
  chipsId: any;
  autoRule = autoRule;
  setContainsvalid = setContainsvalid;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  containesRule = [];

  constructor(public validationsServicesService: ValidationsServicesService) { }

  ngOnInit() {
    // console.log(this.validationsServicesService.doesNotContain("sssssss@#"),
    //  this.validationsServicesService.contains("sssaaassss@#"),
    //  this.validationsServicesService.noSpecialCharater("sssaaassss@#"),
    //  )
  }

  addValidation(item) {
    if (!this.validateSelctorData.validationArray.includes(item)) {
      this.validateSelctorData.validationArray.push(item);
    }
  }
  removeValidation(item) {
    this.validateSelctorData.validationArray.splice(this.validateSelctorData.validationArray.indexOf(item),1);
  }

  add(event: MatChipInputEvent, index) {
    if ((event.value || '').trim()) {
      if (!this.validateSelctorData.validationArray[index]['rule'].includes(event.value)) {
        this.validateSelctorData.validationArray[index]['rule'].push(event.value.trim());
        event.input.value = '';
      }

    }
  }

  remove(item, i): void {
    const index = this.validateSelctorData.validationArray[i]['rule'].indexOf(item);
    if (index >= 0) {
      this.validateSelctorData.validationArray[i]['rule'].splice(index, 1);
    }

  }

}
