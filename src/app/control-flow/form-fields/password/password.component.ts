import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import { template } from '@angular/core/src/render3';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  hide = true;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  isValid: boolean = true;
  validationCount: number;
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor(public validationsServicesService: ValidationsServicesService) { }

  ngOnInit() {
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.isValid = (!this.inputData.data.required);
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
  }

  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  startReturn(val) {
    let str = '';
    for (let i = 0; i < val; i++) {
      str = str + '*';
    }
    return str;
  }

  validationCallBack(val) {
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = true;
    }
    this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;

  }
  emitChangeData() {
    // this.event.emit({ [this.inputData.data.name]: this.inputData.data.defaultValue, 'isValid': this.isValid });
    this.inputData.value = this.inputData.data.defaultValue;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
  password(val) {
    if (!!val) {
      let temp = '';
      for (let i = 0; i < val.length; i++) {
        temp = temp + '*';
      }
      return temp;
    }
  }
}
