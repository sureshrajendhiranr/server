import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import { CommonApiService } from '../../../services/common-api.service';
@Component({
  selector: 'app-url',
  templateUrl: './url.component.html',
  styleUrls: ['./url.component.css']
})
export class UrlComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  isPrefixVisible = false;
  validationArray = [];
  resultData: any;
  errorMessage: any;
  isValid: boolean = true;
  validationCount: number;

  getDataObject = {
    limit: 10,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    search: {},
    filterValue: '',
    field: '',
    fieldValue: '',
    tableName: ''
  };

  currentUser = JSON.parse(localStorage.flowPodUser)

  constructor(public validationsServicesService: ValidationsServicesService,
    public commonApiService: CommonApiService) { }

  ngOnInit() {
    this.validationArray = !!this.inputData.data.validations ? JSON.parse(this.inputData.data.validations) : [];
    this.isValid = (!this.inputData.data.required);
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  isUnique() {
    if (!!this.inputData.data.isunique) {
      this.getDataObject.tableName = this.inputData.data.dataTableName;
      this.getDataObject.search = { [this.inputData.data.name]: this.inputData.data.defaultValue }
      this.commonApiService.getAll(this.getDataObject).subscribe(res => {
        if (res.info.length) {
          this.errorMessage = 'already exit, it should be unique';
          this.isValid = false;
        }
      });
    }
  }
  openUrl(url, event) {
    if (event.ctrlKey) {
      url = !url.includes('https://') && !url.includes('http://') ? 'https://' + url : url;
      window.open(url, "newwindow", 'width=800,height=800');
      return false;
    }
  }
  emitChangeData() {
    this.inputData.value = this.inputData.data.defaultValue;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
  validationCallBack(val) {
    if (!!this.validationArray) {
      this.validationCount = 0;
      for (let i = 0; i < this.validationArray.length; i++) {
        this.resultData = this.validationsServicesService.validationCallBack[this.validationArray[i].name](val, this.validationArray[i])
        // console.log(this.resultData)
        if (this.resultData.status) {
          this.errorMessage = !!this.validationArray[i].error ? this.validationArray[i].error : this.validationArray[i].displayValue;
          this.inputData.data.defaultValue = this.resultData.value;
          break;
        } else {
          this.errorMessage = '';
          this.validationCount = this.validationCount + 1;
        }
      }
      this.isValid = (this.validationCount === this.validationArray.length);
    } else {
      this.isValid = true;
    }
    this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
  }
}
