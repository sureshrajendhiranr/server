import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  email: string;
  errorMessage: string = '';
  isValid: boolean = true;
  getDataObject = {
    limit: 10,
    page: 0,
    sortType: 'ASC',
    sortValue: 'id',
    search: {},
    filterValue: '',
    field: '',
    fieldValue: '',
    tableName: ''
  };
  currentUser = JSON.parse(localStorage.flowPodUser);
  // userDesignation = JSON.parse(localStorage.flowPodUserDesgination);
  constructor(public commonApiService: CommonApiService) { }

  ngOnInit() {
    // console.log(this.inputData)
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
    this.isValid = (!this.inputData.data.required)
  }
  ngAfterViewChecked() {
    if (!!this.elementRef && this.isEdit) {
      this.elementRef.nativeElement.focus();
    }
  }
  isUnique() {
    if (!!this.inputData.data.isunique) {
      this.getDataObject.tableName = this.inputData.data.dataTableName;
      this.getDataObject.search = { [this.inputData.data.name]: this.inputData.data.defaultValue }
      this.commonApiService.getAll(this.getDataObject).subscribe(res => {
        if (res.info.length) {
          this.errorMessage = 'already exit, it should be unique';
          this.isValid = false;
        }
      });
    }
  }

  check(val) {
    this.emitChangeData();
    this.commonApiService.mailValidation(val).subscribe(res => {
      if (res.statusCode === 200) {
        this.isValid = res.isValid;
        this.isValid = this.inputData.data.required ? (this.inputData.data.defaultValue.length && this.isValid) : this.isValid;
        if (!!this.inputData.indexNo) {
        } else {
          this.emitChangeData();
        }
        // console.log(res.message)
        this.errorMessage = res.mesage;
      }
    });
  }
  emitChangeData() {
    this.inputData.value = this.inputData.data.defaultValue;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': true, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
}
