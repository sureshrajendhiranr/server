import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { MatSelect } from '@angular/material';
import { AdvancedFieldListenerService } from '../../../services/advanced-field-listener.service';
import { template } from '@angular/core/src/render3';
import { SelectorListContext } from '@angular/compiler';
import { CommonService } from '../../../shared/common/common.service';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') myInput: MatSelect;
  isEdit = false;
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}, requestFields: '', notEqualFilter: {}
  };
  isValid: boolean = true;
  fieldIsOpen = false;
  selectedObjects = [];
  isAdvanced = false;
  SelectedValueList = [];
  advanced: any;
  selectedId = []
  value = [];
  currentUser = JSON.parse(localStorage.flowPodUser);
  constructor(public commonApiService: CommonApiService,
    public advancedFieldListenerService: AdvancedFieldListenerService,
    public commonService: CommonService) { }

  ngOnInit() {
    // console.log(this.inputData)
    if ((!!this.inputData.data.isAdvanced) && this.inputData.data.isAdvanced.length > 1) {
      this.isAdvanced = true;
      let temp;
      this.advanced = JSON.parse(this.inputData.data.isAdvanced);
      if (this.advanced.type === 'depended') {
        if (!!this.inputData.isTableView) {

        } else {
          this.advancedFieldListenerService.dataSource.subscribe(res => {
            temp = res;
            if (!temp['isCreate']) {
              let t = { isCreate: { [this.advanced.dependedField]: '' } };
              Object.assign(temp, t);
            } else {
              temp['isCreate'][this.advanced.dependedField] = !temp['isCreate'][this.advanced.dependedField] ? '' :
                temp['isCreate'][this.advanced.dependedField];
            }
          });
          // console.log(temp)
          this.advancedFieldListenerService.dataSource.next(temp);
        }
      }
    }
  }
  ngAfterViewChecked() {
    if (this.isEdit && !this.fieldIsOpen && !this.isAdvanced) {
      this.fieldIsOpen = true;
      this.selectedObjects = this.inputData.value.split(',');
      // console.log(this.selectedObjects);
      this.myInput.open();
    }
  }
  comparer(o1, o2): boolean {
    return o1 && o2 ? o1 === o2 : o2 === o2;
  }
  emitChangeData() {
    // this.inputData.data.defaultValue = val;
    if (this.isAdvanced) {
      this.inputData.value = this.value;
    } else {
      this.inputData.value = this.inputData.data.defaultValue + '';
    }

    let name = this.commonService.checkToCamelCase(this.inputData.data.name)

    if (this.inputData.isView) {
      this.inputData.rowInfo[name] = this.inputData.data.defaultValue;
    }
    if (this.inputData.isTableCreate) {
      this.inputData.rowData[name] = this.isAdvanced ? this.selectedId + '' : this.inputData.data.defaultValue;
    }
    this.event.emit({
      [this.inputData.data.name]: this.isAdvanced ? this.selectedId + '' : this.inputData.value,
      'isValid': this.isValid, 'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }

  setValue(item) {
    if (!!item.length) {
      console.log(item)
      let name = this.commonService.toCamelCase(this.advanced.fieldName)
      let list = [];
      (item).forEach(i => {
        list.push(i[name]);
      });
      // console.log(list, this.inputData.data.defaultValue)
      this.inputData.data.defaultValue = list + ''
    }

  }

  toggleSelect(item) {
    let name = this.commonService.toCamelCase(this.advanced.fieldName)
    if (!!this.SelectedValueList.includes(item[name])) {
      this.SelectedValueList.splice(this.SelectedValueList.indexOf(item[name]), 1);
      this.value.splice(this.value.indexOf(item), 1);
    } else {
      this.SelectedValueList.push(item[name]);
      this.value.push(item);
    }

    if (!this.selectedId.includes(item.id)) {
      this.selectedId.push(item.id);
    } else {
      this.selectedId.splice(this.selectedId.indexOf(item.id), 1);
    }
    this.inputData.data.defaultValue = this.SelectedValueList + '';
    this.emitChangeData();
  }
  getOption(val) {
    if ((!!this.inputData.data.isAdvanced) && this.inputData.data.isAdvanced.length > 1) {
      let temp = JSON.parse(this.inputData.data.isAdvanced);
      if (temp.type === 'non-depended') {
        this.inputData.data.optionValue = [];
        this.apiObj.tableName = temp.dataTableName;
        this.apiObj.limit = 10;
        this.apiObj.page = 0;
        this.apiObj.search = { [temp.fieldName]: val };
        this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
          this.inputData.data.optionValue = res.info;
        });
      } else if (temp.type === 'auto-fill') {
        this.inputData.data.optionValue = [];
        this.apiObj.tableName = temp.dataTableName;
        this.apiObj.search = { [temp.fieldName]: val };
        this.apiObj.limit = 10;
        this.apiObj.page = 0;
        // console.log(temp)
        this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
          this.inputData.data.optionValue = res.info;
        });
      } else
        if (temp.type === 'depended') {
          this.inputData.data.optionValue = [];
          this.apiObj.tableName = temp.dataTableName;
          this.apiObj.requestFields = 'id,' + temp.fieldName;
          this.apiObj.search = { [temp.fieldName]: val };
          this.apiObj.limit = 10;
          this.apiObj.page = 0;

          // IS TABLE VIEW
          const name = this.commonService.checkToCamelCase(temp.dependedField)
          if (!!this.inputData.isTableView) {
            if (temp.filterType === 'equalTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            } else if (temp.filterType === 'notEqualTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            }
          } else if (!!this.inputData.isView) {
            if (temp.filterType === 'equalTo') {
              if (typeof (this.inputData.rowInfo[name]) !== 'string') {
                this.apiObj.filterValue = { [temp.filterValue]: [this.inputData.rowInfo[name][0]['id'] + ''] };
              } else {
                this.apiObj.filterValue = { [temp.filterValue]: [this.inputData.rowInfo[name] + ''] };
              }
            } else if (temp.filterType === 'notEqualTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowInfo[name][0]['id'] };
              } else {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowInfo[name] };
              }
            }
          } else if (!!this.inputData.isTableCreate) {
            if (temp.filterType === 'equalTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.filterValue = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            } else if (temp.filterType === 'notEqualTo') {
              if (typeof (this.inputData.rowData[name]) !== 'string') {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name][0]['id'] };
              } else {
                this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.rowData[name] };
              }
            }

          } else if (!!this.inputData.isCreate) {
            if (temp.filterType === 'equalTo') {
              this.apiObj.filterValue = { [temp.filterValue]: [this.inputData.inputValue[name] + ''] };
            } else if (temp.filterType === 'notEqualTo') {
              this.apiObj.notEqualFilter = { [temp.filterValue]: this.inputData.inputValue[name] };
            }
          }

          this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
            this.inputData.data.optionValue = res.info;
          });


        }
    }
  }
}