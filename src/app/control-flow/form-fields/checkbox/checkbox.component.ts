import { Component, OnInit, Input, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  @ViewChild('myInput') public elementRef: ElementRef;
  isEdit = false;
  isValid: boolean = true;
  currentUser = JSON.parse(localStorage.flowPodUser);
  constructor() { }

  ngOnInit() {
    (this.inputData.isView) ? this.inputData.data.defaultValue = this.inputData.value : '';
  }
  emitChangeData() {
    this.inputData.value = this.inputData.data.defaultValue;
    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': this.isValid, 'isrequired': this.inputData.data.required, 'rowId': !!this.inputData.rowId ? this.inputData.rowId.id :this.inputData.indexNo
    });
  }
}
