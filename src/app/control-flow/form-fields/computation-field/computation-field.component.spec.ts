import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputationFieldComponent } from './computation-field.component';

describe('ComputationFieldComponent', () => {
  let component: ComputationFieldComponent;
  let fixture: ComponentFixture<ComputationFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComputationFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputationFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
