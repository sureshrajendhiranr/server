import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { computational } from '../../constant';
import { CommonApiService } from '../../../services/common-api.service';

@Component({
  selector: 'app-computation-field',
  templateUrl: './computation-field.component.html',
  styleUrls: ['./computation-field.component.css']
})
export class ComputationFieldComponent implements OnInit {
  @Input('computationalSelector') computationalSelector: any;
  @Output() event = new EventEmitter();
  computationalData = [];
  computationalList = [];
  tempList = computational;
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  fieldList = [];
  selectedItem: any;
  experssion = '';
  userBasedType = ['createdBy', 'lastModifiedBy'];
  constructor(public commonApiService: CommonApiService) { }

  ngOnInit() {
    this.selectedItem = {};
    this.computationalData = this.computationalSelector.data.computational;
    if (this.computationalData) {
      this.selectedItem = this.computationalData;
      this.getField();
      this.getexperssion();
      // console.log(this.selectedItem.value, this.selectedItem.fieldList)
    }
    if (this.computationalSelector.data.type !== 'user') {
      this.tempList.forEach(i => {
        if (!this.userBasedType.includes(i.name)) {
          this.computationalList.push(i);
        }
      });
    } else {
      this.tempList.forEach(i => {
        if (this.userBasedType.includes(i.name)) {
          this.computationalList.push(i);
        }
      });
    }
    // console.log(this.computationalList)
  }
  getexperssion() {
    this.experssion = (this.selectedItem.value).slice(0, (this.selectedItem.value.length) - 1) + this.selectedItem.fieldList + '' + ')';
  }
  addItem(item) {
    this.selectedItem = {};
    this.selectedItem = item;
    if (this.computationalSelector.data.type === 'user') {
      this.computationalSelector.data['computational'] = this.selectedItem;
      this.event.emit(this.selectedItem);
    } else {
      this.getField();
      this.fieldList = [];
      this.getexperssion();
    }
  }

  setConcatenateJoin(value) {
    this.selectedItem['joinValue'] = value;
    // console.log(this.selectedItem)
  }
  addExpression(e) {
    this.selectedItem.fieldList = e;
    this.computationalSelector.data['computational'] = this.selectedItem;
    // console.log(this.selectedItem)
    this.getexperssion();
    this.event.emit(this.computationalSelector.data['computational']);
  }
  getField() {
    this.apiObj.field = 'data_table_name';
    this.apiObj.fieldValue = this.computationalSelector.dataTableName;
    this.apiObj.filterValue = {};
    if (!!this.selectedItem.fieldType) {
      this.apiObj.filterValue['type'] = [this.selectedItem.fieldType];
    }
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        res.info.forEach(i => {
          this.fieldList.push(i);
        });
      }
    });
  }
}
