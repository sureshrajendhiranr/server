import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ComputationListenerService } from '../../../services/computation-listener.service';
import { ValidationsServicesService } from '../validations-services/validations-services.service';
import { LastmodifiedListenerService } from '../../../services/lastmodified-listener.service';

@Component({
  selector: 'app-computation-field-view',
  templateUrl: './computation-field-view.component.html',
  styleUrls: ['./computation-field-view.component.css']
})
export class ComputationFieldViewComponent implements OnInit {
  @Input('inputData') inputData: any;
  @Output() event = new EventEmitter();
  data = {};
  computational: any;
  constructor(private computationListenerService: ComputationListenerService,
    public validationsServicesService: ValidationsServicesService,
    public lastmodifiedListenerService: LastmodifiedListenerService) { }
  ngOnInit() {
    // console.log(this.inputData)
    this.computational = !!this.inputData.data.computational.length ? JSON.parse(this.inputData.data.computational) : '';
    let list = [];
    this.computationListenerService.dataSource.subscribe(res => {

      this.inputData.data.defaultValue =
        this.validationsServicesService.computationalCallBack[this.computational.name](this.inputData.inputValue, this.computational);
      // console.log(this.inputData.inputValue, this.inputData.data.name);
      if (!!this.inputData.inputValue) {
        // this.inputData.inputValue[this.inputData.data.name] = this.inputData.data.defaultValue;
      }
    });
  }

  emitChange() {
    // console.log(this.inputData.data.defaultValue)

    this.event.emit({
      [this.inputData.data.name]: this.inputData.data.defaultValue,
      'isValid': true, 'isrequired': this.inputData.data.required,
      'rowId': !!this.inputData.rowId ? this.inputData.rowId.id : this.inputData.indexNo
    });
  }
  setValue(temp) {
    let list = Object.keys(temp)
    list.forEach(i => {
      temp['_' + i] = temp[i]
    });
  }
}
