import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputationFieldViewComponent } from './computation-field-view.component';

describe('ComputationFieldViewComponent', () => {
  let component: ComputationFieldViewComponent;
  let fixture: ComponentFixture<ComputationFieldViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComputationFieldViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputationFieldViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
