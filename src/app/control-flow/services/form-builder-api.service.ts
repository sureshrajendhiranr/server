import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class FormBuilderApiService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  // Contact Start
  getAllMetaData(limit, page, sortType, sortValue, search, filterValue): Observable<any> {
    const params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('sortType', sortType)
      .set('sortField', sortValue)
      .set('search', JSON.stringify(search).length !== 2 ? JSON.stringify(search) : '')
      .set('filter', JSON.stringify(filterValue).length !== 2 ? JSON.stringify(filterValue) : '');
    const requestUrl = this.baseUrl + '/metaData/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  createMetaData(body): Observable<any> {
    const requestUrl = this.baseUrl + '/metaData/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

  updateMetaData(id, body): Observable<any> {
    const requestUrl = this.baseUrl + '/metaData/' + id;
    return this.http.put(requestUrl, body).pipe(map(this.extractData));
  }

  updateMetaDataPosition(body): Observable<any> {
    const requestUrl = this.baseUrl + '/metaDataPosition/';
    return this.http.put(requestUrl, body).pipe(map(this.extractData));
  }

  deleteMetaData(id): Observable<any> {
    // const params = new HttpParams()
    //   .set('id', id);
    const requestUrl = this.baseUrl + '/metaData/' + id;
    return this.http.delete(requestUrl).pipe(map(this.extractData));
  }
  getTableFields(val): Observable<any> {
    // console.log(val)
    const params = new HttpParams()
      .set('tableName','meta_data')
      .set('field','data_table_name')
    const requestUrl = this.baseUrl + '/common/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  getMetaDataGroupBy(dataTableName): Observable<any> {
    const requestUrl = this.baseUrl + '/metaDataGroupBy/'+dataTableName;
    return this.http.get(requestUrl).pipe(map(this.extractData));
  }

  // getTableField(val): Observable<any> {
  //   console.log(val)
  //   const params = new HttpParams()
  //     .set('tableName',val)
  //   const requestUrl = this.baseUrl + '/common/';
  //   return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  // }
}
