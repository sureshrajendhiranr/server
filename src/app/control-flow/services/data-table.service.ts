import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataTableService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  getAllDataTables(obj): Observable<any> {
    const params = new HttpParams()
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('sortType', obj.sortType)
      .set('sortField', obj.sortValue)
      .set('search', JSON.stringify(obj.search).length !== 2 ? JSON.stringify(obj.search) : '')
      .set('filter', JSON.stringify(obj.filterValue).length !== 2 ? JSON.stringify(obj.filterValue) : '');
    const requestUrl = this.baseUrl + '/dataTable/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  createDataTables(body): Observable<any> {
    const requestUrl = this.baseUrl + '/dataTable/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

  updateDataTables(id, body): Observable<any> {
    const requestUrl = this.baseUrl + '/dataTable/' + id;
    return this.http.put(requestUrl, body).pipe(map(this.extractData));
  }

  deleteDataTables(id): Observable<any> {
    const requestUrl = this.baseUrl + '/dataTable/' + id;
    return this.http.delete(requestUrl).pipe(map(this.extractData));
  }

  deleteCategoriesDataTable(id, name): Observable<any> {
    const params = new HttpParams()
      .set('id', id)
      .set('data_table_name', name);
    const requestUrl = this.baseUrl + '/deleteCategoriesDataTable/';
    return this.http.delete(requestUrl, { params }).pipe(map(this.extractData));
  }
}
