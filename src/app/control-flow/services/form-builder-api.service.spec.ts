import { TestBed } from '@angular/core/testing';

import { FormBuilderApiService } from './form-builder-api.service';

describe('FormBuilderApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormBuilderApiService = TestBed.get(FormBuilderApiService);
    expect(service).toBeTruthy();
  });
});
