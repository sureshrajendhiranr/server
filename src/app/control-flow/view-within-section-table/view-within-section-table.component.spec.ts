import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWithinSectionTableComponent } from './view-within-section-table.component';

describe('ViewWithinSectionTableComponent', () => {
  let component: ViewWithinSectionTableComponent;
  let fixture: ComponentFixture<ViewWithinSectionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewWithinSectionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWithinSectionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
