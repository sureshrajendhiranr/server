import { Component, OnInit, Input } from '@angular/core';
import { CommonApiService } from '../../../app/services/common-api.service';
import { FormBuilderApiService } from '../services/form-builder-api.service';
import { CommonService } from '../../shared/common/common.service';
import { ToastService } from '../../shared/common/toast.service';
import { LastmodifiedListenerService } from '../../services/lastmodified-listener.service';


@Component({
  selector: 'app-view-within-section-table',
  templateUrl: './view-within-section-table.component.html',
  styleUrls: ['./view-within-section-table.component.css']
})
export class ViewWithinSectionTableComponent implements OnInit {
  @Input('infoData') data: any;
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'id', field: '', fieldValue: '', search: {}, filter: {}
  };
  metaDataByGroup = [];
  groupList = [];
  groupSelect = '';
  value = {};
  putBody = {};
  currentUser = JSON.parse(localStorage.flowPodUser)
  constructor(public commonApiService: CommonApiService,
    public formBuilderApiService: FormBuilderApiService,
    public commonService: CommonService,
    public toastService: ToastService,
    public lastmodifiedListenerService: LastmodifiedListenerService) { }

  ngOnInit() {
    // console.log(this.data)
    this.value = this.data.value;
    this.getMetaData();
    this.getGroups();
  }
  getGroups() {
    this.apiObj.tableName = 'Field_Groups';
    this.apiObj.field = 'data_table_name_id';
    this.apiObj.fieldValue = this.data.dataTableId;
    this.apiObj.sortValue = 'position';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
      }
    });
  }
  getMetaData() {
    this.formBuilderApiService.getMetaDataGroupBy(this.data.dataTableName).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaDataByGroup = res.info;
      }
    });
  }

  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.data.dataTableName;
    this.putBody['id'] = this.data.rowId;
    if (e.isValid) {
      let fieldList;
      this.lastmodifiedListenerService.fieldList.subscribe(res => {
        fieldList = res;
      });
      if (!!fieldList.length) {
        let temp;
        this.lastmodifiedListenerService.dataSource.subscribe(res => {
          temp = res;
        });
        fieldList.forEach(i => {
          const userInfo = { 'userName': this.currentUser.userName, 'id': this.currentUser.id + '', 'email': this.currentUser.email };
          temp['isView'][i] = [userInfo];
          this.putBody[i] = this.currentUser.id + '';
        });
        this.lastmodifiedListenerService.dataSource.next(temp);
      }

      this.commonApiService.update(this.putBody).subscribe(res => {
        if (res.status === 202) {
          if (!!e.file) {
            this.commonApiService.fileCreate(e.file).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.toastService.success(res.message);
              }
            });
          } else {
            this.toastService.success(res.message);
          }
        }
      });
    } else {
      this.toastService.success('inValid Value');
    }
    this.putBody = {};
  }
}
