import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShowHideListenerService } from '../../services/show-hide-listener.service';
import { CommonService } from '../../shared/common/common.service';
import { componentNeedsResolution } from '@angular/core/src/metadata/resource_loading';
@Component({
  selector: 'app-fields-map',
  templateUrl: './fields-map.component.html',
  styleUrls: ['./fields-map.component.css']
})
export class FieldsMapComponent implements OnInit {
  @Input('data') inputData: any;
  @Output() eventEmit = new EventEmitter();
  selector: any;
  componentValueInput = {};
  showHide = [];
  fieldList = [];
  showHideField = [];
  rowId = 0;
  constructor(public showHideListenerService: ShowHideListenerService,
    public commonService: CommonService) {


    // this.componentValueInput['isCreate'] = 1;
    // this.componentValueInput['data'] = this.selector;

  }

  ngOnInit() {
    // console.log(this.inputData)
    this.inputData.value = !!this.inputData.value ? this.inputData.value : '';
    this.selector = this.inputData;
    this.componentValueInput = this.selector;
    this.showHideListenerService.data.subscribe(res => {
      this.showHide = res;
      if (this.inputData.isTableView) {
        this.rowId = this.inputData.rowId.id;
        if (res['isTableView'] && res['isTableView'][this.rowId]) {
          this.showHideField = Object.keys(res['isTableView'][this.rowId]);
          this.showHideListenerService.TableFieldList.subscribe(res => {
            this.fieldList = res;
          });
        }
        // console.log(this.fieldList)
      } else {
        this.showHideField = Object.keys(res);
        if (this.showHideField.includes('isTableView')) {
          this.showHideField.splice(this.showHideField.indexOf('isTableView'), 1);
        }
        this.showHideListenerService.fieldList.subscribe(res => {
          this.fieldList = res;
        });
      }
    });
  }
  isShowHide(name) {
    let f = false;
    if (this.inputData.isTableView) {
      if (this.showHide['isTableView'] && this.showHide['isTableView'][this.rowId]) {
        if (!!this.showHideField.length) {
          for (let i = 0; i < this.showHideField.length; i++) {
            if (this.showHide['isTableView'][this.rowId][this.showHideField[i]].includes(this.commonService.checkToCamelCase(name))) {
              f = true;
              // console.log(f, name, this.showHide, this.showHideField, name)
              break;
            }
          }
        }
        return f;

      } else {
        return true;
      }
    } else {
      if (this.showHide) {
        if (!!this.showHideField.length) {
          for (let i = 0; i < this.showHideField.length; i++) {
            if (this.showHide[this.showHideField[i]].includes(this.commonService.checkToCamelCase(name))) {
              f = true;
              // console.log(f, name, this.showHide, this.showHideField, name)
              break;
            }
          }
        }
        return f;

      } else {
        return true;
      }
    }
  }
  test(e) {
    // console.log(e);

  }
  test1(name) {
    if (this.inputData.isTableView) {
      if (this.fieldList) {
        // console.log(this.commonService.checkToCamelCase(name))
        // console.log(name)
        if (this.fieldList.includes(this.commonService.checkToCamelCase(name))) {
          // console.log('s', false)
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    } else {
      if (this.fieldList) {
        // console.log(this.commonService.checkToCamelCase(name))
        if (this.fieldList.includes(this.commonService.checkToCamelCase(name))) {
          // console.log('s', false)
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  }
}
