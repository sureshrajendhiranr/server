import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { PopupEditFormComponent } from '../popup-edit-form/popup-edit-form.component';
import { MatDialog } from '@angular/material';
import { dataType } from '../constant';
import { TextComponent } from '../form-fields/text/text.component';
import { FormBuilderApiService } from '../services/form-builder-api.service';
import { DataShareService } from '../services/data-share.service';
import { CommonApiService } from '../../services/common-api.service';
import { ResizeEvent } from 'angular-resizable-element';
import { ToastService } from '../../shared/common/toast.service';
import { TableSectionBuilderListenerService } from '../../services/table-section-builder-listener.service';
import { CommonService } from '../../shared/common/common.service';
import { DesignationService } from '../../services/designation.service';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css']
})
export class FormBuilderComponent implements OnInit {
  @ViewChild('popupEditForm') popupEditForm: PopupEditFormComponent;
  @Input('dataTableInfo') dataTable: any;
  // width find
  @ViewChild('myDiv') theDiv: ElementRef;
  @ViewChild('exDiv') exDiv: ElementRef;
  containerWidth = 0;
  dataType = JSON.parse(JSON.stringify(dataType));
  totalField = 0;

  dataTypeTemp = JSON.parse(JSON.stringify(this.dataType));
  formFields = [];
  obj = {
    tableName: 'Field_Groups', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'id', field: 'data_table_name_id', fieldValue: '', search: [], filter: {}
  };
  formArray = [];
  formArrayIndex: number;
  imageUrl: string;
  isaddGroup: boolean = false;
  groupList = [];
  isForm: boolean = true;
  isPermission: boolean = false;
  groupName: string;
  // Row based items
  rowList = {};
  rowListArray = [];
  rowListArrayWidth = [];
  groups = {};
  isProcessEdit: boolean = false;
  metaDataGroup: any;
  isLoad = false;
  designationList: any;
  isHover = 0;
  isGroupHover = 0;
  accessLevel = 2;
  InfoData = {};
  isdeleteId: number;
  isdeleteGroup: boolean = false;
  isMoveGroup = 0;
  isMoveGroupView: boolean = false;
  onClickDiv: any;
  onResizeWidth: number;
  center = '';
  gridItems = {};
  inputValue = {};
  tempGroupName = [];
  isWorkFlow: boolean = false;
  constructor(public dialog: MatDialog,
    public formBuilderApiService: FormBuilderApiService,
    public dataShareService: DataShareService,
    public commonApiService: CommonApiService,
    private _elRef: ElementRef,
    public toastService: ToastService,
    public tableSectionBuilderListenerService: TableSectionBuilderListenerService,
    public commonService: CommonService,
    public designationService: DesignationService) { }

  ngOnInit() {
    this.getMeta();
    this.getAllGroup();
    // this.getGroupFields();
    this.getAllDesignation();
    // this.getTableFields();
    this.dataShareService.data.subscribe(data => {
      if (!!data.created) {
        this.getMeta();

        // this.getTableFields();
      }
    });

    this.InfoData = {
      name: this.dataTable.name, description: this.dataTable.description,
      icon: this.dataTable.icon, categories: this.dataTable.categories,
      id: this.dataTable.id,
      groupName: this.dataTable.groupName
    };
    console.log(this.dataTable)
  }
  ngAfterViewChecked() {
    if (!this.isWorkFlow) {
      this.containerWidth = this.theDiv.nativeElement.clientWidth;
    }
  }
  formBuilderType() {
    let temp = {};
    // this.tableSectionBuilderListenerService.dataSource.subscribe(res => {
    //   temp = res;
    // });
    temp['isForm'] = this.isForm;
    temp['isPermission'] = this.isPermission;
    temp['isWorkFlow'] = this.isWorkFlow;

    this.tableSectionBuilderListenerService.dataSource.next(temp);
  }
  drop11(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      // moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      let bodyList = [];
      let body1 = {};
      let body2 = {};
      body1['tableName'] = 'meta_data';
      body1['id'] = event.container.data[event.previousIndex]['id'];
      body1['position'] = event.container.data[event.currentIndex]['position'];
      // body1['name']=event.container.data[event.previousIndex]['name'];

      body2['tableName'] = 'meta_data';
      body2['id'] = event.container.data[event.currentIndex]['id'];
      body2['position'] = event.container.data[event.previousIndex]['position'];
      // body2['name']=event.container.data[event.currentIndex]['name'];
      bodyList.push(body1);
      bodyList.push(body2);
      // console.log(bodyList)

      this.commonApiService.update(bodyList).subscribe(res => {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      })

    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }

    // moveItemInArray(this.timePeriods, event.previousIndex, event.currentIndex);
  }
  dropList = [];
  test(row, groupId) {
    let name = groupId + ''
    if (!this.dropList.includes(name)) {
      this.dropList.push(name);
    }
    console.log(name)
  }
  withInRowDrop(e, groupName) {

    let putItem = [];
    this.metaDataGroup[groupName][e.currentIndex].items.forEach((item, index) => {
      let body = {};
      body['rowNo'] = e.previousIndex
      body['id'] = this.metaDataGroup[groupName][e.currentIndex].items[index].id;
      body['tableName'] = 'meta_data';
      if (!!this.metaDataGroup[groupName][e.currentIndex].items[index].id) {
        putItem.push(body);
      }
    });
    this.metaDataGroup[groupName][e.previousIndex].items.forEach((item, index) => {
      let body = {};
      body['rowNo'] = e.currentIndex;
      body['groupName'] = groupName;
      body['id'] = this.metaDataGroup[groupName][e.previousIndex].items[index].id;
      body['tableName'] = 'meta_data';
      if (!!this.metaDataGroup[groupName][e.previousIndex].items[index].id) {
        putItem.push(body);
      }
    });
    this.commonApiService.update(putItem).subscribe(res => {
      if (res.statusCode === 200) {
      }
    });
    let currentTemp = this.metaDataGroup[groupName][e.currentIndex].items;
    let previousTemp = this.metaDataGroup[groupName][e.previousIndex].items;
    this.metaDataGroup[groupName][e.previousIndex].items = currentTemp;
    this.metaDataGroup[groupName][e.currentIndex].items = previousTemp;
    const tempCurrentIndexWidth = this.metaDataGroup[groupName][e.currentIndex].grid;
    const tetmpPreViewIndexWidth = this.metaDataGroup[groupName][e.previousIndex].grid;
    this.metaDataGroup[groupName][e.previousIndex].grid = tempCurrentIndexWidth
    this.metaDataGroup[groupName][e.currentIndex].grid = tetmpPreViewIndexWidth
  }

  addItemDrag(rowIndex, rowItemIndex, group) {
    let dataType = this.dataType[1];
    dataType.rowNo = rowIndex;
    dataType.groupName = group.groupName;
    dataType.groupId = group.id;
    dataType.width = this.metaDataGroup[group.groupName][rowIndex].grid.split(' ')[rowItemIndex].replace('%', '');
    const dialogRef = this.dialog.open(PopupEditFormComponent, {
      width: '900px',
      data: {
        'item': dataType, 'dataTable': this.dataTable, 'isUpdate': 0,
        'position': this.totalField, 'tableViewPosition': this.totalField, 'groupList': !!this.groupList ? this.groupList : []
      }
    });
  }
  onSizeEvent(row, event) {
    document.getElementById(row.id).style.zIndex = '9999999999999999999999';
    this.onResizeWidth = Math.round(((event.size.width + 30) / this.containerWidth) * 100);
  }
  onResizeEnd(event, rowIndex, rowItemIndex, item, row): void {
    let tempy = item.grid.split(' ');
    let tlen = tempy.length - 1;
    if (!!tempy[tlen]) {
    } else {
      tempy.splice(tlen, 1);
    }
    let total = 0;
    // let valueCal = Math.round(((event.rectangle.width + 20) / this.containerWidth) * 100);
    let valueCal = Math.round(((event.size.width + 30) / this.containerWidth) * 100);
    tempy[rowItemIndex] = valueCal < 10 ? '20%' : valueCal + '%';
    tempy.forEach(i => {
      total = total + parseInt(i.replace('%', ''));
    });

    let total1 = 0;
    tempy.forEach(i => {
      total1 = total1 + parseInt(i.replace('%', ''));
    });

    const l = item.items.length;
    if (total > 100) {
      let u = parseInt(tempy[l - 1].replace('%', '')) - (total1 - 100);
      tempy[l - 1] = (parseInt(tempy[l - 1].replace('%', '')) - (total1 - 100)) + '%';
      if (!!item.items[l - 1].name) {

      } else {
        if (u < 0) {
          item.items.splice(l - 1, 1);
          tempy.splice(l - 1, 1);
          tempy[rowItemIndex] = (valueCal + (u)) + '%'
        }
      }
    } else {
      if (l === tempy.length && total < 100) {
        if (!item.items[l - 1].name) {
          if (total1 <= 100) {
            tempy[l - 1] = (parseInt(tempy[l - 1].replace('%', '')) + (100 - total1)) + '%';
          } else {
            tempy[l - 1] = (parseInt(tempy[l - 1].replace('%', '')) - (total1 - 100)) + '%';
          }
        } else {
          tempy.push((100 - total) + '%');
          item.items.push({});
        }
      }
    }
    let putItem = [];
    tempy.forEach((i, index) => {
      let body = {};
      body['width'] = parseInt(tempy[index].replace('%', ''));
      body['id'] = item.items[index].id;
      body['tableName'] = 'meta_data';
      if (!!item.items[index].id) {
        putItem.push(body);
      }
    });
    this.commonApiService.update(putItem).subscribe(res => {
      if (res.statusCode === 200) {
      }
    });
    item.grid = tempy.join(' ');
    document.getElementById(row.id).style.width = 'auto';
    this.onResizeWidth = 0;
  }

  drop(event: CdkDragDrop<[]>) {
    if (event.previousContainer === event.container) {


      // let body = {};
      // body['dataTableName'] = event.container.data[event.previousIndex]['dataTableName'];
      // body['id'] = event.container.data[event.previousIndex]['id'];
      // body['previousIndex'] = event.previousIndex;
      // body['currentIndex'] = event.currentIndex;
      // this.formBuilderApiService.updateMetaDataPosition(body).subscribe(res => {
      // if (res.statusCode === 200) {
      // // this.toastService.toast(res.message);
      // moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      // }
      // });
      // moveItemInArray(this.formArray, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
      this.dataType = JSON.parse(JSON.stringify(this.dataTypeTemp));
    }
  }

  groupNameChange(index, event) {
    // console.log(this.metaDataGroup[this.groupList[index].groupName],this.tempGroupName[index])
    const metaDataFields = [];
    // console.log(this.groupList[index].groupName);
    if (!!this.metaDataGroup[this.groupList[index].groupName]) {
      this.metaDataGroup[this.groupList[index].groupName].forEach(i => {
        i.items.forEach(j => {
          if (!!j.name) {
            j.groupName = event.target.value;
            j['tableName'] = 'meta_data';
            metaDataFields.push(j);
          }
        });
      });
    }
    console.log(this.tempGroupName)
    this.tempGroupName[index].groupName = event.target.value;
    this.tempGroupName[index]['tableName'] = 'Field_Groups';
    this.commonApiService.update(this.tempGroupName[index]).subscribe(res => {
      this.metaDataGroup[event.target.value] = this.metaDataGroup[this.groupList[index].groupName];
      this.groupList[index].groupName = event.target.value;
      // console.log(this.metaDataGroup)

      // this.commonApiService.update(metaDataFields).subscribe(res => {
      //   console.log(res)
      // });
    });
    // this.commonApiService.update(metaDataFields).subscribe(res => {
    //   console.log(res)
    // });
    // console.log(metaDataFields);
  }

  groupPositionChange(event: CdkDragDrop<any>) {
    let body = [];
    this.groupList[event.currentIndex].position = event.previousIndex;
    this.groupList[event.currentIndex]['tableName'] = 'Field_Groups';
    this.groupList[event.previousIndex].position = event.currentIndex;
    this.groupList[event.previousIndex]['tableName'] = 'Field_Groups';
    body.push(this.groupList[event.currentIndex]);
    body.push(this.groupList[event.previousIndex]);
    this.commonApiService.update(body).subscribe(res => {
      moveItemInArray(this.groupList, event.previousIndex, event.currentIndex);
      moveItemInArray(this.tempGroupName, event.previousIndex, event.currentIndex);
    });
  }
  addGroupBotton() {

  }
  changeProfilePic(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (even) => {
        if (even.target['result'].toString().startsWith('data:image')) {
          this.imageUrl = even.target['result'];
        }
      };
    }
  }
  editField(item) {
    const dialogRef = this.dialog.open(PopupEditFormComponent, {
      width: '900px',
      data: { 'item': item, 'dataTable': this.dataTable, 'isUpdate': 1 }
    });
  }

  EditView(item, v, group) {
    item.groupName = group.groupName;
    item.groupId = group.id;
    item.rowNo = !!this.metaDataGroup[group.groupName] ? this.metaDataGroup[group.groupName].length : 0;
    const dialogRef = this.dialog.open(PopupEditFormComponent, {
      width: '900px',
      data: {
        'item': item, 'dataTable': this.dataTable, 'isUpdate': v,
        'position': this.totalField, 'groupList': !!this.groupList ? this.groupList : []
      }
    });
  }

  getMeta() {
    this.totalField = 0;
    this.isLoad = true;
    this.formBuilderApiService.getMetaDataGroupBy(this.dataTable.dataTableName).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaDataGroup = res.info;
        Object.keys(res.info).forEach(i => {
          this.metaDataGroup[i].forEach(j => {
            this.totalField = this.totalField + j.items.length;
            let gridList = (j.grid.split(' '))
            let temptotal = 0;
            let a;
            gridList.forEach(x => {
              if (x != '') {
                a = parseInt(x.replace('%', ''))
                this.gridItems[a] = true;
                temptotal = temptotal + a;
              }
            });
            if (temptotal < 100) {
              j.grid = (j.grid + (100 - temptotal)) + '%';
              j.items.push({});
            }
            this.inputValue[j.name] = '';
          });
        });
      }
      this.isLoad = false;
    });
  }

  getTableFields() {
    let test = {};
    this.formBuilderApiService.getAllMetaData(100, 0, 'ASC', 'row_no', '',
      { 'data_table_name': this.dataTable.dataTableName }).subscribe(res => {
        this.formArray = [];
        res.forEach(i => {
          test[i.rowNo] = [];
          if (i.type == 'select' || i.type == 'checkbox' || i.type == 'checklist') {
            i.optionValue = i.optionValue.split(',');
          } this.formArray.push(i);
        });
      });
  }
  delete(id, rowIndex, itemIndex, groupName) {
    // this.metaDataGroup[groupName][rowIndex].grid = (this.metaDataGroup[groupName][rowIndex].grid.split(' ').splice(itemIndex, 1)) + '';
    this.formBuilderApiService.deleteMetaData(id).subscribe(res => {
      if (res.status === 202) {
        this.formArray.splice(itemIndex, 1);
        (this.metaDataGroup[groupName][rowIndex].items).splice(itemIndex, 0, {});
        // this.formArray.splice(itemIndex, 0, {});
      }
    });
  }
  createGroup(name) {
    this.commonApiService.create({ 'tableName': 'Field_Groups', 'group_name': name, 'data_table_name': this.dataTable.dataTableName, 'data_table_name_id': this.dataTable.id, position: this.groupList.length }).subscribe(res => {
      if (res.statusCode === 201) {
        this.groupList.push(res.info);
        this.tempGroupName.push(res.info);
        console.log(this.tempGroupName)
        this.groupName = '';
      }
    });
  }
  getAllGroup() {
    // this.isLoad=true;
    this.obj['fieldValue'] = this.dataTable.id;
    this.obj.sortType = 'ASC';
    this.obj.sortValue = 'position';
    this.commonApiService.getAll(this.obj).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
        this.tempGroupName = JSON.parse(JSON.stringify(res.info));
        // console.log(this.groupList)
      }
    });
    // this.isLoad=false;
  }
  getAllDesignation() {
    this.obj.tableName = 'designation';
    this.obj.field = '';
    this.designationService.getAll(this.obj).subscribe(res => {
      if (res.statusCode === 200) {
        this.designationList = res.info;
        // console.log(this.designationList)
      }
    });
  }
  addPermission(designationId, item, checked) {
    let temp = {};
    if (checked) {
      item[designationId + 'Access'] = this.accessLevel;
    }
    temp[designationId + 'Access'] = this.accessLevel;
    temp['tableName'] = 'meta_data';
    temp['id'] = item.id;
    this.commonApiService.update(temp).subscribe(res => {
      if (res.status === 202) {
        item[designationId + 'Access'] = this.accessLevel;
      }
    });
  }
  addGroupPermission(designationId, item, checked) {

    let temp = {};
    if (checked) {
      item[designationId + 'Access'] = this.accessLevel;
    }
    temp[designationId + 'Access'] = this.accessLevel;
    temp['tableName'] = 'Field_Groups';
    temp['id'] = item.id;
    this.commonApiService.update(temp).subscribe(res => {
      if (res.status === 202) {
        item[designationId + 'Access'] = this.accessLevel;
      }
    });
  }
  deleteGroup(isMove, groupName, item) {
    let deleteBody = {};
    deleteBody['tableName'] = this.dataTable.dataTableName;
    deleteBody['isMove'] = isMove;
    deleteBody['groupName'] = groupName;
    deleteBody['oldGroupName'] = item.groupName;
    this.commonApiService.deleteGroup(deleteBody).subscribe(res => {
      if (res.statusCode === 200) {
        this.getMeta();
        this.groupList.splice((this.groupList).indexOf(item), 1);
        this.tempGroupName.splice((this.groupList).indexOf(item), 1)
        this.toastService.success(res.message);
        this.isdeleteGroup = false;
      }
    });
  }
}

