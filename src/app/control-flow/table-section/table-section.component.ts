import { Component, OnInit, Output, Input, EventEmitter, HostListener } from '@angular/core';
import { PopupEditFormComponent } from '../popup-edit-form/popup-edit-form.component';
import { MatDialog } from '@angular/material';
import { dataType } from '../constant';
import { CommonApiService } from '../../services/common-api.service';
import { CommonService } from '../../shared/common/common.service';
import { ComputationListenerService } from '../../services/computation-listener.service';
import { FormBuilderApiService } from '../services/form-builder-api.service';
import { ToastService } from '../../shared/common/toast.service';
import { ValidationsServicesService } from '../form-fields/validations-services/validations-services.service';
import * as moment from 'moment';
import { AdvancedFieldListenerService } from '../../services/advanced-field-listener.service';
import { TableSectionBuilderListenerService } from '../../services/table-section-builder-listener.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-table-section',
  templateUrl: './table-section.component.html',
  styleUrls: ['./table-section.component.css']
})
export class TableSectionComponent implements OnInit {

  @Input('data') inputData: any;
  @Output() event = new EventEmitter();
  rowItems = [];
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: '', fieldValue: '', search: {}, filter: []
  };
  data = [];
  dataTable: any;
  metaData = [];
  columnsToDisplay = [];
  tempColumnsToDisplay = [];
  metaDataGroupBy = [];
  dataType = JSON.parse(JSON.stringify(dataType));
  // Selection
  selectHover: number;
  isAddField: boolean = false;
  isAllSelected = false;
  onClickFieldName = '';
  selectedItemCount = 0;
  dateData = {};
  isForm = false;
  isPermission = false;
  accessLevel = 0;
  isHover = 0;
  designationList = [];
  selectedList = [];
  // Tab event lister
  // tabObj = {matalen:0};
  metaLen = 0;
  obj = {
    tableName: 'designation', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'id', field: '', fieldValue: '', search: {}, filter: {}
  };
  dialogRef: any;
  inputValue = {};
  infoData: any;
  tableInfo: any;
  putBody = {};
  tempMetaData: any;
  currentView = JSON.parse(localStorage.currentView);
  currentUser = !!localStorage.flowPodUser ? JSON.parse(localStorage.flowPodUser) : '';
  dataTableInfo = {};

  //  Get url paramates
  getUrlParam = {
    data_table_name: '',
    data_table_id: '',
    row_id: ''
  };
  constructor(public dialog: MatDialog,
    public commonService: CommonService,
    public commonApiService: CommonApiService,
    public computationListenerService: ComputationListenerService,
    public formBuilderApiService: FormBuilderApiService,
    public toastService: ToastService,
    public validationsServicesService: ValidationsServicesService,
    public advancedFieldListenerService: AdvancedFieldListenerService,
    public tableSectionBuilderListenerService: TableSectionBuilderListenerService,
    private routingNavigate: Router,
    private route: ActivatedRoute, ) { }

  ngOnInit() {
    this.getUrlParam.row_id = this.route.snapshot.paramMap.get('row_id');
    this.getUrlParam.data_table_id = this.route.snapshot.paramMap.get('data_table_id');
    this.getUrlParam.data_table_name = this.route.snapshot.paramMap.get('data_table_name');
    this.route.snapshot.paramMap.get('data_table_id');
    if (!!this.inputData.isView) {
      this.inputData['rowInfo'] = {};
      this.inputData['rowInfo']['id'] = this.getUrlParam.row_id;
      this.getAll();

      // this.getMetaData();
      this.tempMetaData = this.inputData.data.sectionMetaData;
      this.dataTableInfo = { dataTableName: this.inputData.data.sectionTableName, dataTableId: this.inputData.data.sectionTableId };

    }
    // console.log(this.inputData.data.sectionMetaData);
    if (!!this.inputData.isTableView || !!this.inputData.isSubTableView) {
      this.inputData['rowInfo'] = this.inputData.rowId;
      this.tempMetaData = this.inputData.data.sectionMetaData;
      // console.log(this.tempMetaData)
      this.getAll();
    }

    if (!!this.inputData.isCreate && this.inputData.isCreate !== 2) {
      this.tableSectionBuilderListenerService.dataSource.subscribe(res => {
        this.isForm = res['isForm'];
        this.isPermission = res['isPermission']
        if (this.isPermission && !this.designationList.length) {
          this.getDesignation();
        }
      });
    }
    this.tempMetaData = this.inputData.data.sectionMetaData;
    this.parseMetaData();
  }

  updateData(e) {
    if (e.message === 'update') {
      this.getAll();
      this.isAllSelected = false;
      this.selectedList = [];
    }
  }
  viewSelected(item, row, Id) {
    // console.log(row, item)
    this.routingNavigate.navigate(['/view-within-section-single-view', this.inputData.data.sectionTableId
      , this.inputData.data.sectionTableName, Id, this.inputData.rowInfo.id]);
    // view-within-section-single-viewz
    this.isLoading = false;
    row['mblInfo'] = this.inputData.inputValue;
    this.apiObj.tableName = 'data_tables';
    this.apiObj.field = 'id';
    this.apiObj.fieldValue = this.inputData.data.sectionTableId;
    this.apiObj.sortValue = 'id';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.tableInfo = res.info[0];
        this.infoData = {
          value: row, rowId: Id, dataTableId: this.inputData.data.sectionTableId,
          dataTableName: this.inputData.data.sectionTableName, tableInfo: this.tableInfo, inputValue: row
        };
        this.isLoading = true;
        // this.dialogRef = this.dialog.open(item, {
        //   // width: this.tableInfo.height + '%',
        //   // height: this.tableInfo.width + '%',
        //   width: '98%',
        //   height: '98%',
        //   autoFocus: false,
        //   disableClose: true
        // });
      }
    });
  }

  keyEvent(event, rowIndex, metaIndex) {
    event.preventDefault();
    let id = '';
    if (metaIndex + 1 === this.metaLen) {
      id = (rowIndex + 1) + '_' + this.metaData[0].name;
    } else {
      id = (rowIndex) + '_' + this.metaData[metaIndex + 1].name;
    }
    const style = getComputedStyle(document.body);
    const color = style.getPropertyValue('--color-primary');
    document.getElementById(id + 'td').style.border = '1px solid ' + color;
    const d = document.getElementById(id);
    setTimeout(() => {
      d.click();
    }, 10);
  }

  drop(event: CdkDragDrop<any>) {
    let bodyList = [];
    let body1 = {};
    let body2 = {};
    // body1['dataTableName'] = this.metaData[event.previousIndex]['dataTableName'];
    body1['tableName'] = 'meta_data';
    body1['id'] = this.metaData[event.previousIndex]['id'];
    body1['position'] = event.currentIndex;
    bodyList.push(body1);
    body2['tableName'] = 'meta_data';
    // body2['dataTableName'] = this.metaData[event.previousIndex]['dataTableName'];
    body2['id'] = this.metaData[event.currentIndex]['id'];
    body2['position'] = event.previousIndex;
    bodyList.push(body2);
    this.commonApiService.update(bodyList).subscribe(res => {
      moveItemInArray(this.metaData, event.previousIndex, event.currentIndex);
    });
  }

  getDesignation() {
    this.obj.tableName = 'designation';
    this.obj.field = '';
    this.commonApiService.getAll(this.obj).subscribe(res => {
      if (res.statusCode === 200) {
        this.designationList = res.info;
        // this.getMetaData();
      }
    });
  }

  addPermission(designation, item, checked) {
    let temp = {};
    if (checked) {
      temp[designation] = this.accessLevel;
    }
    temp[designation] = this.accessLevel;
    temp['tableName'] = 'meta_data';
    temp['id'] = item.id;
    this.commonApiService.update(temp).subscribe(res => {
      if (res.status === 202) {
        item[designation] = this.accessLevel;
      }
    });
  }


  eventHandle(e) {
    this.putBody = {};
    // console.log(this.inputData)
    if (!!this.inputData.isView || !!this.inputData.isTableView || !!this.inputData.isSubTableView) {
      if (!!e.autoFill) {
        e.autoFill.forEach(i => {
          this.data[e.rowId - 1][i] = this.inputValue[this.commonService.toCamelCase(i)];
          this.putBody[i] = this.inputValue[this.commonService.toCamelCase(i)];

        });
      }
      this.data[e.rowId - 1][this.onClickFieldName] = e[this.onClickFieldName];
      this.putBody[this.onClickFieldName] = e[this.onClickFieldName];
      this.updateFields(e);

    } else {
      if (!!e.file) {
        e['isSectionTable'] = true;
        this.event.emit(e);
      }
      this.data[e.rowId - 1][this.onClickFieldName] = e[this.onClickFieldName];
      this.event.emit({ [this.inputData.data.sectionTableId]: this.data, isSectionTable: true });
    }
    this.computationListenerService.dataSource.next(this.inputValue);
  }

  isSelectAllCheck(e) {
    this.isAllSelected = e.checked;
    let totalCount = 0;
    let checkedCount = 0;
    this.data.forEach(i => {
      i.checked = e.checked;
      if (i.checked) { checkedCount++; }
      totalCount++;
    });
    this.selectedItemCount = checkedCount;
  }
  isSelectCheck(e) {
    if (!e) { this.isAllSelected = false; }
    let totalCount = 0;
    let checkedCount = 0;
    this.data.forEach(i => {
      if (i.checked) { checkedCount++; }
      totalCount++;
    });
    this.selectedItemCount = checkedCount;
    if (totalCount === checkedCount) { this.isAllSelected = true; }
  }
  selectedData() {
    this.selectedList = [];
    this.data.forEach(i => {
      if (i.checked) {
        this.selectedList.push(i);
      }
    });
  }
  test(name, type, defaultValue, row) {
    return row[name] ? row[name] : (type === 'datetime') ? defaultValue[type] : defaultValue;
  }
  // addRow(count) {
  //   setTimeout(() => {
  //     this.addRow1(count)
  //   }, 50);
  // }
  addRow(count) {
    let temp = {};
    this.tempColumnsToDisplay.forEach(i => {
      if (i.type === 'datetime' || i.type === 'date' || i.type === 'time') {
        if (!!this.dateData[i.name] && !!this.dateData[i.name].length) {
          if (i.type === 'datetime') {
            temp[i.name] = moment(this.validationsServicesService.setDefaultDate(this.dateData[i.name])).format('YYYY-MM-DD hh:mm:ss');
          } else if (i.type === 'date') {
            temp[i.name] = moment(this.validationsServicesService.setDefaultDate(this.dateData[i.name])).format('YYYY-MM-DD hh:mm:ss');
          }
        } else {
          // console.log("sss")
          if (i.type === 'datetime') {
            temp[i.name] = '';
          } else if (i.type === 'date') {
            temp[i.name] = '';
          }
        }
      } else {
        temp[i.name] = !!i.defaultValue ? i.defaultValue : '';
      }
    });
    if (this.inputData.isView) {
      this.createRows(this.data, count, temp);
    } else {
      let totalLen = this.data.length;
      for (let i = 1; i <= count; i++) {
        temp['id'] = totalLen + i;
        this.data.push(JSON.parse(JSON.stringify(temp)));
        this.isDependedAdvnced();
      }
      this.event.emit({ [this.inputData.data.sectionTableId]: this.data, isSectionTable: true });
    }
  }
  addField() {
    let dataType = this.dataType[1];
    dataType['dataTableName'] = this.inputData.data.sectionTableId;
    dataType['dataTableId'] = this.inputData.data.sectionTableName;
    dataType['groupName'] = this.inputData.data.groupName;
    dataType['groupId'] = this.inputData.data.groupId;
    this.inputData['dataTable'] = {
      dataTableName: this.inputData.data.sectionTableName,
      name: this.inputData.data.sectionTableName,
      id: this.inputData.data.sectionTableId
    };
    const dialogRef = this.dialog.open(PopupEditFormComponent, {
      width: '900px',
      data: {
        'item': dataType, 'dataTable': this.inputData.dataTable,
        'isUpdate': 0, 'position': this.metaData.length,
        'groupList': []
      }
    });
  }
  EditField(item) {
    this.inputData['dataTable'] = {
      dataTableName: this.inputData.data.sectionTableName,
      name: this.inputData.data.sectionTableName,
      id: this.inputData.data.sectionTableId
    };
    const dialogRef = this.dialog.open(PopupEditFormComponent, {
      width: '900px',
      data: {
        'item': item, 'dataTable': this.inputData.dataTable, 'isUpdate': 1
      }
    });
  }
  deleteField(item, itemIndex) {
    this.formBuilderApiService.deleteMetaData(item.id).subscribe(res => {
      if (res.status === 202) {
        this.metaData.splice(itemIndex, 1);
        this.toastService.success(res.message);
      }
    });
  }
  getTableInfo() {
    this.apiObj.sortValue = 'id';
    this.apiObj.tableName = 'data_tables';
    this.apiObj.field = 'data_table_id';
    this.apiObj.fieldValue = this.inputData.data.sectionTableId;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      this.dataTable = res.info;
    });
  }

  getMetaData() {
    this.columnsToDisplay = [];
    this.apiObj.sortValue = 'row_no';
    this.apiObj.tableName = 'meta_data';
    this.apiObj.field = 'data_table_id';
    this.apiObj.sortType = 'asc';
    this.apiObj.sortValue = 'position';
    this.apiObj.fieldValue = this.inputData.data.sectionTableId;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaLen = res.info.length;
        // if (res.info.length === this.tempColumnsToDisplay.length) {
        //   this.tempColumnsToDisplay.forEach(i => {
        //     if (i.checked) { this.columnsToDisplay.push(i.name); }
        //   });
        //   this.metaData = this.tempColumnsToDisplay;
        // } else {}

      }
    });
  }
  parseMetaData() {
    this.tempMetaData.forEach(i => {
      i.name = this.commonService.checkToCamelCase(i.name);
      if (i.type === 'datetime' || i.type === 'time' || i.type === 'date') {
        this.dateData[i.name] = !!i.defaultValue.length ? i.defaultValue : '';
      }
      if (i.type !== 'table') {
        this.columnsToDisplay.push(i.name);
        this.tempColumnsToDisplay.push(i);
        if (!!this.inputData.isTableView) {
          if (i.tableViewPosition !== -1) {
            this.metaData.push(i);
          }
        } else
          if (!!this.inputData.isSubTableView) {
            if (i.sectionTableViewPosition !== -1) {
              this.metaData.push(i);
            }
          } else {
            this.columnsToDisplay.push(i.name);
            this.tempColumnsToDisplay.push(i);
            this.metaData.push(i);
          }

      }
    });
    if (this.inputData.isSubTableView) {
      this.metaData.sort(function (a, b) {
        return a.sectionTableViewPosition - b.sectionTableViewPosition;
      });
    }
    // console.log(this.columnsToDisplay)
    let temp = [], advanced;
    let t = {};
    t['isTableCreate'] = {};
    t['isTableCreate'][this.inputData.data.sectionTableId] = {};
    this.metaData.forEach(i => {
      if (!!i.isAdvanced.length) {
        advanced = JSON.parse(i.isAdvanced);
        if (advanced.type === 'depended') {
          this.advancedFieldListenerService.dataSource.subscribe(res => {
            temp = res;
          });
          let name = this.commonService.toCamelCase(advanced.dependedField);
          if (!this.data.length) {
            t['isTableCreate'][this.inputData.data.sectionTableId][1] = {};
            t['isTableCreate'][this.inputData.data.sectionTableId][1][name] = '';
            if (!temp['isTableCreate']) {
              Object.assign(temp, t)
            } else {
              Object.assign(temp['isTableCreate'], t['isTableCreate'])
            }
            this.advancedFieldListenerService.dataSource.next(temp);
            // console.log(temp)
          } else {

            this.data.forEach((j, index) => {
              t['isTableCreate'][this.inputData.data.sectionTableId][index + 1] = {};
              if (this.inputData.isView) {
                if (typeof (j[name]) !== 'string') {
                  let fname = this.commonService.toCamelCase(advanced.filterValue)
                  t['isTableCreate'][this.inputData.data.sectionTableId][index + 1][name] = j[name][0][fname];
                } else {
                  t['isTableCreate'][this.inputData.data.sectionTableId][index + 1][name] = j[name];
                }
              } else {
                t['isTableCreate'][this.inputData.data.sectionTableId][index + 1][name] = '';
              }
              if (!temp['isTableCreate']) {
                Object.assign(temp, t)
              } else {
                Object.assign(temp['isTableCreate'], t['isTableCreate'])
              }
              this.advancedFieldListenerService.dataSource.next(temp);
            });
          }

          // console.log(temp['isTableCreate'][this.inputData.data.sectionTableId])
        }
      }
    });
    if (this.inputData.isCreate !== 2 && !(this.inputData.isTableView)) {
      this.columnsToDisplay.push('addField');
    }
  }
  isLoading: boolean = false;
  getAll() {
    this.isLoading = true;
    this.apiObj.sortValue = 'id';
    this.apiObj.fieldValue = this.inputData.rowInfo.id;
    this.apiObj.field = this.inputData.data.dataTableId + '_id';
    this.apiObj.tableName = this.inputData.data.sectionTableName;
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
        // console.log(this.data)
        this.isLoading = false;

        // this.getMetaData();
      }
    });
  }
  // Create rows
  createRows(data, count, temp) {
    let postBody = [];
    // let temp = {};
    for (let i = 1; i <= count; i++) {
      temp['tableName'] = this.inputData.data.sectionTableName;
      temp[this.inputData.data.dataTableId + '_id'] = this.inputData.rowInfo.id;
      let temp1 = JSON.stringify(temp);
      temp1 = JSON.parse(temp1)
      Object.keys(temp1).forEach(i => {
        if (typeof (temp1[i]) === 'object') {
          temp1[i] = temp1[i][0]['id'];
        }
      });
      postBody.push(temp1);
      // this.data.push(JSON.parse(JSON.stringify(temp)));
    }
    this.commonApiService.create(postBody).subscribe(res => {
      // if (res.statusCode === 201) {
      res.forEach(i => {
        this.isDependedAdvnced();
        temp.id = i.info.id;
        this.data.push(temp);
      });
      this.toastService.success('Successful created ');
      // }
    });
  }
  isDependedAdvnced() {
    let list = [], tempRes;
    this.advancedFieldListenerService.dataSource.subscribe(res => {
      tempRes = res;
    });
    if (tempRes['isTableCreate'] && tempRes['isTableCreate'][this.inputData.data.sectionTableId]) {
      list = Object.keys(tempRes['isTableCreate'][this.inputData.data.sectionTableId]);
      let rowNo = list.length + 1;
      let f = JSON.stringify(tempRes['isTableCreate'][this.inputData.data.sectionTableId][list.length]);

      let newRowInfo = { [rowNo]: JSON.parse(f) };
      Object.assign(tempRes['isTableCreate'][this.inputData.data.sectionTableId], newRowInfo);
      this.advancedFieldListenerService.dataSource.next(tempRes);
    }
  }

  // Update Field
  updateFields(e) {
    let putBody = {};
    this.putBody['tableName'] = this.inputData.data.sectionTableName;
    this.putBody['id'] = this.data[e.rowId - 1].id;
    // this.putBody[this.onClickFieldName] = e[this.onClickFieldName];
    // console.log(this.putBody)
    if (e.isValid) {
      this.commonApiService.update(this.putBody).subscribe(res => {
        if (res.status === 202) {
          if (!!e.file) {
            this.commonApiService.fileCreate(e.file).subscribe(fileRes => {
              if (fileRes.statusCode === 200) {
                this.toastService.success(res.message);
              }
            });
          } else {
            this.toastService.success(res.message);
          }
        }
      });
    } else {
      this.toastService.success('inValid Value');
    }
  }

  // Delete Row
  deleteRow(id, index, type) {

    //
    let idList = [];
    let temp = [];
    if (this.selectedItemCount) {
      this.data.forEach((i, index) => {
        if (i.checked) {
          idList.push(i.id);
        } else {
          temp.push(i);
        }
      });
    } else {
      idList.push(id);
      this.data.splice(index, 1);
      temp = this.data;
    }
    // Api call based (like view page )
    if (this.inputData.isView) {
      id = idList + '';
      this.commonApiService.delete(id, this.inputData.data.sectionTableName).subscribe(res => {
        if (res.status === 200) {
          this.toastService.success(res.message);
          this.data = temp;
          this.selectedItemCount = 0;
        }
      });
    } else {
      this.data = temp;
      this.selectedItemCount = 0;
    }
  }



  filedHeight() {
    var textarea = document.getElementById("textarea");
    textarea.oninput = function () {
      textarea.style.height = "";
      textarea.style.height = Math.min(textarea.scrollHeight) + "px";
    };
  }
  // Resize events
  onSizeEvent(row, event) {
    document.getElementById(row.name + row.dataTableName).style.zIndex = '9999999999999999999999';
    document.getElementById(row.name + row.dataTableName).style.minWidth = event.size.width + 'px';
  }
  widthSizeUpdated(row, event) {
    let body = {};
    body['id'] = row['id'];
    body['width'] = event.size.width;
    body['tableName'] = 'meta_data';
    this.commonApiService.update(body).subscribe(res => {
      if (res.status === 202) {

      }
    });
  }
}
