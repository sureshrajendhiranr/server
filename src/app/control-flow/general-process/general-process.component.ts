import { Component, OnInit } from '@angular/core';
import { MatDialog, MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { DataTableService } from '../services/data-table.service';
import { CommonApiService } from '../../services/common-api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-general-process',
  templateUrl: './general-process.component.html',
  styleUrls: ['./general-process.component.css']
})
export class GeneralProcessComponent implements OnInit {
  getDataObject = {
    limit: 100,
    page: 0,
    sortType: 'DESC',
    sortValue: 'created_on',
    search: '',
    filterValue: { is_subtable: ['0'], }
  };
  data = [];
  groupList = [];
  apiObj = {
    tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  Infodata = [];
  constructor(public commonApiService: CommonApiService,
    public dataTableService: DataTableService,
    public router: Router) { }

  ngOnInit() {
    this.get();
  }
  test(val, item) {
    this.storeCurrentProcess(item);
    this.router.navigate(['main/view/' + val]);
  }

  storeCurrentProcess(item) {
    localStorage.setItem('currentView', JSON.stringify(item));
  }

  get() {
    this.dataTableService.getAllDataTables(this.getDataObject).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
        this.getTableGroup();
      }
    });
  }
  getTableGroup() {
    this.apiObj.tableName = 'Table_Groups';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      let temp = [];
      this.groupList = res.info;
      this.groupList.forEach(i => {
        temp[i.name] = [];
        this.data.forEach(j => {
          if (i.name === j.groupName) {
            temp[i.name].push(j);
          }
        });
      });
      this.Infodata = temp;
    });
  }
  searchProcess(val) {
    let getBody = {
      tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
      field: '', fieldValue: '', search: { 'name': val }, filterValue: { is_subtable: ['0'] }
    };
    this.commonApiService.getAll(getBody).subscribe(res => {
      this.data = res.info;
      this.getTableGroup();
    });
  }
}
