import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { DataTableService } from '../services/data-table.service';
import { CommonApiService } from '../../services/common-api.service';
import { FormControl } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { iconList } from '../constant';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ToastService } from '../../shared/common/toast.service';
import { CommonService } from '../../shared/common/common.service';
import { DesignationService } from '../../services/designation.service';

@Component({
  selector: 'app-create-flow',
  templateUrl: './create-flow.component.html',
  styleUrls: ['./create-flow.component.css']
})
export class CreateFlowComponent implements OnInit {
  @ViewChild('formBuilder') openFormBuilder: any;
  getDataObject = {
    limit: 100,
    page: 0,
    sortType: 'DESC',
    sortValue: 'created_on',
    search: '',
    filterValue: { is_subtable: ['0'] }
  };
  createDataTable = { name: '', description: '', icon: '', categories: '', groupName: '' };
  dataTablePassInfo: any;
  data: any;
  dialogRef: any;
  // Delete
  deleteDialogRef: any;
  deleteInput: any;
  deleteitemTemp = { id: -1, index: -1, groupName: '', groupId: '' };

  formBuilderDialogRef: any;
  isNameExist = false;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  categoryCtrl = new FormControl();
  flowView = ['Side Navigation'];
  categoryList = ['General', 'Side Navigation'];
  // Icons
  iconList = iconList;
  materialIconList = [];
  filterIconList = [];
  groupList = [];
  Infodata = {};
  tempGroup: string;
  apiObj = {
    tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  isPermission = false;
  isEditProcess = true;
  isHover = true;
  groupName = ''
  designationList = [];
  accessLevel = 0;
  isEditClick = false;
  groupId = 0;
  @ViewChild('categoryInput') categoryInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @ViewChild('autoIcon') matAutocompleteIcon: MatAutocomplete;
  constructor(public dialog: MatDialog,
    public dataTableService: DataTableService,
    public commonApiService: CommonApiService,
    public toastService: ToastService,
    public commonService: CommonService,
    public designationService: DesignationService) {
    this.iconList.categories.forEach(i => { i.icons.forEach(j => { this.materialIconList.push(j.id); }); });
    this.filterIconList = this.materialIconList;
  }

  ngOnInit() {
    this.get();
  }

  editgroupName(id) {
    document.getElementById(id).focus();
  }
  addPermission(designationId, item, checked) {
    let temp = {};
    if (checked) {
      item[designationId + 'Access'] = this.accessLevel;
    }
    temp[designationId + 'Access'] = this.accessLevel;
    temp['tableName'] = 'data_tables';
    temp['id'] = item.id;
    this.commonApiService.update(temp).subscribe(res => {
      if (res.status === 202) {
        item[designationId + 'Access'] = this.accessLevel;
      }
    });
  }
  getDesignation() {
    this.apiObj.tableName = 'designation';
    this.apiObj.field = '';
    this.designationService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {

        this.designationList = res.info;
        // console.log(this.designationList)
      }
    });
  }

  openFormModel(item) {
    this.dataTablePassInfo = item;
    this.formBuilderOpenModel();
  }
  createFlowOpenModel(item, groupName, groupId) {
    this.groupName = groupName;
    this.groupId = groupId;
    this.dialogRef = this.dialog.open(item, {
      width: '400px',
      autoFocus: false,
      disableClose: true,
      // hasBackdrop: false
    });
  }

  formBuilderOpenModel() {
    this.formBuilderDialogRef = this.dialog.open(this.openFormBuilder, {
      width: '98%',
      height: '97%',
      autoFocus: false,
      disableClose: true,
    });
  }

  get() {
    this.dataTableService.getAllDataTables(this.getDataObject).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
        (this.data).forEach(i => {
          this.categoryList.push(i.name);
        });
        this.getTableGroup();
      }
    });
  }
  getTableGroup() {
    this.apiObj.tableName = 'Table_Groups';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      let temp = [];
      this.apiObj.tableName = '';
      this.groupList = res.info;
      this.groupList.forEach(i => {
        temp[i.id] = [];
        this.data.forEach(j => {
          if (i.name === j.groupName) {
            temp[i.id].push(j);
          }
        });
      });
      this.Infodata = temp;
    });
  }
  designationEvent(e) {
    // this.createDataTable['access'] = e + '';
  }

  dialogClose(e) {
    this.dialogRef.close();
  }


  // Create General group in Group table
  createGroup(dataTable) {
    this.commonApiService.create({
      'tableName': 'Field_Groups', 'group_name': 'General Information',
      'data_table_name': dataTable.dataTableName, 'data_table_name_id': dataTable.id
    }).subscribe(res => {
      if (res.statusCode === 201) {
        this.formBuilderOpenModel();
      }
    });
  }
  deleteDialogOpen(item, index, temp) {
    this.deleteitemTemp['id'] = item.id;
    this.deleteitemTemp['index'] = index;
    this.deleteDialogRef = this.dialog.open(temp, {
      width: '400px',
      autoFocus: false,
    });
  }
  delete() {
    this.deleteDialogRef.close();
    console.log(this.Infodata, this.deleteitemTemp)
    this.dataTableService.deleteDataTables(this.deleteitemTemp.id).subscribe(res => {
      if (res.statusCode === 202) {
        this.Infodata[this.deleteitemTemp.groupId].splice(this.deleteitemTemp.index, 1);
      }
    });
  }
  searchProcess(val) {
    let getBody = {
      tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
      field: '', fieldValue: '', search: { 'name': val }, filterValue: { is_subtable: ['0'] }
    };
    this.commonApiService.getAll(getBody).subscribe(res => {
      this.data = res.info;
      this.getTableGroup();
    });
  }

  // Update Group name
  updateGroupName(value, id) {
    let body = {};
    body['tableName'] = 'Table_Groups';
    body['id'] = id;
    body['name'] = value;
    this.commonApiService.update(body).subscribe(res => {
      if (res.status === 202) {
        this.toastService.success(res.message)
      }
    });

  }
}
