import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog, MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatDialogRef } from '@angular/material';
import { DataTableService } from '../../services/data-table.service';
import { CommonApiService } from '../../../services/common-api.service';
import { FormControl } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { iconList } from '../../constant';
import { ToastService } from '../../../shared/common/toast.service';
import { CommonService } from '../../../shared/common/common.service';
import { CreateFlowComponent } from '../create-flow.component';
import { DesignationService } from '../../../services/designation.service';

@Component({
  selector: 'app-createflow-model',
  templateUrl: './createflow-model.component.html',
  styleUrls: ['./createflow-model.component.css']
})
export class CreateflowModelComponent implements OnInit {
  @ViewChild('formBuilder') openFormBuilder: any;
  getDataObject = {
    limit: 10,
    page: 0,
    sortType: 'DESC',
    sortValue: 'created_on',
    search: '',
    filterValue: ''
  };
  dataTablePassInfo: any;
  data: any;
  dialogRef: any;
  formBuilderDialogRef: any;
  isNameExist = false;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  categoryCtrl = new FormControl();
  flowView = [{ name: 'Side Navigation', id: 0 }];
  // categoryList = ['General', 'Side Navigation'];
  categoryList = [
    { name: 'Side Navigation' }
  ]
  listOfFields = ['categories', 'createdOn', 'dataTableName', 'description', 'icon', 'id', 'lastModified', 'name', 'groupName'];
  // Icons
  iconList = iconList;
  materialIconList = [];
  filterIconList = [];
  designationAcess = [];
  isLoading: boolean = false;
  apiObj = {
    tableName: 'Table_Groups', limit: 100, page: 0, sortType: 'ASC',
    sortValue: 'id', field: '', fieldValue: '', search: {}, filter: {}
  };
  groupList = [];
  @ViewChild('categoryInput') categoryInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  // @ViewChild('autoGroup') matAutocomplete: MatAutocomplete;
  @ViewChild('autoIcon') matAutocompleteIcon: MatAutocomplete;
  @Input('data') InfoData: any;
  @Input('groupInfo') groupInfo: any;
  @Output('eventEmit') eventEmit = new EventEmitter();

  createDataTable = {
    name: '', description: '',
    icon: '', categories: '',
    groupName: '', groupId: ''
  };
  constructor(public dialog: MatDialog, public dataTableService: DataTableService,
    public commonApiService: CommonApiService, public toastService: ToastService,
    public commonService: CommonService, public createFlowComponent: CreateFlowComponent,
    public designationService: DesignationService) {
    this.iconList.categories.forEach(i => { i.icons.forEach(j => { this.materialIconList.push(j.id); }); });
    this.filterIconList = this.materialIconList;
  }

  ngOnInit() {
    if (this.groupInfo) {
      this.createDataTable.groupName = this.groupInfo.groupName;
      this.createDataTable.groupId = this.groupInfo.groupId;
    }
    this.createDataTable = this.InfoData ? this.InfoData : this.createDataTable;
    this.get();
    this.getGroup('');
    // this.getdesignation();
  }

  getdesignation() {
    let tempDesignation = [];
    this.apiObj.tableName = 'designation';
    this.apiObj.search = {};
    this.designationService.getAll(this.apiObj).subscribe(res => {
      res.info.forEach(i => {
        tempDesignation.push(i.designation);
      });
      (tempDesignation).forEach(i => {
        if (this.createDataTable[i] === 1) {
          this.designationAcess.push(i);
        }
      });
    });
  }
  // add(event: MatChipInputEvent): void {
  //   console.log(event.value)
  //   if (!this.matAutocomplete.isOpen) {
  //     const input = event.input;
  //     const value = event.value;
  //     if ((value || '')) {
  //       this.flowView.push(value);
  //     }

  //     // Reset the input value
  //     if (input) {
  //       input.value = '';
  //     }

  //     this.categoryCtrl.setValue(null);
  //   }
  // }
  displayName(val) {
    console.log(val);
    return val ? val.name : '';
  }

  remove(fruit): void {
    let indexNo = -1;
    this.flowView.forEach((i, index) => {
      if (i.name === fruit.name) { indexNo = index; };
    });

    if (indexNo >= 0) {
      this.flowView.splice(indexNo, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (!(this.flowView).includes(event.option.value) && this.categoryList.includes(event.option.value)) {
      this.flowView.push(event.option.value);
    }
    this.categoryInput.nativeElement.value = '';
    this.categoryCtrl.setValue(null);
  }


  createFlowOpenModel(createFlowModel) {
    this.dialogRef = this.dialog.open(createFlowModel, {
      width: '400px',
      autoFocus: false,
      disableClose: true
    });
  }

  formBuilderOpenModel() {
    this.formBuilderDialogRef = this.dialog.open(this.openFormBuilder, {
      width: '98%',
      height: '97%',
      autoFocus: false
    });
  }

  get() {
    this.dataTableService.getAllDataTables(this.getDataObject).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
        (this.data).forEach(i => {
          this.categoryList.push(i);
        });
      }
    });
  }
  getGroup(val) {
    let getBody = {
      tableName: 'Table_Groups', limit: 100, page: 0, sortType: 'ASC',
      sortValue: 'id', field: '', fieldValue: '', search: { display_value: val }, filter: {}
    };
    this.commonApiService.getAll(getBody).subscribe(res => {
      if (res.statusCode === 200) {
        this.groupList = res.info;
        this.apiObj.search = {};
      }
    });
  }
  addGroup(val) {
    let postBody = {
      tableName: 'Table_Groups',
      name: val,
      displayValue: val
    }
    this.commonApiService.create(postBody).subscribe(res => {
      if (res.statusCode === 201) {
        this.toastService.success(res.statusMessage);
      }
    });
  }
  designationEvent(e) {
    e.forEach(i => {
      Object.assign(this.createDataTable, i);
    });
  }
  dialogClose() {
    this.eventEmit.emit(true);
  }
  createUpdate() {
    this.createDataTable.name = this.createDataTable.name.trim();
    this.isLoading = true;
    let categoriesListTemp = [];
    this.flowView.forEach(i => {
      if (i.name !== 'Side Navigation') {
        categoriesListTemp.push(i.id);
      } else if (i.name === 'Side Navigation') {
        categoriesListTemp.push('Side Navigation');
      }
    });
    this.createDataTable['categories'] = categoriesListTemp + '';
    let groupId = this.createDataTable['groupId']
    delete this.createDataTable['groupId'];
    if (!this.InfoData) {
      // console.log(this.InfoData)
      this.dataTableService.createDataTables(this.createDataTable).subscribe(res => {
        if (res.statusCode === 201) {
          this.data.push(res.info);

          this.createFlowComponent.Infodata[groupId].push(res.info);
          this.dialogClose();
          this.dataTablePassInfo = res.info;

          this.createDataTable = { name: '', description: '', icon: '', categories: '', groupName: '', groupId: '' };
          this.isLoading = false;
          this.formBuilderOpenModel();
        }
      });
    } else {
      this.createDataTable['dataTableName'] = (this.createDataTable.name).split(' ').join('_');
      // this.createDataTable['name'] = (this.createDataTable.name).split(' ').join('_');
      this.dataTableService.updateDataTables(this.InfoData.id, this.createDataTable).subscribe(res => {
        if (res.status === 202) {
          this.toastService.success(res.message);
          this.dialogClose();
          this.isLoading = false;
          // this.formBuilderOpenModel();

        }
      });
    }

  }

  // Create General group in Group table
  createGroup(dataTable) {
    this.commonApiService.create({
      'tableName': 'Field_Groups', 'group_name': 'General Information',
      'data_table_name': dataTable.dataTableName, 'data_table_name_id': dataTable.id
    }).subscribe(res => {
      if (res.statusCode === 201) {
        this.formBuilderOpenModel();
      }
    });
  }

  // Check Exist
  isCheckExist() {
    if (this.createDataTable.name.length) {
      this.commonApiService.isCheckExist({ tableName: 'data_tables', field: 'name', fieldValue: this.createDataTable.name }).subscribe(res => {
        if (res.statusCode === 200) {
          this.isNameExist = res.check;
        }
      });
    }

  }
  iconsFilter(val) {
    this.filterIconList = this.materialIconList.filter(i => i.includes(val))
  }
}
