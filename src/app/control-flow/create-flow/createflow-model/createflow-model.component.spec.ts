import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateflowModelComponent } from './createflow-model.component';

describe('CreateflowModelComponent', () => {
  let component: CreateflowModelComponent;
  let fixture: ComponentFixture<CreateflowModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateflowModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateflowModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
