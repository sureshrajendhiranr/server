import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public dataSource = new BehaviorSubject<any>({});
  data = this.dataSource.asObservable();
  
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  login(body): Observable<any> {
    const requestUrl = this.baseUrl + '/loginValidation/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

}
