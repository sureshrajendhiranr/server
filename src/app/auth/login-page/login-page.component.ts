import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { LoginService } from '../services/login.service';
// import { ToastService } from 'src/app/shared/services/toast.service';
import { Router } from '@angular/router';
import { window } from 'rxjs/operators';
import { ToastService } from '../../shared/common/toast.service';
import { CommonApiService } from '../../services/common-api.service';
import { DesignationService } from '../../services/designation.service';
// import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  passwordType = 'password';

  loginFormGroup = new FormGroup({
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required)
  });
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filterValue: {}
  };
  currentUser: any;
  userInfo = JSON.parse(localStorage.getItem('airliftUser'));
  designation: any;
  // adminRole = environment.ACL;
  constructor(private loginService: LoginService, private route: Router,
    private toastService: ToastService,
    public commonApiService: CommonApiService, public designationService: DesignationService) { }

  ngOnInit() {

  }

  togglePasswordType() {
    this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
  }

  login() {
    const body = {};
    Object.assign(body, this.loginFormGroup.value);
    this.loginService.login(body).subscribe(res => {
      if (res.statusCode === 200) {
        localStorage.setItem('flowPodUser', JSON.stringify(res.info));
        localStorage.setItem('x-token', res.token);
        this.currentUser = res.info;
        // this.getCurrentUserdesignation();
        localStorage.setItem('currentView', JSON.stringify(res.landingPage[0]));
        location.replace('/main/view/' + res.landingPage[0].dataTableName);
      } else {
        this.toastService.success(res.message);
      }
    });
  }
  // getCurrentUserdesignation() {
  //   this.apiObj.tableName = 'designation';
  //   this.apiObj.field = 'id';
  //   this.apiObj.fieldValue = this.currentUser.designation;
  //   this.designationService.getAll(this.apiObj).subscribe(res => {
  //     if (res.statusCode === 200) {
  //       localStorage.setItem('flowPodUserDesgination', JSON.stringify(res.info[0]));
  //     }
  //   });
  // }
}
