import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import * as moment from 'moment';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private route: Router) {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method === 'GET') {

        } else if (request.method === 'POST') {
            request = request.clone({

                // body: {
                //     ...request.body, createdOn: moment().format("YYYY-MM-DD HH:mm:ss"),
                //     lastModified: moment().format("YYYY-MM-DD HH:mm:ss")
                // },
            });
        } else if (request.method === 'PUT') {
            // request = request.clone({
            //     body: { ...request.body, lastModified: moment().format("YYYY-MM-DD HH:mm:ss") },
            // });
        }

        // add authorization header with jwt token if available
        const airliftUser = JSON.parse(localStorage.getItem('flowPodUser'));
        const token = localStorage.getItem('x-token');
        if (airliftUser && token) {
            request = request.clone({
                setHeaders: {
                    'x-token': token,
                    'current-time': moment().format('YYYY-MM-DD HH:mm:ss')
                }
            });
        } else {
            // window.location.replace("/login");
            this.route.navigateByUrl('/login');
        }

        return next.handle(request);
    }
}
