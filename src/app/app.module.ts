import { ControlFlowModule } from './control-flow/control-flow.module';
import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CreateFlowComponent } from './control-flow/create-flow/create-flow.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TokenInterceptor } from './auth/token/token.interceptor';
import { ErrorsInterceptor } from './auth/errors/errors.interceptor';
import { ViewModule } from './view/view.module';

import { CreateComponent } from './view/create/create.component';
import { ViewComponent } from './view/view/view.component';

import { AngularDraggableModule } from 'angular2-draggable';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { NotificationsComponent } from './notify/notifications/notifications.component';
import { AnnouncementsComponent } from './notify/announcements/announcements.component';
import { AdminSettingsModule } from './admin-settings/admin-settings.module';


import { FilterModule } from './filter/filter.module';
import { TimetableModule } from './timetable/timetable.module';
import { AcademicInfoModule } from './academic-info/academic-info.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    NotificationsComponent,
    AnnouncementsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    AngularDraggableModule,
    // Modules
    ControlFlowModule,
    DashboardModule,
    ViewModule,
    AuthModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AdminSettingsModule,
    FilterModule,
    TimetableModule,
    AcademicInfoModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ErrorsInterceptor, multi: true },],
  bootstrap: [AppComponent],
  entryComponents: [CreateFlowComponent]
})
export class AppModule { }
