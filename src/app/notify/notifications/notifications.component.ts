import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {


  // API get Object
  currentUser = JSON.parse(localStorage.flowPodUser);
  apiObj = {
    user_id: 0, limit: 100, page: 0
  };
  apiObjNotify = {
    userId: 0
  };
  page = 0;
  notifications = [];
  seen = false;
  notify: any;
  // notifications1 = [];

  Colors: Array<any> = ["rgb(255, 82, 122)", "rgb(51, 148, 219)", "rgb(38, 206, 103)", "rgb(221, 173, 42)", "rgb(41, 188, 171)"];
  constructor(private commonApiService: CommonApiService) { }

  ngOnInit() {
    // this.getNotification();
    this.getNotificationGeneral(0);
  }

  markAsRead() {
    this.apiObjNotify.userId = this.currentUser.id;
    this.commonApiService.getMarkAsRead(this.apiObjNotify).subscribe(res => {
      // this.notificationsCount = res;
      if (res.statusCode === 200) {
        this.seen = true;
        // this.notificationsCount = res.count;
      }
    });
  }



  getNotificationGeneral(page) {
    this.apiObj.limit = 6;
    this.apiObj.page = page;
    this.page = page;
    this.apiObj.user_id = this.currentUser.id;
    this.commonApiService.getNotificationGeneral(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        if (!!res.info.length) {
          if (this.page === 0) {
            this.notifications = res.info;
            if (this.seen === true) {
              this.notify = '1';
            }
          } else {
            res.info.forEach(i => {
              this.notifications.push(i);
            });
          }
        }
      }
    });
  }

  // Random Colors Avatar

  getColors(index) {
    let num = this.getnumber(index);
    return this.Colors[num];
  }

  getnumber(data) {

    let i = data;
    if (i > this.Colors.length - 1) {

      i = i - this.Colors.length;
      if (i < this.Colors.length) {
        return i;
      }
      else {
        this.getnumber(i);
      }

    }
    else {
      return i;
    }


  }

  // Scroll Event

  scroll(e) {
    if (document.getElementById('notificationDiv').offsetHeight + document.getElementById('notificationDiv').scrollTop >=
      document.getElementById('notificationDiv').scrollHeight) {
      this.page = this.page + 1;
      this.getNotificationGeneral(this.page);
    }
  }

}