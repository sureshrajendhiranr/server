import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {
  // API get Object
  apiObj = {
    tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'name',
    field: '', fieldValue: '', search: {}, filter: []
  };
  page = 0;
  notifications = [];

  constructor(private commonApiService: CommonApiService) { }

  ngOnInit() {
    this.getNotification(0);
  }
  getNotification(page) {
    this.apiObj.limit = 6;
    this.apiObj.page = page;
    this.page = page;
    this.apiObj.tableName = 'Notifications';
    this.apiObj.sortValue = 'createdOn';
    this.apiObj.sortType = 'DESC';
    this.apiObj.search = {};
    this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        if (!!res.info.length) {
          if (this.page === 0) {
            this.notifications = res.info;
          } else {
            res.info.forEach(i => {
              this.notifications.push(i);
            });
          }
          // this.notifications = res.info;
          // this.updateNotification(res.info);
        }
      }
    });
  }

  scroll(e) {
    if (document.getElementById('notificationDiv').offsetHeight + document.getElementById('notificationDiv').scrollTop >=
      document.getElementById('notificationDiv').scrollHeight) {
      this.page = this.page + 1;
      this.getNotification(this.page);
    }
  }

}
