import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }
  search(body): Observable<any> {
    const requestUrl = this.baseUrl + '/search/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }
}
