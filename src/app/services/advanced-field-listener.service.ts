import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvancedFieldListenerService {
  res = {};
  public dataSource = new BehaviorSubject<any>(this.res);
  data = this.dataSource.asObservable();
  constructor() { }
}
