import { TestBed } from '@angular/core/testing';

import { ComputationListenerService } from './computation-listener.service';

describe('ComputationListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComputationListenerService = TestBed.get(ComputationListenerService);
    expect(service).toBeTruthy();
  });
});
