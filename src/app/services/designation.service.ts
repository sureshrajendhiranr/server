import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DesignationService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  //Common service
  getAll(obj): Observable<any> {
    const params = new HttpParams()
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('sortType', obj.sortType)
      .set('sortField', obj.sortValue)
      .set('field', obj.field)
      .set('fieldValue', obj.fieldValue)
      .set('search', JSON.stringify(!!obj.search ? obj.search : '').length !== 2 ? JSON.stringify(obj.search) : '')
      .set('filter', JSON.stringify(!!obj.filterValue ? obj.filterValue : '').length !== 2 ? JSON.stringify(obj.filterValue) : '');
    const requestUrl = this.baseUrl + '/designation/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  create(body): Observable<any> {
    const requestUrl = this.baseUrl + '/designation/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

  update(id, body): Observable<any> {
    const requestUrl = this.baseUrl + '/designation/' + id;
    return this.http.put(requestUrl, body).pipe(map(this.extractData));
  }

  delete(id): Observable<any> {
    const requestUrl = this.baseUrl + '/designation/'+id;
    return this.http.delete(requestUrl).pipe(map(this.extractData));
  }
}
