import { TestBed } from '@angular/core/testing';

import { ShowHideListenerService } from './show-hide-listener.service';

describe('ShowHideListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShowHideListenerService = TestBed.get(ShowHideListenerService);
    expect(service).toBeTruthy();
  });
});
