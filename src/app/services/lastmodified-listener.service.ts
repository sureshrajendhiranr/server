import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LastmodifiedListenerService {
  res = {};
  public dataSource = new BehaviorSubject<any>(this.res);
  data = this.dataSource.asObservable();
  temp = []
  public field = new BehaviorSubject<any>(this.temp);
  fieldList = this.field.asObservable();
  constructor() { }
}
