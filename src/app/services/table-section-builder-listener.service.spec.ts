import { TestBed } from '@angular/core/testing';

import { TableSectionBuilderListenerService } from './table-section-builder-listener.service';

describe('TableSectionBuilderListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableSectionBuilderListenerService = TestBed.get(TableSectionBuilderListenerService);
    expect(service).toBeTruthy();
  });
});
