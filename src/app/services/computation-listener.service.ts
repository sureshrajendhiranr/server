import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComputationListenerService {
  res = {};
  list = [];
  public dataSource = new BehaviorSubject<any>(this.res);
  data = this.dataSource.asObservable();
  public fieldSource = new BehaviorSubject<any>(this.list);
  fields = this.fieldSource.asObservable();
  constructor() { }
}
