import { TestBed } from '@angular/core/testing';

import { AdvancedFieldListenerService } from './advanced-field-listener.service';

describe('AdvancedFieldListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvancedFieldListenerService = TestBed.get(AdvancedFieldListenerService);
    expect(service).toBeTruthy();
  });
});
