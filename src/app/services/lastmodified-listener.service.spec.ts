import { TestBed } from '@angular/core/testing';

import { LastmodifiedListenerService } from './lastmodified-listener.service';

describe('LastmodifiedListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LastmodifiedListenerService = TestBed.get(LastmodifiedListenerService);
    expect(service).toBeTruthy();
  });
});
