import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShowHideListenerService {
  res = {};
  fields = [];
  tablefield = [];
  public dataSource = new BehaviorSubject<any>(this.res);
  data = this.dataSource.asObservable();
  public FieldDataSource = new BehaviorSubject<any>(this.fields);
  fieldList = this.FieldDataSource.asObservable();
  public TableViewField = new BehaviorSubject<any>(this.tablefield);
  TableFieldList = this.TableViewField.asObservable();
  constructor() { }
}
