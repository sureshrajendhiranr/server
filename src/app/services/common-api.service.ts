import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CommonApiService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  //Common service
  getAll(obj): Observable<any> {
    const params = new HttpParams()
      .set('tableName', obj.tableName)
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('sortType', obj.sortType)
      .set('sortField', obj.sortValue)
      .set('field', obj.field)
      .set('fieldValue', obj.fieldValue)
      .set('requestFields', (!!obj.requestFields) ? obj.requestFields : '')
      .set('search', JSON.stringify(!!obj.search ? obj.search : '').length !== 2 ? JSON.stringify(obj.search) : '')
      .set('filter', JSON.stringify(!!obj.filterValue ? obj.filterValue : '').length !== 2 ? JSON.stringify(obj.filterValue) : '');
    const requestUrl = this.baseUrl + '/common/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  create(body): Observable<any> {
    const requestUrl = this.baseUrl + '/common/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

  update(body): Observable<any> {
    const requestUrl = this.baseUrl + '/common/';
    return this.http.put(requestUrl, body).pipe(map(this.extractData));
  }

  delete(id, tableName): Observable<any> {
    const params = new HttpParams()
      .set('id', id)
      .set('tableName', tableName)
    const requestUrl = this.baseUrl + '/common/';
    return this.http.delete(requestUrl, { params }).pipe(map(this.extractData));
  }

  // Mail validation
  mailValidation(email): Observable<any> {
    const params = new HttpParams()
      .set('email', email);
    const requestUrl = this.baseUrl + '/mailValidate/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  // getDistinct
  getDistinct(obj): Observable<any> {
    const params = new HttpParams()
      .set('tableName', obj.tableName)
      .set('field', obj.field)
      .set('fieldValue', !!obj.fieldValue ? obj.fieldValue : '')
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('notEqualFilter', JSON.stringify(!!obj.notEqualFilter ? obj.notEqualFilter : '').length !== 2 ?
        JSON.stringify(obj.notEqualFilter) : '')
      .set('filter', JSON.stringify(!!obj.filterValue ? obj.filterValue : '').length !== 2 ? JSON.stringify(obj.filterValue) : '');
    const requestUrl = this.baseUrl + '/getDistinct/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getCommon(obj): Observable<any> {
    const params = new HttpParams()
      .set('tableName', obj.tableName)
      .set('field', obj.field)
      .set('fieldValue', obj.fieldValue)
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('sortType', obj.sortType)
      .set('sortField', obj.sortValue)
      .set('requestFields', obj.requestFields)
      .set('search', JSON.stringify(!!obj.search ? obj.search : '').length !== 2 ? JSON.stringify(obj.search) : '')
      .set('notEqualFilter', JSON.stringify(!!obj.notEqualFilter ? obj.notEqualFilter : '').length !== 2 ?
        JSON.stringify(obj.notEqualFilter) : '')
      .set('filter', JSON.stringify(!!obj.filterValue ? obj.filterValue : '').length !== 2 ? JSON.stringify(obj.filterValue) : '');
    const requestUrl = this.baseUrl + '/common/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }
  getWithoutPermission(obj): Observable<any> {
    const params = new HttpParams()
      .set('tableName', obj.tableName)
      .set('field', obj.field)
      .set('fieldValue', obj.fieldValue)
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('sortType', obj.sortType)
      .set('sortField', obj.sortValue)
      .set('requestFields', !!obj.requestFields ? obj.requestFields : '')
      .set('search', JSON.stringify(!!obj.search ? obj.search : '').length !== 2 ? JSON.stringify(obj.search) : '')
      .set('notEqualFilter', JSON.stringify(!!obj.notEqualFilter ? obj.notEqualFilter : '').length !== 2 ?
        JSON.stringify(obj.notEqualFilter) : '')
      .set('filter', JSON.stringify(!!obj.filterValue ? obj.filterValue : '').length !== 2 ? JSON.stringify(obj.filterValue) : '');
    const requestUrl = this.baseUrl + '/getOption/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  // File Upload
  fileCreate(body): Observable<any> {
    const requestUrl = this.baseUrl + '/files/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }


  // Check Exist
  isCheckExist(obj): Observable<any> {
    const params = new HttpParams()
      .set('tableName', obj.tableName)
      .set('field', obj.field)
      .set('fieldValue', obj.fieldValue);
    const requestUrl = this.baseUrl + '/checkExist/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getCount(tableName): Observable<any> {
    const params = new HttpParams()
      .set('tableName', tableName);
    const requestUrl = this.baseUrl + '/getCount/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  deleteTableData(tableName): Observable<any> {
    const params = new HttpParams()
      .set('tableName', tableName);
    const requestUrl = this.baseUrl + '/deleteTableData/';
    return this.http.delete(requestUrl, { params }).pipe(map(this.extractData));
  }

  deleteGroup(obj): Observable<any> {
    const params = new HttpParams()
      .set('tableName', obj.tableName)
      .set('isMove', obj.isMove)
      .set('oldGroupName', obj.oldGroupName)
      .set('groupName', obj.groupName);
    const requestUrl = this.baseUrl + '/deleteGroup/';
    return this.http.delete(requestUrl, { params }).pipe(map(this.extractData));
  }
  getSubProcessTables(id): Observable<any> {
    const requestUrl = this.baseUrl + '/dataTableSectionSideBar/' + id;
    return this.http.get(requestUrl).pipe(map(this.extractData));
  }

  getNotificationGeneral(obj): Observable<any> {
    const params = new HttpParams()
      .set('limit', obj.limit)
      .set('page', obj.page)
      .set('user_id', obj.user_id);
    const requestUrl = this.baseUrl + '/notificationGeneral/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getNotificationCount(obj): Observable<any> {
    const params = new HttpParams()
      .set('userId', obj.userId);
    const requestUrl = this.baseUrl + '/notificationCount/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }


  getMarkAsRead(obj): Observable<any> {
    const params = new HttpParams()
      .set('userId', obj.userId);
    const requestUrl = this.baseUrl + '/notificationMarkasRead/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  // Data table info get
  getDataTableInfo(id): Observable<any> {
    const requestUrl = this.baseUrl + '/dataTable/' + id;
    return this.http.get(requestUrl).pipe(map(this.extractData));
  }

  //Common service
  get(body): Observable<any> {
    const requestUrl = this.baseUrl + '/get/';
    return this.http.post(requestUrl, body).pipe(map(this.extractData));
  }

}
