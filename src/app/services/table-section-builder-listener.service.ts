import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TableSectionBuilderListenerService {
  res = { isForm: true, isPermission: false };
  public dataSource = new BehaviorSubject<any>(this.res);
  data = this.dataSource.asObservable();
  constructor() { }
}
