import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from './auth/services/login.service';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('notification_audio') notificationAudio: ElementRef;
  currentUser: any;
  isMain = false;
  constructor(private loginService: LoginService,
    private router: Router) {

  }
  ngOnInit() {
    if (!!localStorage.flowPodUser) {
      this.currentUser = JSON.parse(localStorage.flowPodUser);
      this.wsCon();
    } else {
      this.router.navigate(['login']);
    }

    // Get the notification permission for user
    if (Notification.permission !== 'granted') {
      Notification.requestPermission();
    }
    // if (!localStorage.flowPodUser) {
    //   this.router.navigate(['login']);
    //   this.isMain = true;
    // } else {
    //   this.isMain = false;
    // }
  }




  // Websocket connection make
  wsCon() {
    let _self = this;
    let socket = new WebSocket(environment.ws + this.currentUser.id);
    // socket['signOut'] = this.signOut();
    // socket.onopen = function (e) {
    //   socket.send('My name is John');
    // };

    setInterval(() => {
      if (socket.readyState === 3) {
        console.log('reconnecting...');
        socket = new WebSocket(environment.ws + this.currentUser.id);
      } else {
       }
    }, 5000);
    socket.onmessage = function (event) {
      const data = JSON.parse(event.data);
      if (data.statusCode === 200) {
        _self.wsToModuleRedirect(data);
      } else if (event.data.statusCode === 403) {
      }
    };
  }

  wsToModuleRedirect(data) {
    if (data.module === 'notification') {
      this.notificationsWsEmit(data);
    }
  }


  notificationsWsEmit(data) {
    if (data.info.action === 'add_count') {
      if (Notification.permission === 'granted') {
        new Notification('New notification received...');
      }
      this.notificationAudio.nativeElement.play();
    }
  }
}
