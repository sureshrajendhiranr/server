import { TestBed } from '@angular/core/testing';

import { AcademicApiService } from './academic-api.service';

describe('AcademicApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AcademicApiService = TestBed.get(AcademicApiService);
    expect(service).toBeTruthy();
  });
});
