import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AcademicApiService {
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  getHandlingClass(obj): Observable<any> {
    const params = new HttpParams()
      .set('userId', obj.userId)
      .set('date', obj.date);
    const requestUrl = this.baseUrl + '/handlingClassByHour/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getStudentAttendance(obj): Observable<any> {
    const params = new HttpParams()
      .set('userId', obj.userId)
      .set('degree', obj.degree)
      .set('department', obj.department)
      .set('batch', obj.batch)
      .set('section', obj.section)
      .set('date', obj.date)
      .set('academic_year', obj.academic_year)
      .set('split', obj.split)
      .set('dayOrder', obj.dayOrder)
      .set('semester', obj.semester);
    const requestUrl = this.baseUrl + '/getStudentAttendance/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getStudentMarkInfo(obj): Observable<any> {
    const params = new HttpParams()
      .set('userId', obj.userId)
      .set('degree', obj.degree)
      .set('department', obj.department)
      .set('batch', obj.batch)
      .set('section', obj.section)
      .set('subject', obj.subject)
      .set('assessmentName', obj.assessmentName)
      .set('semester', obj.semester);
    const requestUrl = this.baseUrl + '/getAssessementMark/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getAssessementList(obj): Observable<any> {
    const params = new HttpParams()
      .set('userId', obj.userId)
      .set('degree', obj.degree)
      .set('department', obj.department)
      .set('batch', obj.batch)
      .set('section', obj.section)
      .set('subject', obj.subject)
      .set('semester', obj.semester);
    const requestUrl = this.baseUrl + '/getAssessementList/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

  getReportFilter(name): Observable<any> {
    const requestUrl = this.baseUrl + '/assessmentFilter/' + name;
    return this.http.get(requestUrl).pipe(map(this.extractData));
  }
  getReportStudent(obj): Observable<any> {
    const params = new HttpParams()
      .set('UserId', !!obj.UserId ? obj.UserId : '')
      .set('Degrees', !!obj.Degrees ? obj.Degrees : '')
      .set('Departments', !!obj.Departments ? obj.Departments : '')
      .set('Batches', !!obj.Batches ? obj.Batches : '')
      .set('Sections', !!obj.Sections ? obj.Sections : '')
      .set('Subjects', !!obj.Subjects ? obj.Subjects : '')
      .set('Semesters', !!obj.Semesters ? obj.Semesters : '')
      .set('Academic_Years', !!obj.Academic_Years ? obj.Academic_Years : '')
      .set('Assessments_Name', !!obj.Assessments_Name ? obj.Assessments_Name : '');
    const requestUrl = this.baseUrl + '/assessmentReport/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }


}
