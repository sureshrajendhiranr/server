import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../view/services/student.service';
import { AcademicApiService } from '../service/academic-api.service';
import { CommonApiService } from '../../services/common-api.service';
import { CommonService } from '../../shared/common/common.service';

@Component({
  selector: 'app-assesment-mark',
  templateUrl: './assesment-mark.component.html',
  styleUrls: ['./assesment-mark.component.css']
})
export class AssesmentMarkComponent implements OnInit {
  currentUser = JSON.parse(localStorage.flowPodUser);
  currentView = JSON.parse(localStorage.currentView);
  classList = [];
  selectedClass: any;
  AssessementList = [];
  selectedAssessment: any;
  studentMarkList = [];
  tempMetaData = [];
  putBody = {};
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '', search: {}, filter: {}, notEqualFilter: {}, requestFields: ''
  };


  univ_exam_field = ['univExternalMark', 'univInternalMark', 'grade'];
  optionalField = ['univExternalMark', 'univInternalMark', 'grade', 'classTestMark'];

  isLoadedAssessmentList: boolean = false;
  metaData = [];
  constructor(public studentService: StudentService,
    public academicApiService: AcademicApiService,
    public commonApiService: CommonApiService,
    public commonService: CommonService) { }

  ngOnInit() {
    this.getHandlingClass();
    this.getMetaData();
  }

  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.currentView.dataTableName;
    this.putBody['id'] = e['rowId'];
    const rowId = e['rowId'];
    if (e.isValid) {
      this.commonApiService.update(this.putBody).subscribe(res => {
        console.log(res);
      });
    }

    this.putBody = {};
  }
  getHandlingClass() {
    this.studentService.getHandlingClassess(this.currentUser.id, '').subscribe(res => {
      if (res.statusCode === 200) {
        this.classList = res.info;
        if (!!res.info.length) {
          this.selectedClass = res.info[0];
          this.getAssessementInfo();
        }
      }
    });
  }
  getAssessementInfo() {
    this.AssessementList = [];
    const getBody = {
      section: this.selectedClass.section,
      semester: this.selectedClass.semester,
      department: this.selectedClass.department[0].id + '',
      degree: this.selectedClass.degree[0].id + '',
      batch: this.selectedClass.batch[0].id + '',
      subject: this.selectedClass.subjectName[0].id + ''
    };
    getBody['userId'] = this.currentUser.id + '';
    this.academicApiService.getAssessementList(getBody).subscribe(res => {
      if (res.statusCode === 200) {
        this.AssessementList = res.info;
        this.studentMarkList = [];
        if (!!res.info.length) {
          this.selectedAssessment = res.info[0];
          this.getStudentMarkInfo();
        }
      }
    });

  }
  getStudentMarkInfo() {
    this.studentMarkList = [];
    const getBody = {
      semester: this.selectedAssessment.semester,
      department: this.selectedAssessment.department[0].id + '',
      degree: this.selectedAssessment.degree[0].id + '',
      batch: this.selectedAssessment.batch[0].id + '',
      section: this.selectedClass.section,
      subject: this.selectedAssessment.subject[0].id + '',
      assessmentName: this.selectedAssessment.assessmentType[0].id + ''
    };
    getBody['userId'] = this.currentUser.id + '';
    this.academicApiService.getStudentMarkInfo(getBody).subscribe(res => {
      if (res.statusCode === 200) {
        this.studentMarkList = res.info;
        this.setDisplayMetaData();
      }
    });
  }

  getMetaData() {
    this.apiObj.fieldValue = this.currentView.id + '';
    this.apiObj.sortValue = 'table_view_position';
    this.commonApiService.getCommon(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaData = res.info;

      }
    });

  }

  setDisplayMetaData() {
    this.tempMetaData = [];
    const isUnivExam = this.selectedAssessment.assessmentType[0].shortName === 'UNIV' ? true : false;
    this.metaData.forEach(i => {
      i.name = this.commonService.checkToCamelCase(i.name);
      if (i.tableViewPosition !== -1) {
        if (isUnivExam) {
          if (this.optionalField.includes(i.name)) {
            if (this.univ_exam_field.includes(i.name)) {
              this.tempMetaData.push(i);
            }
          } else {
            if (i.name === 'classRetestMark') {
              if (!!this.selectedAssessment.retestPolicy) {
                this.tempMetaData.push(i);
              }
            } else {
              this.tempMetaData.push(i);
            }
          }
        } else {
          if (this.optionalField.includes(i.name)) {
            if (!this.univ_exam_field.includes(i.name)) {
              this.tempMetaData.push(i);
            }
          } else {
            if (i.name === 'classRetestMark') {
              if (!!this.selectedAssessment.retestPolicy) {
                this.tempMetaData.push(i);
              }
            } else {
              this.tempMetaData.push(i);
            }
          }
        }
      }
    });
  }
}
