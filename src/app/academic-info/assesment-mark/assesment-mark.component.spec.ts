import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssesmentMarkComponent } from './assesment-mark.component';

describe('AssesmentMarkComponent', () => {
  let component: AssesmentMarkComponent;
  let fixture: ComponentFixture<AssesmentMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssesmentMarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssesmentMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
