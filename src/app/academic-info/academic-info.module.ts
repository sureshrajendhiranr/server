import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcademicCalendarComponent } from './academic-calendar/academic-calendar.component';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AttendanceComponent } from './attendance/attendance.component';
import { AssesmentMarkComponent } from './assesment-mark/assesment-mark.component';
import { ControlFlowModule } from '../control-flow/control-flow.module';
import { SubjectSplitAllocationComponent } from './subject-split-allocation/subject-split-allocation.component';
import { AssesmentMarkReportComponent } from './assesment-mark-report/assesment-mark-report.component';
import { ViewModule } from '../view/view.module';

@NgModule({
  declarations: [AcademicCalendarComponent, AttendanceComponent, AssesmentMarkComponent, SubjectSplitAllocationComponent, AssesmentMarkReportComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ControlFlowModule,
    ViewModule
  ]
})
export class AcademicInfoModule { }
