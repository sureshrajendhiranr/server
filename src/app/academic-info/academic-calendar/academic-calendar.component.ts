import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import * as moment from 'moment';
import { CommonService } from '../../shared/common/common.service';
import { ToastService } from '../../shared/common/toast.service';

@Component({
  selector: 'app-academic-calendar',
  templateUrl: './academic-calendar.component.html',
  styleUrls: ['./academic-calendar.component.css']
})
export class AcademicCalendarComponent implements OnInit {
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '417', search: {}, filter: {}, notEqualFilter: {}, requestFields: ''
  };
  metaData: any;
  data = [];
  emptyRow = {};
  displayColumn = [];
  postBody = [];
  isEdit = 0;
  calendarList = [];
  selectedCalendarGroup = {};
  countManual = 0;
  selectedDates = [];
  dateInput = ''
  constructor(public commonApiService: CommonApiService,
    public commonService: CommonService,
    public toastService: ToastService) { }

  ngOnInit() {
    this.getMetaData();
    // this.getData();
    this.getCalendarGroup()
  }
  opendate() {
    // console.log(this.isEdit, !!this.isEdit)
    if (!!this.isEdit) {
      setTimeout(() => {
        let id = document.getElementById('date');
        if (id !== null) {
          id.click();
        }
      }, 10);
    }
  }
  getCalendarGroup() {
    this.apiObj.tableName = 'Academic_Calender_Group';
    this.apiObj.fieldValue = '';
    this.apiObj.field = '';
    this.apiObj.sortValue = 'id';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.calendarList = res.info;
        if (!!res.info.length) {
          this.selectedCalendarGroup = res.info[0];
          this.getData();
        }
      }
    });
  }
  getData() {
    this.data = [];
    this.apiObj.tableName = 'Academic_Calendar';
    this.apiObj.field = 'calendarGroup';
    this.apiObj.fieldValue = this.selectedCalendarGroup['academicCalendarCode'];
    this.apiObj.sortValue = 'id';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
        this.data.forEach(i => {
          i['selected'] = false;
        });
        if (!this.data.length) {
          this.data.push(this.emptyRow);
        }
      }
    });
  }
  addDate(e) {
    let date = moment(e).format('DD-MM-YYYY');
    this.selectedDates.push(date);
    this.dateInput = '';
  }
  removeDate(index) {
    this.selectedDates.splice(index, 1);
  }
  addRow(count) {
    this.postBody = [];
    let month;
    let monthDays = 0;
    let isMonth = false;
    let isdayOrderValid = false;
    const temp = this.data[this.data.length - 1];
    if (count === 'forMonth' || count === 'exceptSelected') {
      monthDays = moment(temp['date']).daysInMonth();
      month = moment(temp['date']).format('YYYY-MM-DD');
      isMonth = true;
    }

    let dayOrder = 0;
    for (let i = this.data.length - 1; i >= 0; i--) {
      let tempData = this.data[i];
      if (tempData['dayOrder'] !== 'na' && !isdayOrderValid) {
        dayOrder = parseInt(tempData['dayOrder'].split('-')[1]);
        isdayOrderValid = true;
      }

      if (moment(month).month === moment(tempData['date']).month && isMonth && (count === 'forMonth' || count === 'exceptSelected')) {
        let f = moment(tempData['date']).date();

        monthDays = monthDays - f;
        isMonth = false;
      }

    }
    if (monthDays > 0) {
      count = monthDays;
    }
    for (let i = 1; i <= count; i++) {
      const postBody = {};
      postBody['tableName'] = 'Academic_Calendar';
      postBody['calendarGroup'] = this.selectedCalendarGroup['academicCalendarCode'];
      Object.assign(postBody, this.emptyRow);
      postBody['date'] = (moment(temp['date']).add(i, 'days')).format('YYYY-MM-DD');
      let date = (moment(temp['date']).add(i, 'days')).format('DD-MM-YYYY');
      if (moment(postBody['date']).format('dddd') === 'Sunday') {
        postBody['dayType'] = 'Holiday';
        postBody['description'] = 'First Day of the Weak';
        postBody['dayOrder'] = 'na';
      } else {
        if (!this.selectedDates.includes(date)) {
          postBody['dayType'] = 'working day';
          if (postBody['dayOrder']) {
            if (dayOrder >= 6) {
              dayOrder = 0;
            }
            dayOrder = dayOrder + 1;
            postBody['dayOrder'] = 'day-' + dayOrder;
          }
        } else {
          postBody['dayOrder'] = 'na';
        }
      }
      this.postBody.push(postBody);
    }
    this.commonApiService.create(this.postBody).subscribe(res => {
      if (!!res && !!res.length) {
        res.forEach(i => {
          i['selected'] = false;
          this.data.push(i.info);
        });
        this.toastService.success('Created Successfully');
      }
    });
  }
  delete(row) {
    this.commonApiService.delete(row.id, 'Academic_Calendar').subscribe(res => {
      if (res.status === 200) {
        this.data.splice(this.data.indexOf(row), 1);
        this.toastService.success(res.message);
      }
    });
  }
  update(id, value, name, row) {
    let putBody = {};
    putBody['tableName'] = 'Academic_Calendar';
    putBody['id'] = id + '';
    if (name === 'date') {
      putBody['date'] = moment(value).format('YYYY-MM-DD');
      // row.day = moment(value).format('dddd');
    } else {
      putBody[name] = value;
    }
    this.commonApiService.update(putBody).subscribe(res => {
      if (res.status === 202) {
        this.toastService.success(res.message);
      }
    });
  }
  getMetaData() {
    this.apiObj.field = 'data_table_name';
    this.apiObj.fieldValue = 'Academic_Calendar';
    this.apiObj.sortType = 'ASC';
    this.apiObj.sortValue = 'tableViewPosition';
    this.commonApiService.getCommon(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaData = res.info;
        this.metaData.forEach(i => {
          if (i.tableViewPosition !== -1) {
            i.name = this.commonService.toCamelCase(i.name);
            if (i.type === 'date') {
              this.emptyRow[i.name] = moment().format('YYYY-MM-DD');
            } else {
              this.emptyRow[i.name] = i.defaultValue;
            }
            this.displayColumn.push(i);
          }

        });
      }
    });
  }
}
