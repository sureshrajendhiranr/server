import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssesmentMarkReportComponent } from './assesment-mark-report.component';

describe('AssesmentMarkReportComponent', () => {
  let component: AssesmentMarkReportComponent;
  let fixture: ComponentFixture<AssesmentMarkReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssesmentMarkReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssesmentMarkReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
