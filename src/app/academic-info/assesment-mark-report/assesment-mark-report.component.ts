import { Component, OnInit } from '@angular/core';
import { AcademicApiService } from '../service/academic-api.service';

@Component({
  selector: 'app-assesment-mark-report',
  templateUrl: './assesment-mark-report.component.html',
  styleUrls: ['./assesment-mark-report.component.css']
})
export class AssesmentMarkReportComponent implements OnInit {

  filterItemObject = {
    'Degrees': { display_value: 'Degree', table_name: 'Degrees', filterList: [] },
    'Batches': { display_value: 'Batch', table_name: 'Batches', filterList: [] },
    'Academic_Years': { display_value: 'Academic year', table_name: 'Academic_Years', filterList: [] },
    'Assessments_Name': { display_value: 'Assessment type', table_name: 'Assessments_Name', filterList: [] },
    'Semesters': { display_value: 'Semesters', table_name: 'Semesters', filterList: [] },
    'Departments': { display_value: 'Departments', table_name: 'Departments', filterList: [] },

  };
  filterItemList = ['Degrees', 'Batches', 'Academic_Years', 'Departments', 'Semesters', 'Assessments_Name'];
  studentApi = {};
  data = { headers: [], marks: [] };

  constructor(private academicApiService: AcademicApiService) { }

  ngOnInit() {
    this.getFilterList('Degrees');
  }

  selectOption(item, e, index) {
    this.studentApi[item] = e.value;
    let id;
    if (index < this.filterItemList.length - 1) {
      id = this.filterItemList[index + 1];
    } else {
      id = this.filterItemList[index];
    }
    document.getElementById(id).click();
  }





  getData() {
    // console.log(this.studentApi)
    // this.studentApi = JSON.parse('{"Degrees":"1","Batches":"7","Academic_Years":"3","Departments":"1","Semesters":"7","Assessments_Name":"4"}')
    this.academicApiService.getReportStudent(this.studentApi).subscribe(res => {
      if (res.statusCode === 200) {
        this.data.headers = res['headers'];
        this.data.marks = res['marks'];
      }
      // this.data.marks.forEach(i => {
      //   this.data.headers.forEach(i => {

      //   });
      //   i.sum = 0;
      //   i.avg = 0;
      // });
      console.log(this.data);
    });
  }


  getFilterList(name) {
    this.academicApiService.getReportFilter(name).subscribe(res => {
      if (res.statusCode == 200) {
        this.filterItemObject[name].filterList = res.info;
      }
    });
  }

}
