import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import * as moment from 'moment';
import { AcademicApiService } from '../service/academic-api.service';
import { CommonService } from '../../shared/common/common.service';
import { ToastService } from '../../shared/common/toast.service';


@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '', search: {}, filter: {}, notEqualFilter: {}, requestFields: ''
  };
  hoursList = ['hour1', 'hour2', 'hour3', 'hour4', 'hour5', 'hour6', 'hour7', 'hour8']
  currentView = JSON.parse(localStorage.currentView);
  currentUser = JSON.parse(localStorage.flowPodUser);
  metaData = [];
  selectedDate: any;
  handlingClass = [];
  selectedHours = [];
  tempMetaData = [];
  date = '';
  data = [];
  statusList = [];
  status: any;
  isEdit = '';
  constructor(public commonApiService: CommonApiService,
    public academicApiService: AcademicApiService,
    public commonService: CommonService,
    public toastService: ToastService) { }

  ngOnInit() {
    this.getMetaData();
    this.getAttendanceStudent();
    this.selectedDate = new Date();
    this.getClass(this.selectedDate);
  }
  getClass(e) {
    this.selectedDate = e;
    this.selectedHours = [];
    this.tempMetaData = [];
    this.data = [];
    const getBody = {};
    getBody['userId'] = this.currentUser.id;
    this.date = moment(e).format('YYYY-MM-DD');
    getBody['date'] = this.date;
    this.handlingClass = [];
    this.academicApiService.getHandlingClass(getBody).forEach(res => {
      if (res.statusCode === 200) {
        this.handlingClass = res.info;
        if (!!res.info.length) {
          this.insertStudent();
          this.getStudentAttendance(res.info[0].handlingHour[0], res.info[0], '', 0);
        }
      }
    });
  }
  massAttendance(value) {
    if (!!this.status) {
      if (this.status.short === value) {
        this.status = {};
      } else {
        this.statusList.forEach(i => {
          if (i.short === value) {
            this.status = i;
          }
        });
      }

    } else {
      this.statusList.forEach(i => {
        if (i.short === value) {
          this.status = i;
        }
      });
    }
    if (!!this.status.short) {
      this.data.forEach(i => {
        this.selectedHours.forEach(j => {
          let k = j.slice(0, j.length - 1)
          i[k] = this.status;
        });
      });
      this.massUpdateAttendance();
    }
  }
  massUpdateAttendance() {
    const putdata = [];
    this.data.forEach(i => {
      const temp = {};
      temp['id'] = i.id;
      this.selectedHours.forEach(j => {
        let k = j.slice(0, j.length - 1)
        temp[k] = this.status.id;
      });
      temp['tableName'] = 'Attendances';
      putdata.push(temp);
    });
    this.commonApiService.update(putdata).subscribe(res => {
      if (res[0].status === 202) {
        this.toastService.success(res[0].message);
      }
    });
  }
  updateAttendace(id, item, field) {
    let putdata = {};
    putdata['id'] = id + '';
    putdata['tableName'] = 'Attendances';
    putdata[field] = item['id'];
    this.commonApiService.update(putdata).subscribe(res => {
      if (res.status === 202) {
        this.toastService.success(res.message);
      }
    });
  }
  getStudentAttendance(hour, item, e, index) {
    let temphour = hour + index;
    if (e && e.ctrlKey) {
      if (this.selectedHours.includes(temphour)) {
        this.selectedHours.splice(this.selectedHours.indexOf(temphour), 1);
      } else {
        this.selectedHours.push(temphour);
      }
    } else {
      this.selectedHours = [temphour];
      this.tempMetaData = [];
      this.insertStudent();
    }
    this.apiObj.tableName = 'Students';
    // let getBody = {};
    let getBody = {
      batch: item.batch[0]['id'], department: item.department[0]['id'] + '',
      section: item.section, semester: item.semester + '',
      degree: item.degree[0]['id'] + '', date: this.date, academic_year: item.academicYear[0]['id'] + '',
      split: item.split,
      dayOrder: item.dayOrder
    };
    // console.log(item.dayOrder, item)
    getBody['userId'] = this.currentUser.id + '';
    this.academicApiService.getStudentAttendance(getBody).subscribe(res => {
      if (res.statusCode === 200) {

        this.insertHour(hour, e);
        this.data = res.info;

      }
    });
  }
  insertHour(hour, e) {
    // if (e.ctrlKey) {
    this.metaData.forEach(i => {
      if (i.name === hour) {
        if (this.tempMetaData.includes(i)) {
          this.tempMetaData.splice(this.tempMetaData.indexOf(i), 1);
        } else {
          this.tempMetaData.push(i);
        }
      }
    });
    // }
  }

  getAttendanceStudent() {
    this.apiObj.tableName = 'Attendances_Status';
    this.apiObj.sortValue = 'id';
    this.apiObj.field = '';
    this.apiObj.fieldValue = '';
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.statusList = res.info;
      }
    });
  }

  getMetaData() {
    this.apiObj.fieldValue = this.currentView.id;
    this.apiObj.notEqualFilter = { 'tableViewPosition': ['-1'] };
    this.commonApiService.getCommon(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaData = res.info;
        this.insertStudent();
      }
    });
  }
  insertStudent() {
    // console.log("sssss")
    this.metaData.forEach(i => {
      i.name = this.commonService.checkToCamelCase(i.name);
      // console.log(i.name);
      if (i.name === 'studentName') {
        this.tempMetaData.push(i);
      }
    });
  }

}
