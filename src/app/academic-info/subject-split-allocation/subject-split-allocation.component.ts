import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { CommonService } from '../../shared/common/common.service';
import { MatDialog } from '@angular/material';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-subject-split-allocation',
  templateUrl: './subject-split-allocation.component.html',
  styleUrls: ['./subject-split-allocation.component.css']
})
export class SubjectSplitAllocationComponent implements OnInit {

  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '', search: {}, filterValue: {}, notEqualFilter: {}, requestFields: ''
  };
  studentList = [];
  currentView = JSON.parse(localStorage.currentView);
  metaData = [];
  data = [];
  putBody = {};
  dialogRef: any;
  splitedStudentList = [];
  subjectInfo: any;
  inputValue = {};
  postBody = {};
  constructor(
    public commonApiService: CommonApiService,
    public commonService: CommonService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.getMetaData();
    this.getAll();

  }
  getMetaData() {
    this.apiObj.fieldValue = this.currentView.id + '';
    this.apiObj.sortValue = 'table_view_position';
    this.commonApiService.getCommon(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        res.info.forEach(i => {
          if (i.tableViewPosition !== -1) {
            if (i.type === 'table') {
              i.displayValue = "Split Allocation"
            }
            this.inputValue[i.name] = '';
            this.metaData.push(i);
          }
        });
      }
    });
  }
  deleteAllocation(rowId) {
    this.commonApiService.delete(rowId + '', 'Subject_Allocations').subscribe(res => {
      if (res.status === 200) {
        this.getAll();
      }
    });
  }
  getAll() {
    this.apiObj.tableName = "Subject_Allocations"
    this.apiObj.sortValue = 'id';
    this.apiObj.field = ''
    this.apiObj.fieldValue = ''
    this.commonApiService.getCommon(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
      }
    });
  }
  eventListen(e) {
    const key = Object.keys(e)[0];
    this.putBody[key] = e[key];
    this.putBody['tableName'] = this.currentView.dataTableName;
    this.putBody['id'] = e['rowId'];
    const rowId = e['rowId'];
    if (e.isValid) {
      this.commonApiService.update(this.putBody).subscribe(res => {
        console.log(res);
      });
    }

    this.putBody = {};
  }

  splitAllocation(item, templateName) {
    // console.log(item)
    this.subjectInfo = item;
    this.apiObj.tableName = "Students"
    this.apiObj.sortType = 'ASC'
    this.apiObj.sortValue = 'first_name'
    this.apiObj.filterValue = {
      department: [item.department[0].id + ''],
      degree: [item.degree[0].id + ''],
      section: [item.section],
      semester: [item.semester],
      batch: [item.batch[0].id + '']
    };
    this.apiObj.requestFields = "id,first_name,register_number"
    this.commonApiService.getWithoutPermission(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.studentList = res.info;
        this.studentList.forEach(i => {
          i['checked'] = false;
        });

        this.getSplitedStudent();
      }
    });
    this.dialogRef = this.dialog.open(templateName, {
      width: '800px',
      height: '90%',
      disableClose: true
    });
  }
  getSplitedStudent() {
    this.splitedStudentList = [];
    this.apiObj.requestFields = '';
    this.apiObj.tableName = 'Split_Allocation';
    this.apiObj.filterValue = { '413_id': [this.subjectInfo.id + ''] };
    this.apiObj.sortValue = 'id'
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        let temp = res.info;
        this.studentList.forEach(i => {
          temp.forEach(j => {
            if (i.id == j.student[0].id) {
              i['checked'] = true;
              i['split_id'] = j.id + ''
              this.splitedStudentList.push(i)
            }
          });
        });
      }
    });
  }

  drop(event: CdkDragDrop<string[]>, type) {
    let item;
    let valid = false;
    if (event.previousContainer === event.container) {

    } else {
      if (type === 'allocate') {
        item = this.studentList[event.previousIndex];
        this.insertSplit(item)
        item['checked'] = true;
      } else if (type === 'non-allocate') {
        item = this.splitedStudentList[event.previousIndex]
        item['checked'] = false;
        this.deleteSplit(item);
      }
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
  insertSplit(item) {
    let postBody = {};
    postBody['413_id'] = this.subjectInfo.id + ''
    postBody['student'] = item.id + ''
    postBody['tableName'] = 'Split_Allocation';
    this.commonApiService.create(postBody).subscribe(res => {
      if (res.statusCode === 201) {
        // return true
        item['split_id'] = res.info.id + ''
      }
    });
    // return false
  }
  deleteSplit(item) {
    // console.log(item)
    this.commonApiService.delete(item['split_id'], 'Split_Allocation').subscribe(res => {
      // console.log(res);
    });
  }
  updateList(checked, item, index) {
    if (checked) {
      this.studentList.splice(index, 1);
      item.checked = checked;
      this.splitedStudentList.push(item);
      this.insertSplit(item)
    } else {
      item.checked = false;
      // this.studentList.push(item);
      this.splitedStudentList.splice(index, 1);
      this.deleteSplit(item)
    }
  }
  addRow(templateName) {

    this.dialogRef = this.dialog.open(templateName, {
      width: '800px',
      height: '90%',
      disableClose: true
    });
  }
  eventListenForCreate(e) {
    const key = Object.keys(e)[0];
    this.postBody[key] = e[key];
    console.log(this.postBody)
  }

  save() {
    this.postBody["tableName"] = "Subject_Allocations"
    this.commonApiService.create(this.postBody).subscribe(res => {
      if (res.statusCode === 201) {
        this.getAll();
        this.dialogRef.close();
      }
    });
  }
}
