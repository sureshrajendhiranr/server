import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectSplitAllocationComponent } from './subject-split-allocation.component';

describe('SubjectSplitAllocationComponent', () => {
  let component: SubjectSplitAllocationComponent;
  let fixture: ComponentFixture<SubjectSplitAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectSplitAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectSplitAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
