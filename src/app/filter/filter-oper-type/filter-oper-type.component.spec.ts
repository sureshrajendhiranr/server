import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterOperTypeComponent } from './filter-oper-type.component';

describe('FilterOperTypeComponent', () => {
  let component: FilterOperTypeComponent;
  let fixture: ComponentFixture<FilterOperTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterOperTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterOperTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
