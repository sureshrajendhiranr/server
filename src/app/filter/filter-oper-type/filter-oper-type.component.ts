import { Component, OnInit, Input } from '@angular/core';
import { operations, fieldType, match } from './../filterValue';

@Component({
  selector: 'app-filter-oper-type',
  templateUrl: './filter-oper-type.component.html',
  styleUrls: ['./filter-oper-type.component.css']
})
export class FilterOperTypeComponent implements OnInit {
  operation: any;
  type: any;

  constructor() { }

  ngOnInit() {
    this.operation = operations.filter(i => {
      return i;
    });
    this.type = fieldType.filter(i => {
      return i;
    });
  }

}
