import { Component, OnInit, Input } from '@angular/core';
import { operations, fieldType, match } from './../filterValue';

@Component({
  selector: 'app-filter-view',
  templateUrl: './filter-view.component.html',
  styleUrls: ['./filter-view.component.css']
})
export class FilterViewComponent implements OnInit {
  @Input() metaData: any;
  operation: any;
  type: any;
  matchType: any;
  selected = [];
  value: any;
  data: any;

  constructor() { }

  ngOnInit() {

    this.data = this.metaData;

    this.operation = operations.filter(i => {
      return i;
    });
    this.type = fieldType.filter(i => {
      return i;
    });
    this.matchType = match.filter(i => {
      return i;
    });
    this.selected = this.matchType[0];

  }

}
