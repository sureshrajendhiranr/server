import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterViewComponent } from './filter-view/filter-view.component';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterFieldComponent } from './filter-field/filter-field.component';
import { FilterOperTypeComponent } from './filter-oper-type/filter-oper-type.component';
import { FilterValueComponent } from './filter-value/filter-value.component';

@NgModule({
  declarations: [FilterViewComponent, FilterFieldComponent, FilterOperTypeComponent, FilterValueComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [FilterViewComponent]
})
export class FilterModule { }
