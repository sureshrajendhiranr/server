import { FilterViewComponent } from './filter-view/filter-view.component';

export const operations = ['Equal to', 'Not equal to', 'Contains', 'Does not contain', 'Min length', 'Max length'];

export const fieldType = ['Value', 'Field'];

export const match = ['Any', 'All'];
