import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [DashboardMainComponent],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class DashboardModule { }
