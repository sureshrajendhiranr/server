import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }
  getDashboardAnalysis(): Observable<any> {
    const params = new HttpParams();
    const requestUrl = this.baseUrl + '/dashboardAnalysis/';
    return this.http.get(requestUrl, { params }).pipe(map(this.extractData));
  }

}
