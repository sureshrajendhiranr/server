import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.css']
})
export class DashboardMainComponent implements OnInit {

  constructor(public dashboardService: DashboardService) { }
  data: any;
  isLoaded: boolean = false;
  studentInfo = [
    { icon: 'sentiment_satisfied_alt', name: 'boys', displayName: 'Boys' },
    { icon: 'face', name: 'girls', displayName: 'Girls' },
    { icon: 'school', name: '1st_graduate', displayName: '1st Graduate' },
    { icon: 'school', name: 'scholarship', displayName: 'Scholarship' },
    { icon: 'school', name: 'daysschollar', displayName: 'Days Schollar' },
    { icon: 'directions_bus', name: 'transport', displayName: 'College Bus' }
  ];
  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoaded = false;
    this.dashboardService.getDashboardAnalysis().subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
        console.log(this.data);
        this.isLoaded = true;
      }
    });
  }
}
