import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private _snackBar: MatSnackBar) { }
  success(message) {
    this._snackBar.open(message, '', {
      duration: 1 * 1000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }
}
