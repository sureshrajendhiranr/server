import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  // File size
  fileSizeParse(val) {
    let byte = val;
    const kb = Math.round(byte / 1024);
    const mb = Math.round(kb / 1024);
    const gb = Math.round(mb / 1024);
    if (byte >= 1024) {
      return kb + ' KB'
    } else if (kb >= 1024) {
      return mb + ' MB'
    } else if (mb >= 1024) { return gb + ' GB' };
  }

  // Snake case to Camel case
  toCamelCase = (str) => {
    return str.split('_').map(function (word, index) {
      if (index === 0) {
        return word.toLowerCase();
      }
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }).join('');
  }
  // Jwt to Object
  parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
  };

  // to lowerCase
  toLowerCase(str) {
    return str.toLowerCase();
  }
  toSnakeCase(str) {
    if (str.includes(' ')) {
      return str.split(' ').join('_').toLowerCase();
    } else {
      return str.split(/(?=[A-Z])/).join('_').toLowerCase()
    }
  }

  // check and Camel case
  checkToCamelCase = (str) => {
    if (str.includes('_')) {
      return str.split('_').map(function (word, index) {
        if (index === 0) {
          return word.toLowerCase();
        }
        return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
      }).join('');
    } else {
      return str
    }
  }
}
