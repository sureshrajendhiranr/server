import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { MaterialModule } from '../material.module';
import { DesignationComponent } from './components/designation/designation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SortableColumnComponent, SortableTableDirective } from './directives/sortable-column.directive';
import { DeleteComponent } from './components/delete/delete.component';
import { SearchComponent } from './components/search/search.component';
import { UserAvatorComponent } from './components/user-avator/user-avator.component';
import { UserSmallViewComponent } from './components/user-small-view/user-small-view.component';


@NgModule({
  declarations: [LoaderComponent, DesignationComponent,
    SortableColumnComponent, SortableTableDirective, DeleteComponent,
    UserAvatorComponent,
    SearchComponent, UserSmallViewComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [LoaderComponent, DesignationComponent, SortableColumnComponent,
    SortableTableDirective, DeleteComponent, SearchComponent, UserAvatorComponent, UserSmallViewComponent],
  entryComponents: [UserAvatorComponent]
})
export class SharedModule { }
