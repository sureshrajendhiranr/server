import { Directive, Input, HostListener, Component, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { SortServiceService } from './sort-service.service';


@Component({
  selector: '[sortable-column]',
  templateUrl: './sortable-column.component.html'
})
export class SortableColumnComponent implements OnInit, OnDestroy {

  constructor(public sortService: SortServiceService) { }

  @Input('sortable-column')
  columnName: string;

  @Input('sort-direction')
  sortDirection: string = '';
  isHover = false;

  private columnSortedSubscription: Subscription;

  @HostListener('click')
  sort() {
    if (this.sortDirection === '') {
      this.sortDirection = 'asc';
      this.isHover = false;
    } else if (this.sortDirection === 'asc') {
      this.sortDirection = 'desc';
      this.isHover = false;
    } else if (this.sortDirection === 'desc') {
      this.sortDirection = '';
    }
    this.sortService.columnSorted({ active: this.columnName, direction: this.sortDirection });
  }
  @HostListener('mouseover')
  mouseover() {
    if (this.sortDirection === '') {
      this.isHover = true;
    }
  }

  @HostListener('mouseleave')
  mouseleave() {
    if (this.sortDirection === '') {
      this.isHover = false;
      this.sortDirection = '';
    }
  }

  ngOnInit() {
    this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
      if (this.columnName != event.active) {
        this.sortDirection = '';
      }
    });
  }

  ngOnDestroy() {
    this.columnSortedSubscription.unsubscribe();
  }

}








@Directive({
  selector: '[sortable-table]'
})
export class SortableTableDirective implements OnInit, OnDestroy {

  constructor(private sortService: SortServiceService) { }

  @Output()
  sorted = new EventEmitter();

  private columnSortedSubscription: Subscription;

  ngOnInit() {
    this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
      this.sorted.emit(event);
    });
  }

  ngOnDestroy() {
    this.columnSortedSubscription.unsubscribe();
  }
}
