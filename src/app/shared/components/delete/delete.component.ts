import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
  @Input('inputData') inputData: any;
  @Output() event = new EventEmitter();
  data = { head: '', sub: '' };
  constructor() { }

  ngOnInit() {
    if (this.inputData.type === 'row') {
      this.data.head = 'Row';
      this.data.sub = 'the selected row?';
    } else if (this.inputData.type === 'rows') {
      this.data.head = 'Row(s)';
      this.data.sub = 'the selected row(s)?';
    } else if (this.inputData.type === 'item') {
      this.data.head = 'Item';
      this.data.sub = 'this item?';
    } else if (this.inputData.type === 'items') {
      this.data.head = 'Item(s)';
      this.data.sub = 'the selected item(s)?';
    } else if (this.inputData.type === 'field') {
      this.data.head = 'Field';
      this.data.sub = 'this field?';
    } else if (this.inputData.type === 'group') {
      this.data.head = 'Group';
      this.data.sub = 'this group?';
    }
  }

}
