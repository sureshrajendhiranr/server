import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  @Input('size') rsize: number;
  @Input('font') fsize: number;
  @Input('mode') mode: string;
  @Input('progressBarType') progressBarType: string;
  constructor() { }

  ngOnInit() {
  }

}
