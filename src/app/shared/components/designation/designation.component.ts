import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { CommonApiService } from '../../../services/common-api.service';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';
@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.css']
})
export class DesignationComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  designationCtrl = new FormControl();
  fruits: string[] = ['Lemon'];
  designationList = [];
  apiObj = {
    tableName: '', limit: 100, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filter: {}
  };
  temp = [];
  controlList = [];
  @ViewChild('designationInput') designationInput: ElementRef<HTMLInputElement>;
  @ViewChild('designationAuto') matAutocomplete: MatAutocomplete;
  @Output('designationEventEmit') designationEventEmit = new EventEmitter();
  @Input('designationAcess') designationAcess: any
  constructor(public commonApiService: CommonApiService) { }

  ngOnInit() {
    this.getAllDesignation();
    this.controlList = this.designationAcess;
  }
  getAllDesignation() {
    this.apiObj.tableName = 'designation';
    this.apiObj.search = {};
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      this.designationList = res.info;
    });
  }
  emitChangeData() {
    this.designationEventEmit.emit(this.temp);
    this.temp = [];
  }
  addToAccess() {
    (this.designationList).forEach(i => {
      if ((this.controlList).includes(i.designation)) {
        this.temp.push({ [i.designation]: 1 });
      } else {
        this.temp.push({ [i.designation]: 0 });
      }
    });
  }
  add(event: MatChipInputEvent): void {

    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.controlList.push(value.trim());
        // this.addToAccess();
      }
      if (input) {
        input.value = '';
      }

      this.designationCtrl.setValue(null);
    }
  }

  remove(item: string): void {
    const index = this.controlList.indexOf(item);

    if (index >= 0) {
      this.controlList.splice(index, 1);
    }
    this.addToAccess();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (!(this.controlList.includes(event.option.viewValue))) {
      this.controlList.push(event.option.viewValue);
    }
    this.addToAccess();
    this.designationInput.nativeElement.value = '';
    this.designationCtrl.setValue(null);
  }
}
