import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CommonApiService } from '../../../services/common-api.service';
import { CommonService } from '../../common/common.service';
import { SearchService } from '../../../services/search.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input('inputData') inputData: any;
  @Output() event = new EventEmitter();
  selectedSearchField = { name: '', displayValue: '' };
  oldSearchField = '';
  data = [];
  isAdvanceField = '';
  // apiObj: any;
  // API get Object
  apiObj = {
    tableName: '', limit: 10, page: 0, sortType: 'ASC', sortValue: 'id',
    field: '', fieldValue: '', search: {}, filter: [], requestFields: ''
  };

  constructor(private commonApiService: CommonApiService,
    private commonService: CommonService, private searchService: SearchService) { }

  ngOnInit() {

  }

  searchFieldSelected(item) {
    this.apiObj.search = {};
    this.apiObj.search[item.name] = '';
    const style = getComputedStyle(document.body);
    const color = style.getPropertyValue('--color-primary');
    const th = document.getElementById(item.name);
    // const td = document.getElementById(item.name + 'td');
    // th.style.border = '1px solid ' + color;
    // th.style.borderBottom = 'none';
    th.style.color = color;
    // td.style.borderLeft = '1px solid ' + color;
    // td.style.borderRight = '1px solid ' + color;
    this.selectedSearchField = item;
    this.oldSearchField = item.name;
    setTimeout(() => {
      document.getElementById(item.name).scrollIntoView({
        behavior: 'smooth', block: 'center', inline: 'center'
      });

    }, 100);
    if (!(this.selectedSearchField['isAdvanced'] == '0')) {
      this.isAdvanceField = this.commonService.toCamelCase(JSON.parse(this.selectedSearchField['isAdvanced'])['fieldName']);
      // this.getAll();
    }
  }

  clearSearch() {
    if (!!this.oldSearchField.length) {
      const thold = document.getElementById(this.oldSearchField);
      // const tdold = document.getElementById(this.oldSearchField + 'td');
      // thold.style.border = 'none';
      thold.style.color = '#101c36';
      // tdold.style.border = 'none';
      this.oldSearchField = '';
      this.data = [];
      this.event.emit();
    }
  }
  selectEventEmit(e) {
    if (this.selectedSearchField['isAdvanced'] != '0') {
      this.apiObj.search[this.selectedSearchField['name']] = e.option['value']['id'];
    } else {
      this.apiObj.search[this.selectedSearchField['name']] = e.option['viewValue'];
    }
    this.event.emit(this.apiObj.search);
    this.apiObj.search[this.selectedSearchField['name']] = e.option['viewValue'];
  }

  searchFunc(value) {
    const body = {};
    body['field'] = this.selectedSearchField;
    body['value'] = value;
    this.searchService.search(body).subscribe(res => {
      if (res.statusCode === 200) {
        this.data = res.info;
      }
    });
  }

  getAll() {
    this.data = [];
    if (!!this.selectedSearchField['dataTableName'] && this.selectedSearchField['name']) {
      this.apiObj.tableName = this.selectedSearchField['dataTableName'];
      this.apiObj.sortType = 'ASC';
      this.apiObj.requestFields = 'id,' + this.selectedSearchField['name'];
      this.apiObj.sortValue = this.selectedSearchField['name'];
      this.commonApiService.getAll(this.apiObj).subscribe(res => {
        if (res.statusCode === 200) {
          if (!(this.selectedSearchField['isAdvanced'] == '0')) {
            this.isAdvanceField = this.commonService.toCamelCase(JSON.parse(this.selectedSearchField['isAdvanced'])['fieldName']);
            res.info.forEach(element => {
              this.data.push(element[this.selectedSearchField['name']][0]);
            });
          } else {
            this.data = res.info;
          }
        }
      });

    }
  }
}
