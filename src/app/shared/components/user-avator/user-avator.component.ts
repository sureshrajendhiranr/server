import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-avator',
  templateUrl: './user-avator.component.html',
  styleUrls: ['./user-avator.component.css']
})
export class UserAvatorComponent implements OnInit {
  @Input('inputData') inputData: any;
  @Input('size') size: any;
  @Input('fontSize') fontSize: any;

s
  constructor() { }

  ngOnInit() {
    if (!!this.size) {
      this.size = this.size + 'px';
    } else {
      this.size = '35px';
    }

    if (!!this.fontSize) {
      this.fontSize = this.fontSize + 'px';
    } else {
      this.fontSize = '16px';
    }


  }

}
