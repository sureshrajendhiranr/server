import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingMainViewComponent } from './setting-main-view/setting-main-view.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UserTableViewComponent } from './user-management/user-table-view/user-table-view.component';
import { CreateUserComponent } from './user-management/create-user/create-user.component';
import { GroupManagementComponent } from './group-management/group-management.component';
import { MaterialModule } from '../material.module';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [SettingMainViewComponent, UserManagementComponent, UserTableViewComponent, CreateUserComponent, GroupManagementComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports:[SettingMainViewComponent]
})
export class AdminSettingsModule { }
