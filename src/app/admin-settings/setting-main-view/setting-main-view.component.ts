import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setting-main-view',
  templateUrl: './setting-main-view.component.html',
  styleUrls: ['./setting-main-view.component.css']
})
export class SettingMainViewComponent implements OnInit {
  selectItem=0;
  tabList = [
    { link: 'user-management', name: 'User Management',isActive:false },
    { link: 'group-management', name: 'Group Management',isActive:false }
  ];
  constructor() { }

  ngOnInit() {
  }

  test(e){
    // console.log(e)
  }
}
