import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingMainViewComponent } from './setting-main-view.component';

describe('SettingMainViewComponent', () => {
  let component: SettingMainViewComponent;
  let fixture: ComponentFixture<SettingMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
