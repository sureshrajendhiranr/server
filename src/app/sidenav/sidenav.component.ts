import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CommonApiService } from '../services/common-api.service';
import { Router } from '@angular/router';
import { LoginService } from '../auth/services/login.service';
import { MatTabsModule, MatTabGroup } from '@angular/material/tabs';
import { environment } from '../../environments/environment';
// import { Notification } from 'rxjs';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @ViewChild('notification_audio') notificationAudio: ElementRef;
  sideMenu = [
    // { icon: 'dashboard', label: 'Dashboard', link: '' },
    // { icon: 'note_add', label: 'Create Flow', link: 'create-flow' }
  ];
  sideMenuListForAPI = [];
  isNavOnClickOpen = false;
  isNavOpen = false;
  isSelectMenu: string;
  dialogRef: any;
  isLoginPage = false;
  user: any;
  signOutLister = true;
  isNotify: boolean = false;
  // API get Object
  apiObj = {
    tableName: 'data_tables', limit: 100, page: 0, sortType: 'ASC', sortValue: 'name',
    field: '', fieldValue: '', search: {}, filter: []
  };
  apiObjNotify = {
    userId: 0
  };
  page1 = 0;
  currentUser: any;
  page = 0;
  notifications = [];
  notificationsCount = 0;
  notificationInputData = { notification: 0, announcement: 0 };
  notificationOnClick = false;
  // index = 0;

  constructor(public dialog: MatDialog, private commonApiService: CommonApiService,
    private route: Router, private loginService: LoginService) { }

  ngOnInit() {
    // this.wsCon();
    if (!!localStorage.flowPodUser) {
      this.user = JSON.parse(localStorage.flowPodUser);
      this.currentUser = this.user;
      this.getNotifyCount();
    }
    console.log("sssssssssssss", this.user)
    this.signOutLister = false;
    this.route.events.subscribe((val) => {
      if (!this.signOutLister) {
        this.getSideNav();
        // this.user = JSON.parse(localStorage.flowPodUser);
        this.signOutLister = true;
      }
    });
    this.getSideNav();
  }
  sideNavbtn(temp) {
    if (!this.isNavOnClickOpen) {
      this.isNavOnClickOpen = true;
      temp.open();
    } else {
      this.isNavOnClickOpen = false;
      temp.open();
    }
  }
  signOut() {
    localStorage.clear();
    // localStorage.removeItem('flowPodUser');
    // localStorage.removeItem('x-token');
    // localStorage.removeItem('flowPodUserDesignation');
    // this.sideMenuListForAPI = [];
    // this.signOutLister = false;
    // this.loginService.dataSource.next({ loginPage: true });
    location.replace('/login');

  }

  test(val) {
    if (val === 'over' && !this.isNavOnClickOpen) {
      this.isNavOpen = true;
    } else {
      this.isNavOpen = false;
    }
  }

  storeCurrentProcess(item) {
    // console.log(item)
    localStorage.setItem('currentView', JSON.stringify(item));
  }

  openDialog(openModelContainer): void {
    this.dialogRef = this.dialog.open(openModelContainer, {
      width: '99%',
      height: '98%'
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  // API service
  getSideNav() {
    this.sideMenuListForAPI = [];
    this.apiObj.search = { categories: 'side Navigation' };
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        (res.info).forEach(i => {
          if (i.dataTableName === 'Create_Flow') {
            i.link = 'create-flow/' + i.dataTableName;
          } else if (i.dataTableName === 'Time_Table') {
            i.link = 'timetable';
          } else
            if (i.dataTableName === 'Academic_Calendar') {
              i.link = 'Academic_Calendar';
            } else if (i.dataTableName === 'Attendances') {
              i.link = 'attendances';
            } else if (i.dataTableName === 'Students') {
              i.link = 'students';
            } else if (i.dataTableName === 'Assessment_Marks') {
              i.link = 'AssesmentMark';
            } else if (i.dataTableName === 'Assessment_Scheduler') {
              i.link = 'assesmentScheduler';
            } else if (i.dataTableName === 'Subject_Allocations') {
              i.link = 'Subject_Allocation';
            }
            else {
              i.link = 'view/' + i.dataTableName;
            }
          this.sideMenuListForAPI.push(i);
        });
        // console.log(this.sideMenuListForAPI);
      }
    });
  }




  // Websocket connection make
  wsCon() {
    let socket = new WebSocket('ws:localhost:8000/ws/' + this.user.id);
    socket['signOut'] = this.signOut();
    // socket.onopen = function (e) {
    //   socket.send('My name is John');
    // };
    setInterval(() => {
      if (socket.readyState === 3) {
        socket = new WebSocket('ws:localhost:8000/ws/' + this.user.id);
      }
      console.log(socket.readyState)
    }, 5000);
    socket.onmessage = function (event) {
      if (event.data.statusCode === 200) {
        console.log('connected');
        console.log(event['signOut']);
      } else if (event.data.statusCode === 403) {
        console.log(event['signOut']);
      }
    };
  }


  // Notification Tab Count
  getNotifyCount() {
    this.apiObjNotify.userId = this.currentUser.id;
    this.commonApiService.getNotificationCount(this.apiObjNotify).subscribe(res => {
      // this.notificationsCount = res;
      if (res.statusCode === 200) {
        this.notificationsCount = res.count;
        console.log(this.notificationsCount);

      }
    });
  }

  markAsRead() {
    this.apiObjNotify.userId = this.currentUser.id;
    this.commonApiService.getMarkAsRead(this.apiObjNotify).subscribe(res => {
      // this.notificationsCount = res;
      if (res.statusCode === 200) {
        // this.notificationsCount = res.count;
      }
    });
  }

}
