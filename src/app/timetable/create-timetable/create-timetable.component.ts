import { Component, OnInit, Input } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { TimetableService } from '../services/timetable.service';
import { _MatListItemMixinBase, MatTableDataSource, throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { CommonService } from '../../shared/common/common.service';
import { ToastService } from '../../shared/common/toast.service';

@Component({
  selector: 'app-create-timetable',
  templateUrl: './create-timetable.component.html',
  styleUrls: ['./create-timetable.component.css']
})
export class CreateTimetableComponent implements OnInit {
  @Input('inputData') inputData: any;
  currentView = JSON.parse(localStorage.currentView);
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '417', search: {}, filter: {}
  };
  metaData = [];
  filterList = [
    { displayName: 'Batch', name: 'batch', value: '', optionValue: [], field: 'academicYear', tableName: 'Batches' },
    { displayName: 'Academic year', name: 'academicYear', value: '', optionValue: [], field: 'degree', tableName: 'Academic_Years' },
    { displayName: 'Degree', name: 'degree', value: '', optionValue: [], field: 'department', tableName: 'Degrees' },
    { displayName: 'Department', name: 'shortName', value: '', optionValue: [], tableName: 'Departments' },
    {
      displayName: 'Semester', name: 'semester', value: '', optionValue: [
        { semester: '1' }, { semester: '2' }, { semester: '3' }, { semester: '4' },
        { semester: '5' }, { semester: '6' }, { semester: '7' }, { semester: '8' }
      ]
    },
    {
      displayName: 'Section', name: 'section', value: '', optionValue: [
        { section: 'A' }, { section: 'B' }, { section: 'C' }, { section: 'D' }, { section: 'E' }, { section: 'F' }
      ]
    }
  ];
  semester = '';
  department = '';
  academicYear = '';
  degree = '';
  selectedItem = {};
  tableMatrix: any;
  dataSource: any;
  data = [];
  displayColumns = [];
  subjectList = [];
  spiltList = ['split-1', 'split-2', 'split-3', 'split-4', 'split-5', 'split-6']
  // editField = ['academic_year', 'batch', 'degree', 'split_type', 'department', 'semester'];
  displayTocolumn = ['dayOrder', 'splitType', 'hour1', 'hour2', 'hour3', 'hour4', 'hour5', 'hour6', 'hour7', 'hour8',
    'hour9', 'action'];
  isUpdate: boolean = true;
  getBody = {};
  isEdit: boolean = false;
  dataTableInfo: any;
  constructor(public commonApiService: CommonApiService,
    public timetableService: TimetableService,
    public commonService: CommonService,
    public toastService: ToastService) { }

  ngOnInit() {
    this.getProcessInfo();
    this.getMetaData();
    if (!!this.inputData) {
      // console.log(this.inputData)
    }
  }
  addSplit(row, index) {

    let filter = this.getBody['filterValue'];
    let temp = {};
    let split = parseInt(row['splitType'].split('-')[1])
    let list = Object.keys(this.value);
    // console.log(this.value)
    this.metaData.forEach(j => {
      if (list.includes(j.name)) {
        temp[j.name] = this.value[j.name];
      } else {
        if (j.name !== 'lastModified' && j.name !== 'createdOn') {
          if (j.name === 'splitType') {
            temp[j.name] = 'split-' + (split + 1);
          }
          else if (j.name === 'dayOrder') {
            temp[j.name] = row['dayOrder'];
          } else {
            temp[j.name] = '';
          }
          if (j.name === 'timeTableCode') {
            temp[j.name] = filter['degree'][0] + '-' + filter['department'][0] + '-' + filter['batch'][0] +
              '-' + filter['semester'][0] + '-' + filter['section'][0];
          }
          if (j.name === 'department') {
            temp[j.name] = this.department['department'] + '';
          }
        }

      }
    });
    let t1 = [];
    if (!!this.isUpdate) {
      delete (temp['action']);
      this.timetableService.createTimeTable([temp]).subscribe(res => {
        if (!!res.length) {
          if (res[0].statusCode === 201) {
            this.toastService.success("created successfully");
            temp = res[0].info;
            this.data.forEach((i, index1) => {
              if (index1 === index + 1) {
                t1.push(temp);
              }
              t1.push(i);
            });
            this.data = t1;
            this.dataSource = new MatTableDataSource(this.data);
          }
        }

      });
    } else {
      this.data.forEach((i, index1) => {
        if (index1 === index + 1) {
          t1.push(temp);
        }
        t1.push(i);
      });
      this.data = t1;
      this.dataSource = new MatTableDataSource(this.data);
    }

  }
  removeSplit(row, i) {
    if (this.isUpdate) {
      this.commonApiService.delete(row.id, 'Time_Table').subscribe(res => {
        console.log(res)
        this.data.splice(i, 1);
        this.dataSource = new MatTableDataSource(this.data);
      });
    } else {
      this.data.splice(i, 1);
      this.dataSource = new MatTableDataSource(this.data);
    }
  }
  getProcessInfo() {
    this.commonApiService.getDataTableInfo(this.currentView.id).subscribe(res => {
      if (res.statusCode === 200) {
        this.dataTableInfo = res.info;
      }
    });

  }
  getMetaData() {
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaData = res.info;
        this.metaData.push({ name: 'action' })
        this.metaData.forEach(i => {
          i.name = this.commonService.toCamelCase(i.name);

          this.displayColumns.push(i.name);
        });
      }
    });
  }
  get(item) {
    this.apiObj.tableName = item.tableName;
    this.apiObj.field = '';
    this.apiObj.fieldValue = '';
    this.apiObj.sortValue = 'id';
    this.apiObj.search = { [item.name]: item.value };
    if (item.name !== 'semester' && item.name !== 'section') {
      this.commonApiService.getAll(this.apiObj).subscribe(res => {
        if (res.statusCode === 200) {
          item.optionValue = res.info;
        }
      });
    }
  }
  value = {}
  getTableTimeConfig(item, e) {
    // console.log(e)
    item.value = e.option.viewValue;
    if (item.name === 'semester') {
      this.semester = item.value + '';
    }
    if (item.name === 'academicYear') {
      this.academicYear = e.option.value;
    }
    if (item.name === 'shortName') {
      this.department = e.option.value;
      // this.department = e.option.viewValue;
      // console.log(this.department)
    }
    if (item.name !== 'shortName') {
      this.value[item.name] = e.option.viewValue;
    } else {
      this.value['department'] = e.option.viewValue;
    }
    if (item.name === 'degree') {
      this.degree = e.option.value;
      let body = {};
      body['tableName'] = 'Time_Table_Config';
      body['field'] = 'degree';
      body['sortValue'] = 'id';
      body['sortType'] = 'ASC';
      body['limit'] = 100;
      body['page'] = 0;
      body['fieldValue'] = e.option.value['id'];
      this.commonApiService.getAll(body).subscribe(res => {
        if (res.statusCode === 200) {
          this.tableMatrix = res.info[0];

        }
      });
    }
    this.getTimeTable();
  }
  getTimeTable() {
    let isvalid = false;
    this.getBody['tableName'] = 'Time_Table';
    this.getBody['field'] = '';
    this.getBody['sortValue'] = 'id';
    this.getBody['sortType'] = 'ASC';
    this.getBody['limit'] = 100;
    this.getBody['page'] = 0;
    this.getBody['fieldValue'] = ''
    let temp = {};
    this.filterList.forEach(i => {
      if (!!i.value.length) {
        temp[i.name] = [i.value];
        isvalid = true;
      } else {
        isvalid = false;
      }
    });
    if (!!isvalid) {
      this.getBody['tableName'] = 'Subjects';
      this.getBody['sortValue'] = 'id';
      this.getBody['filterValue'] = {
        department: [this.department['id'] + ''], semester: [this.semester],
        degree: [this.degree['id'] + '']
      };
      this.commonApiService.getAll(this.getBody).subscribe(res => {
        if (res.statusCode === 200) {
          this.subjectList = res.info;
        }
      });
      // getbody['tableName'] = 'Time_Table';
      delete (this.getBody['tableName']);
      delete (temp['shortName']);
      temp['department'] = [this.department['shortName'] + ''];
      this.getBody['filterValue'] = temp;
      this.getBody['sortValue'] = 'day_order';
      console.log(temp, this.department)
      this.timetableService.getTimeTable(this.getBody).subscribe(res => {
        if (res.statusCode === 200) {
          this.data = res.info;
          if (!this.data.length) {
            this.isUpdate = false;
            this.generateEmptyTable(temp);
          } else {
            this.isUpdate = true;
            this.dataSource = new MatTableDataSource(this.data);
          }
        }
      });
    }
  }
  generateEmptyTable(filter) {
    let list = Object.keys(this.value);
    let splitType = 0;
    this.data = [];
    console.log(this.value)
    for (let i = 1; i < parseInt(this.tableMatrix['noOfDayOrder']) + 1; i++) {
      let temp = {}
      this.metaData.forEach(j => {
        if (list.includes(j.name)) {
          temp[j.name] = this.value[j.name];
        } else {
          if (j.name !== 'lastModified' && j.name !== 'createdOn') {
            if (j.name === 'splitType') {
              temp[j.name] = 'split-1';
            }
            else if (j.name === 'dayOrder') {
              temp[j.name] = i;
            } else {
              temp[j.name] = '';
            }
            if (j.name === 'timeTableCode') {
              temp[j.name] = filter['degree'][0] + '-' + filter['department'][0] + '-' + filter['batch'][0] +
                '-' + filter['semester'][0] + '-' + filter['section'][0];
            }
          }

        }
      });
      this.data.push(temp);

    }

    // for (let i = 1; i < (this.tableMatrix['noOfDayOrder'] * 2) + 1; i++) {
    //   let temp = {}
    //   let day = Math.round(i / 2);
    //   this.metaData.forEach(j => {
    //     if (list.includes(j.name)) {
    //       temp[j.name] = this.value[j.name];
    //     } else
    //       if (j.name !== 'lastModified' && j.name !== 'createdOn') {
    //         if (j.name === 'splitType') {
    //           splitType++;
    //           if (splitType === 1) {
    //             temp[j.name] = 'split-1';
    //           } else {
    //             temp[j.name] = 'split-2';
    //           }
    //           if (splitType === 2) {
    //             splitType = 0;
    //           }
    //         } else if (j.name === 'dayOrder') {
    //           temp[j.name] = day;
    //         } else {
    //           temp[j.name] = '';
    //         }
    //         if (j.name === 'timeTableCode') {
    //           temp[j.name] = filter['degree'][0] + '-' + filter['department'][0] + '-' + filter['batch'][0] +
    //             '-' + filter['semester'][0] + '-' + filter['section'][0];
    //           // console.log(temp[j.name]);
    //         }
    //         if (j.name === 'department') {
    //           temp[j.name] = this.department['id'] + '';
    //         }
    //       }
    //   });
    //   this.data.push(temp);
    // }
    this.dataSource = new MatTableDataSource(this.data);
  }

  save(item) {
    if (this.isUpdate) {
      const putBody = {};
      putBody['id'] = item.id;
      putBody[item.field] = item.value;
      this.timetableService.updateTimeTable(putBody).subscribe(res => {
        if (res.status === 202) {
          this.toastService.success(res.message)
        }
      });
    } else {
      this.data.forEach(i => {
        i.department = this.department['shortName'] + '';
        delete (i['action']);
      });
      this.timetableService.createTimeTable(this.data).subscribe(res => {
        if (res.statusCode === 201) {
          this.toastService.success(res.message);
        }
      });
    }
  }
}
