import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { CreateTimetableComponent } from './create-timetable/create-timetable.component';
import { ViewTimetableComponent } from './view-timetable/view-timetable.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ControlFlowModule } from '../control-flow/control-flow.module';

@NgModule({
  declarations: [CreateTimetableComponent, ViewTimetableComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ControlFlowModule
  ],
  exports: [ViewTimetableComponent],
})
export class TimetableModule { }
