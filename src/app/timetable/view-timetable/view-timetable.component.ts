import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../services/common-api.service';
import { TimetableService } from '../services/timetable.service';
import { _MatListItemMixinBase, MatTableDataSource } from '@angular/material';
import { CommonService } from '../../shared/common/common.service';
import { ToastService } from '../../shared/common/toast.service';



@Component({
  selector: 'app-view-timetable',
  templateUrl: './view-timetable.component.html',
  styleUrls: ['./view-timetable.component.css']
})
export class ViewTimetableComponent implements OnInit {
  currentView = JSON.parse(localStorage.currentView);
  currentUser = JSON.parse(localStorage.flowPodUser);
  apiObj = {
    tableName: 'meta_data', limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '417', search: {}, filter: {}
  };
  api = {
    limit: 100, page: 0, sortType: 'ASC', sortValue: 'position',
    field: 'data_table_id', fieldValue: '417', search: {}, filter: {}
  };
  metaData = [];
  dataSource: any;

  displayTocolumn = [
    'dayOrder', 'hour1', 'hour2', 'hour3', 'hour4', 'hour5', 'hour6', 'hour7', 'hour8'];
  displayToColumn = [
    { displayValue: 'Day Order', name: 'dayOrder' },
    { displayValue: 'Hour 1', name: 'hour1' },
    { displayValue: 'Hour 2', name: 'hour2' },
    { displayValue: 'Hour 3', name: 'hour3' },
    { displayValue: 'Hour 4', name: 'hour4' },
    { displayValue: 'Hour 5', name: 'hour5' },
    { displayValue: 'Hour 6', name: 'hour6' },
    { displayValue: 'Hour 7', name: 'hour7' },
    { displayValue: 'Hour 8', name: 'hour8' },
  ];
  data = [];
  isUpdate: boolean = true;
  timeTableList = [];
  isEdit: boolean = false;
  dataTableInfo: any;
  isLoading: boolean = false;
  isSidebarView: boolean = false;
  constructor(public commonApiService: CommonApiService,
    public timetableService: TimetableService,
    public commonService: CommonService,
    public toastService: ToastService) { }

  ngOnInit() {
    this.getMetaData();
    this.getTimeTable();
    // this.getDistrict();
    if (this.currentUser.designation === 'HOD' || this.currentUser.designation === 'admin') {
      this.getDistrict();
      this.isSidebarView = true;
    }
  }


  getDistrict() {
    let getBody = {};
    getBody['tableName'] = 'Time_Table';
    getBody['field'] = 'time_table_code';
    getBody['limit'] = 100;
    getBody['page'] = 0;
    this.commonApiService.getDistinct(getBody).subscribe(res => {
      if (res.statusCode === 200) {
        this.timeTableList = res.info;
        if (!!res.info.length) {
          this.getTimeTableByCode(res.info[0]);
        }
      }
    });
  }
  getTimeTableByCode(code) {
    this.api.sortValue = 'dayOrder';
    this.api.field = 'time_table_code';
    this.api.fieldValue = code;
    const getbody = {};
    getbody['timeTableCode'] = code;

    this.timetableService.getOneTimeTable(getbody).subscribe(res => {
      this.data = res;
      if (!!this.data.length) {
        this.data.sort(function (a, b) {
          return a.dayOrder - b.dayOrder;
        });
      }
      this.dataSource = new MatTableDataSource(this.data);
    });
  }
  getMetaData() {
    this.commonApiService.getAll(this.apiObj).subscribe(res => {
      if (res.statusCode === 200) {
        this.metaData = res.info;
        this.metaData.forEach(i => {
          i.name = this.commonService.toCamelCase(i.name);
        });
      }
    });
  }

  getTimeTable() {
    this.isLoading = false;
    const getBody = {};
    getBody['userId'] = this.currentUser.id;
    this.timetableService.getOneTimeTable(getBody).subscribe(res => {
      this.data = res;
      if (!!this.data.length) {
        this.data.sort(function (a, b) {
          return a.dayOrder - b.dayOrder;
        });
      }
      this.dataSource = new MatTableDataSource(this.data);
    });
    this.isLoading = true;
  }
}
